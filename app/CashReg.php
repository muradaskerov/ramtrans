<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashReg extends \Eloquent
{
    protected $table = 'cashreg';
    protected $fillable = ['cas_date','cas_usd','cas_azn','cas_eur'];
    protected $primaryKey = 'cas_id';
}
