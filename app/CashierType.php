<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashierType extends \Eloquent
{
    protected $table = "cashier_types";

    protected $primaryKey = "cast_id";
}
