<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends \Eloquent
{
    protected $table = 'm_customers';
    public $timestamps = false;
    protected $fillable = ['cus_name'];

}
