<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database;

class Data extends \Eloquent
{
    public static function menuList()
    {
//        $query = "
//         SELECT
//            menu_view AS id,
//            menu_value AS `value`,
//            menu_layout AS layout,
//            menu_icon AS icon
//                FROM d_menu
//            WHERE menu_status = 'on'
//            ORDER BY menu_priority
//           ";

        $data = User::with('menus')->find(Session::get("user")->user_id);


        return $data->menus;

    }

    public static function lineList()
    {
        $query = "
         SELECT
            line_id AS `id`,
            line_name AS `value`
                FROM `lines`
           ";

        return DB::select($query);

    }

    public static function customer_list()
    {
        $query = "
         SELECT
            cus_id AS `id`,
            cus_name AS `value`
                FROM `m_customers`
           ";

        return DB::select($query);

    }

    public static function terminal_list(){
        $query = "
         SELECT
            ter_id AS `id`,
            ter_name AS `value`
                FROM `terminals`
           ";

        return DB::select($query);
    }

    public static function station_list(){
        $query = "
         SELECT
            sta_id AS `id`,
            sta_name AS `value`
                FROM `stations`
           ";

        return DB::select($query);
    }

    public static function action_list(){
        $query = "
         SELECT
            ac_id AS `id`,
            ac_name AS `value`
                FROM `actions`
           ";

        return DB::select($query);
    }

    public static function tariff_list(){
        $query = "
         SELECT
            tar_id AS `id`,
            tar_id AS `value`
                FROM `tariffs`
           ";

        return DB::select($query);
    }

    public static function cashier_types_list(){
        $query = "
         SELECT
            cast_id AS `id`,
            cast_name AS `value`
                FROM `cashier_types`
           ";

        return DB::select($query);
    }

    public static function report_lists(){
        $query = "
         SELECT
            report_id AS `id`,
            report_id AS `value`
                FROM `reports`
           ";

        return DB::select($query);
    }



}
