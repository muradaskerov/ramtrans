<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseTypes extends \Eloquent
{
    protected $table = "expense_types";

    protected $primaryKey = "et_id";

    protected $fillable = ['et_name'];
}
