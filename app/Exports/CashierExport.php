<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use \Request;
use Illuminate\Contracts\View\View;

class CashierExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
//    public function collection()
//    {
//        $items = json_decode(Request::get("exams"),true);
//        return collect($items);
////        dd($items);
//    }

    public function view(): View
    {
        $items = json_decode(Request::get("expenses"),true);
        //dd($items);
        return view('cashier', [
            'data' => $items
        ]);
    }
}
