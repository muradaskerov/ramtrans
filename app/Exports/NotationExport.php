<?php

namespace App\Exports;

use App\Order;
use Maatwebsite\Excel\Concerns\FromView;
use \Request;
use Illuminate\Contracts\View\View;

class NotationExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $notation_id = Request::get('notation_id');

        $items = Order::where('notation_id',$notation_id)->get()->toArray();
        //dd($items);
        
        $columns = array(
            'invoice',
            'conosament',
            'freight_sum',
            'line_id',
            'container_num',
            'seal',
            'container_type',
            'place',
            'gross',
            'cubic_meter',
            'cargo',
            'receiver_id',
            'free_days',
            'est_departure_date',
            'est_arrival_date',
            'transport_type',
            'documents',
            'details',
        );
        
        return view('notation', [
            'data' => $items,
            'notation_id' => $notation_id,
            'columns' => $columns
        ]);
    }
}
