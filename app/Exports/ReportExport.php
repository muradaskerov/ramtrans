<?php

namespace App\Exports;

use App\Notation;
use App\Order;
use App\Report;
use Maatwebsite\Excel\Concerns\FromView;
use \Request;
use Illuminate\Contracts\View\View;

class ReportExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $report_id = Request::get('report_id');

        $items = Report::whereKey($report_id)->with('orders','station')->first()->toArray();
//        dd($items);

        return view('report', [
            'data' => $items,
            'report_id' => $report_id,
        ]);
    }
}
