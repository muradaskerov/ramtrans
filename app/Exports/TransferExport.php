<?php

namespace App\Exports;

use App\Notation;
use App\Order;
use App\Report;
use Maatwebsite\Excel\Concerns\FromView;
use \Request;
use Illuminate\Contracts\View\View;

class TransferExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $report_id = Request::get('report_id');
        $customer_id = Request::get('customer_id');

        $items = Report::whereKey($report_id)->with(['transfers' => function ($q) use ($customer_id) {
            $q->where('customer_id', $customer_id);
        }])->first();

//        dd($items);

        if($items !== null){
            $items->toArray();
        }else{
            return false;
        }


        foreach($items['transfers'] as $key2 => $val2){
            $found_dem = false;
            $found_sum = false;
            foreach($val2['transfers'] as $transfer){
                $items['transfers'][$key2]['expense_'.$transfer['trnsf_trsp_type']] = (float)$transfer['trnsf_entered_amnt'];
                if($transfer['trnsf_trsp_type'] === 7){
                    $found_dem = true;
                }
                if($transfer['trnsf_trsp_type'] === 23){
                    $found_sum = true;
                }
            }
            if(!$found_dem){
                $items['transfers'][$key2]['expense_7'] = 0;
            }
            if(!$found_sum){
                $items['transfers'][$key2]['expense_23'] = 0;
            }
        }

//        dd($items);

        return view('balance', [
            'data' => $items,
            'report_id' => $report_id,
        ]);
    }
}
