<?php

namespace App\Http\Controllers;

use App\Data;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;

class DataController extends Controller
{

    public static function menuList()
    {
        return Data::menuList();
    }

    public static function lineList()
    {
        return Data::lineList();
    }

    public static function customer_list()
    {
        return Data::customer_list();
    }

    public static function terminal_list()
    {
        return Data::terminal_list();
    }

    public static function action_list()
    {
        return Data::action_list();
    }

    public static function tariff_list()
    {
        return Data::tariff_list();
    }
    public static function station_list()
    {
        return Data::station_list();
    }
    public static function cashier_types_list()
    {
        return Data::cashier_types_list();
    }
    public static function report_lists()
    {
        return Data::report_lists();
    }


}
