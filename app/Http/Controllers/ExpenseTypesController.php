<?php

namespace App\Http\Controllers;

use App\ExpenseTypes;
use Illuminate\Http\Request;
use Response;

class ExpenseTypesController extends Controller
{
    public static function expenseTypesMap(){
        return ExpenseTypes::orderByDesc("et_id")->get(['et_id as id','et_name as value'])->toJson();
    }

    public function addExpenseType(Request $request){
        $type = $request->get('expense_type');

        $et_id = ExpenseTypes::create([
            'et_name' => $type
        ])->et_id;

        $data = ExpenseTypes::orderByDesc("et_id")->get(['et_id as id','et_name as value']);

        return Response::json(array('success' => true,'data' => $data,'et_id' => $et_id), 200);

    }
}
