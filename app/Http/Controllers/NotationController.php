<?php

namespace App\Http\Controllers;

use App\Exports\NotationExport;
use App\Notation;
use App\Order;
use App\Report;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use \Response;

class NotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Notation::orderByDesc('notation_id')->with(['orders','creater'])->get()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Notation::create();

        return Response::json(array('success' => true,'data' => $data->notation_id), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Order::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $notation
     * @return \Illuminate\Http\Response
     */
    public function show(Order $notation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $notation
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $notation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $notation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $notation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $notation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notation $notation)
    {
        if($notation->created_by !== \Session::get('user')->user_id){
            return Response::json(array('success' => false,'message' =>'Вы не создали этот отчет. Поэтому вы не можете удалить.'), 200);

        }else{
            $notation->delete();
            return Response::json(array('success' => true), 200);
        }
    }


    public function addToReport(Request $request){
        $credentials = $request->all();
        $report_id = $credentials['report_id'];
        $orders = json_decode($credentials['orders'],true);
//        dd($orders);
        $serialized = json_decode($credentials['data'],true);
//        dd($serialized);

        foreach ($orders as $order){
            Order::whereKey($order['id'])->update([
               'report_id' => $report_id
            ]);
        }


//        dd($report_id);
        Report::whereKey($report_id)->update($serialized);
//        dd($serialized);
        return Response::json(array('success' => true), 200);
    }

    public function notationToExcel(Request $request){

        Excel::store(new NotationExport(), 'userfiles/access/notations.xls',"public");

        return \Response::json(array('success' => true,'url' => env("APP_URL")."/storage/userfiles/access/notations.xls"), 200);

    }
}
