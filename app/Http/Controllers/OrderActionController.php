<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderAction;
use Illuminate\Http\Request;
use Response;

class OrderActionController extends Controller
{
    public function getActions(){

    }

    public function store(Request $request)
    {
        $data = json_decode($request->get('formData'),true);
        $data['oa_order_id'] = $request->get('oa_order_id');

        $oa_id = OrderAction::create($data)->oa_id;

        Order::where("id",$data['oa_order_id'])->update(['oa_id' => $oa_id]);

        return Response::json(array('success' => true), 200);
    }
}
