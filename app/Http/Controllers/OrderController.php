<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Order;
use App\OrderAction;
use App\Tariff;
use Illuminate\Http\Request;
use \Response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd($request->all());
        $type = $request->get('type','today');
        if($type === 'today'){
            return Order::whereBetween('created_at',[date('Y-m-d').' 00:00:00',date('Y-m-d H:i:s')])->orderByDesc('id')->with(['lastAction'])->get()->toJson();
        }

        return Order::orderByDesc('id')->with(['lastAction'])->get()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $containers = json_decode($request->get('containers'),true);
        $data_order = json_decode($request->get('formData'),true);

        foreach ($containers as $container){
            unset($container['id']);

            $d = array_merge($container,$data_order);
            if(empty(trim($d['free_days']))){
                $d['free_days'] = null;
            }

            $invoice_id = Invoice::where('invoice_status','free')->first();


            if(empty($invoice_id)){
                $invoice_id = Invoice::create(['invoice_status' => 'free'])->invoice_id;
                $d['invoice'] = $invoice_id;
            }else{
                $d['invoice'] = $invoice_id->invoice_id;
            }

            $order_id = Order::create($d)->id;
            Invoice::whereKey($invoice_id)->update(['invoice_status' => 'used']);

            $data = [
                'oa_ac_id' => '1',
                'oa_start_date' => date('Y-m-d h:i:s'),
            ];
            $data['oa_order_id'] = $order_id;

            $oa_id = OrderAction::create($data)->oa_id;

            Order::where('id',$order_id)->update(['oa_id' => $oa_id]);
        }

        return Response::json(array('success' => true), 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
//        dd($request->all());
        $order->whereKey($request->get('id'))->update($request->except(
            [
                'last_action',
                'status',
                'first_period_demurrage',
                'first_period_amount',
                'second_period_demurrage',
                'second_period_amount',
                'third_period_demurrage',
                'third_period_amount',
                'sum_exp',
                'sum_demurrage',
                'sender_terminal',
                'container_terminal',
                'customer',
                'index',
                'type_order',
                'checked',
                'transfers',
            ]
        ));

        return Response::json(array('success' => true), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        Invoice::whereKey($order->invoice)->update(['invoice_status' => 'free']);
        $order->delete();
        return Response::json(array('success' => true), 200);

    }

    public function addToNotation(Request $request){
        $credentials = $request->all();
        $notation_id = $credentials['notation_id'];
        $serialized = json_decode($credentials['data'],true);
        foreach ($serialized as $order){
            Order::whereKey($order['id'])->update(['notation_id' => $notation_id]);
        }
        return Response::json(array('success' => true), 200);
    }

    public function getWithStatus(Request $request){
        $action = $request->get('action');

        return Order::orderByDesc('id')->with(['selectedaction'])->whereHas('selectedaction')->get()->toJson();
    }

    public function calculateDemurrage(Request $request){
        $futt = (int)$request->get('futt');
        $tar_id = explode(',',$request->get('tar_id'));
//        dd($tar_id);
        $line_id = $request->get('line_id');
        $remain_free_days = abs($request->get('remain_free_days'));
        $free_days = $request->get('free_days');


//        $allTariffs = Tariff::where('tar_line_id',$line_id)
//            ->where('tar_free_day',$free_days)
//            ->where('tar_container_type',$futt)
//            ->get(['tar_begin_day','tar_end_day','tar_price'])->toArray();

        $allTariffs = Tariff::whereIn('tar_id',$tar_id)
            ->get(['tar_begin_day','tar_end_day','tar_price'])->toArray();

//        dd($allTariffs);

        $first_price = 0;
        $second_price = 0;
        $third_price = 0;
        $first_period = 0;
        $second_period = 0;
        $third_period = 0;

        if(empty($allTariffs)){
            return Response::json(array(
                'success' => true,
                'first_price' => $first_price,
                'second_price' => $second_price,
                'third_price' => $third_price,
                'first_period' => $first_period,
                'second_period' => $second_period,
                'third_period' => $third_period
            ), 200);
        }



        if($allTariffs[0]['tar_end_day'] >= $remain_free_days){
            $first_price = $allTariffs[0]['tar_price']*$remain_free_days;
            $first_period = $remain_free_days;
        }else{
            $first_price = $allTariffs[0]['tar_price']*$allTariffs[0]['tar_end_day'];
            $first_period = $allTariffs[0]['tar_end_day'];

            $qaliq_1 = $remain_free_days-$allTariffs[0]['tar_end_day'];



            if(isset($allTariffs[1])){

                if($allTariffs[1]['tar_end_day'] >= $qaliq_1 || $allTariffs[1]['tar_end_day'] === null){
                    $second_price = $allTariffs[1]['tar_price']*$qaliq_1;
                    $second_period = $qaliq_1;
//                    dd($second_price);
                }else{

                    $second_price = $allTariffs[1]['tar_price']*$allTariffs[1]['tar_end_day'];
                    $second_period = $allTariffs[1]['tar_end_day'];
                    $qaliq_2 = $qaliq_1-$allTariffs[1]['tar_end_day'];

                    if(isset($allTariffs[2])){
                        $third_price = $allTariffs[2]['tar_price']*$qaliq_2;
                        $third_period = $qaliq_2;

                    }

                }
            }
        }

        return Response::json(array(
            'success' => true,
            'first_price' => $first_price,
            'second_price' => $second_price,
            'third_price' => $third_price,
            'first_period' => $first_period,
            'second_period' => $second_period,
            'third_period' => $third_period
        ), 200);


    }

    function addClient(Request $request){
        $orderIds = json_decode($request->get('orderIds'),true);
        $customer_id = $request->get('customer_id');

        foreach ($orderIds as $orderId){
            Order::whereKey($orderId)->update(['customer_id' => $customer_id]);
        }
        return Response::json(array('success' => true), 200);
    }
}
