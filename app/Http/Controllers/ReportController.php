<?php

namespace App\Http\Controllers;

use App\Exports\ReportExport;
use App\Exports\TransferExport;
use App\Report;
use Illuminate\Http\Request;
use \Response;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Report::orderByDesc('report_id')->with('notation','orders')->get()->toArray();

        foreach($data as $key => $val){
            foreach($val['orders'] as $key2 => $val2){
                foreach($val2['transfers'] as $transfer){
                    $data[$key]['orders'][$key2]['expense_'.$transfer['trnsf_trsp_type']] = (float)$transfer['trnsf_entered_amnt'];
                }
            }
        }
//        dd($data);

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
//        $notation_id = $request->get("notation_id");
        $data = Report::create(['notation_id' => 1]);

        return Response::json(array('success' => true,'data' => $data->report_id), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        $report->delete();
        return Response::json(array('success' => true), 200);
    }

    public function reportToExcel(Request $request){

        Excel::store(new ReportExport(), 'userfiles/access/report.xls',"public");

        return \Response::json(array('success' => true,'url' => env("APP_URL")."/storage/userfiles/access/report.xls"), 200);

    }
    public function balanceToExcel(Request $request){

        Excel::store(new TransferExport(), 'userfiles/access/balance.xls',"public");

        return \Response::json(array('success' => true,'url' => env("APP_URL")."/storage/userfiles/access/balance.xls"), 200);

    }
}
