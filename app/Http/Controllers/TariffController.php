<?php

namespace App\Http\Controllers;

use App\Exports\NotationExport;
use App\Notation;
use App\Order;
use App\Report;
use App\Tariff;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use \Response;

class TariffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tariff::all()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Notation::create();

        return Response::json(array('success' => true,'data' => $data->id), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        dd($request->all());
        Tariff::create($request->all());

        return Response::json(array('success' => true), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $notation
     * @return \Illuminate\Http\Response
     */
    public function show(Order $notation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $notation
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $notation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $notation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $notation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $notation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $notation)
    {
        //
    }

    public function addToReport(Request $request){
        $credentials = $request->all();
        $report_id = $credentials['report_id'];
        $serialized = json_decode($credentials['data'],true);
//        dd($serialized);

//        dd($report_id);
        Report::whereKey($report_id)->update($serialized);
//        dd($serialized);
        return Response::json(array('success' => true), 200);
    }

    public function notationToExcel(Request $request){

        Excel::store(new NotationExport(), 'userfiles/access/notations.xls',"public");

        return \Response::json(array('success' => true,'url' => env("APP_URL")."/storage/userfiles/access/notations.xls"), 200);

    }

    public function getTariffsByOrderID(Request $request){

        $futt = $request->get("futt");
        $line_id = $request->get("line_id");

        $data = Tariff::where("tar_line_id",$line_id)->where('tar_container_type',$futt)
            ->get(['tar_id as id',\DB::raw('CONCAT("$"," ",tar_price," (",tar_begin_day," - ",ifnull(tar_end_day,"∞"),")") as value')])->toArray();

        return \Response::json($data, 200);

    }
}
