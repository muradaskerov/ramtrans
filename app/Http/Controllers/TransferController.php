<?php

namespace App\Http\Controllers;

use App\CashReg;
use App\Exports\CashierExport;
use App\Exports\ExamsExport;
use App\Exports\TransferExport;
use App\Order;
use App\Report;
use App\Transfer;
use Carbon\Carbon;
use \Excel;
use Illuminate\Http\Request;
use \Response;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Order::with('sum_exp','sum_demurrage','customer','transfers','report')->whereHas('transfers')->get()->toArray();



        foreach($data as $key => $val){

            foreach($val['transfers'] as $transfer){
//                dd($transfer);
                $data[$key]['expense_'.$transfer['trnsf_trsp_type']] = (float)$transfer['trnsf_entered_amnt'].' '.$transfer['trnsf_entered_currency'];
            }

        }
//        dd($data);

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());

        $data = json_decode($request->get("expenses"), true);
        $currencies = json_decode($request->get("currencies"), true);

//        dd($currencies);
//        dd($selectedItem);
        foreach ($data as $order){
            Transfer::where("order_id", $order['id'])->delete();
//            dd($order);
            Order::whereKey($order['id'])->update(['customer_id' => $order['customer_id']]);

            foreach ($order as $key => $value){

                if(strpos($key,'expense') !== false){
//                    dd("tapdi");
                    $exploded = explode('_',$key);

                    $transfer = new Transfer();
                    $transfer->trnsf_trsp_type = $exploded[1];
                    $transfer->trnsf_entered_amnt = (float)$value;
                    $transfer->trnsf_entered_currency = $currencies[$key];
                    $transfer->trnsf_type = 'debit';
                    $transfer->order_id = $order['id'];
                    $transfer->save();



                }
            }
        }

        return Response::json(array('success' => true), 200);

    }



    public function addCredit(Request $request)
    {
        $expenses = json_decode($request->get("expenses"), true);

//        dd($expenses);

        if(empty($expenses['trnsf_cashier_type'])){
            $expenses['trnsf_cashier_type'] = null;
        }

        $this->initCashReg();

        $getLastCashRegData = CashReg::orderByDesc("cas_id")->first();

        switch ($expenses['trnsf_entered_currency']) {
            case "USD":
                if ($expenses['trnsf_type'] == 'credit') {
                    $getLastCashRegData->cas_usd += $expenses['trnsf_entered_amnt'];
                } else {
                    $getLastCashRegData->cas_usd += $expenses['trnsf_entered_amnt'];
                }

                break;
            case "AZN":
                if ($expenses['trnsf_type'] == 'credit') {
                    $getLastCashRegData->cas_azn += $expenses['trnsf_entered_amnt'];
                } else {
                    $getLastCashRegData->cas_azn -= $expenses['trnsf_entered_amnt'];
                }

                break;
            case "EUR":
                if ($expenses['trnsf_type'] == 'credit') {
                    $getLastCashRegData->cas_eur += $expenses['trnsf_entered_amnt'];
                } else {
                    $getLastCashRegData->cas_eur -= $expenses['trnsf_entered_amnt'];
                }
                break;
        }

        $getLastCashRegData->save();

        Transfer::create($expenses);
        return Response::json(array('success' => true), 200);

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Transfer $transfer
     * @return \Illuminate\Http\Response
     */
    public function show(Transfer $transfer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Transfer $transfer
     * @return \Illuminate\Http\Response
     */
    public function edit(Transfer $transfer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Transfer $transfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Order::whereKey($request->get('id'))->update(['customer_id' => $request->get('customer_id')]);
        return Response::json(array('success' => true), 200);
    }

    public function destroy(Transfer $transfer)
    {
        $transfer->delete();
        return Response::json(array('success' => true), 200);

    }


    public function getTransfersByOrderId(Request $request, Transfer $transfer)
    {
        $order_id = $request->get('order_id');

        return Response::json(array(
            'success' => true,
            'data' => $transfer
                ->where('order_id', $order_id)
                ->select(
                    [
                        'trnsf_trsp_type as expense_type',
                        'trnsf_entered_amnt as expense_value',
                        'trnsf_entered_currency as expense_currency',
                    ]
                )->get()->toArray()), 200);


    }

    public function test()
    {

    }

    public function getExpenses(Request $request, Transfer $transfer)
    {
        $this->initCashReg();

        $trnsf_type = $request->get('type');

        return Response::json(array(
            'success' => true,
            'data' => $transfer
                ->orderByDesc('trnsf_id')
                ->with('customer')
                ->where('trnsf_type', $trnsf_type)
                ->get()->toArray()), 200);


    }

    public function initCashReg()
    {
        $getLastCashRegData = CashReg::orderByDesc("cas_id")->first();

        if ($getLastCashRegData['cas_date'] != Carbon::today()) {
            $new_cash = new CashReg();
            $new_cash->cas_date = Carbon::today();
            $new_cash->cas_usd = $getLastCashRegData['cas_usd'];
            $new_cash->cas_azn = $getLastCashRegData['cas_azn'];
            $new_cash->cas_eur = $getLastCashRegData['cas_eur'];

            $new_cash->save();
        }
    }

    public function getAllCashReg(CashReg $cashReg){
        return Response::json($cashReg->orderByDesc("cas_id")->get()->toArray(), 200);
    }

    public function expensesToExcel(Request $request){
//        $items = json_decode($request->get("exams"),true);
//        dd($items);

        $result = Excel::store(new CashierExport(), 'userfiles/access/cashier_report.xls',"public");

//        dd($result);

        //return Excel::download(new ResultExport, 'invoices.xls');
        return \Response::json(array('success' => true,'url' => env("APP_URL")."/storage/userfiles/access/cashier_report.xls"), 200);

    }

    public function getInvoice(Request $request){
        $credentials = $request->all();
//        dd($credentials);
        $data = Transfer::whereKey($credentials['trnsf_id'])->with('customer','cash_type')->first()->toArray();
//        dd($data);
        if($data['trnsf_type'] == 'credit'){
            $view = view('medaxil', ["data" => $data]);

        }else{
            $view = view('mexaric', ["data" => $data]);
        }
        return response($view);
    }


}
