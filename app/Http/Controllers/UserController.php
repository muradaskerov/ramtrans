<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

use Session;

class UserController extends Controller
{

    public function logIn(Request $request)
    {
        $params = [];
        $response = [];

        $params['email'] = $request->post("login", null);

        $password = $request->post("password", null);

        if (!is_null($password) && !empty($password)) {
            $continue = true;
        } else {
            $continue = false;
            $response['type'] = 'error';
            $response['message'] = 'Пароль не введен !';
        }

        $params['password'] = $password;

        if ($continue) {
            $params['access_ip'] = $request->ip();
            $check = User::checkEmailData($params);

//            dd($check);

            if (!is_null($check) && $check->user_id > 0 ) {

                    $check->session_id = Session::getId();

                    $response['type'] = 'success';
                    $response['message'] = 'Вы вошли в систему !';

                    Session::put('user', $check);

                    $update_params = [];
                    $update_params['session_id'] = $check->session_id;
                    $update_params['user_id'] = Session::get('user')->user_id;

                    User::updateSessionId($update_params);
            } else {
                $response['type'] = 'error';
                $response['message'] = 'Электронная почта не доступна в базе данных !';
            }
        }

        return response()->json($response)->header('Content-Type', 'application/json');
    }

    public function logOut()
    {
        Session::flush();
        return response()->redirectTo("/");
    }

    public function menus()
    {
        $data = User::with('menus')->find(Session::get("user")->user_id);

        return response()->json($data->menus)->header('Content-Type', 'application/json');
    }


}
