<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Eloquent;

class Invoice extends Eloquent
{
    protected $primaryKey = "invoice_id";

    protected $fillable = [
        'invoice_status',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class,"invoice_id","invoice");
    }


}
