<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Eloquent;
class Menu extends Eloquent
{
    protected $primaryKey = 'menu_id';
    public $timestamps = false;
    protected $table = 'd_menu';


}
