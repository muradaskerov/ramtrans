<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Eloquent;
class Notation extends Eloquent
{
    protected $fillable = [
        'report_id',
        'created_by'
    ];

    protected $primaryKey = 'notation_id';

    public function __construct(array $attributes = array())
    {
        $this->setRawAttributes(array(
            'created_by' => \Session::get('user')->user_id,
        ), true);

        parent::__construct($attributes);
    }

    public function orders()
    {
        return $this->hasMany(Order::class,"notation_id","notation_id")->with('sum_exp','sum_demurrage','sender_terminal','container_terminal','customer');
    }

    public function creater()
    {
        return $this->hasOne(User::class,"usr_id","created_by")->selectRaw('usr_id, CONCAT(usr_name," ",usr_surname) as creator');
    }
}
