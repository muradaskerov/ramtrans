<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Eloquent;
class Order extends Eloquent
{
    protected $fillable = [
        'invoice',
        'conosament',
        'freight_sum',
        'line_id',
        'container_num',
        'seal',
        'container_type',
        'place',
        'gross',
        'cubic_meter',
        'cargo',
        'receiver_id',
        'free_days',
        'est_departure_date',
        'est_arrival_date',
        'transport_type',
        'documents',
        'details',
        'notation_id',
        'current_status',
        'consignee_id',
        'cartons',
        'weight',
        'weight_isized',
        'sender_terminal_id',
        'container_terminal_id',
        'arrival_date_to_poti_of_containers',
        'departure_date_from_poti_of_containers',
        'received_date_to_poti_of_documents',
        'sent_date_from_poti_of_documents',
        'release_date',
        'total_free_days',
        'remain_free_days',
        'past_free_days',
        'first_period_demurrage',
        'second_period_demurrage',
        'note_date',
        'reasoner_demurrage',
        'reason_demurrage_note',
        'arrival_date_to_baku',
        'discharge_date_at_baku',
        'returned_date',
        'tar_id',
        'report_id',
        'type_order',
        'customer_id'
        ];

    public function lastAction()
    {
        return $this->hasOne(OrderAction::class,"oa_id",'oa_id')->with('action');
    }

    public function selectedaction()
    {
        return $this->hasOne(OrderAction::class,"oa_id",'oa_id')->whereHas('selectedaction');
    }

    public function sum_exp()
    {
        return $this->hasOne(Transfer::class,"order_id","id")->where('trnsf_trsp_type',"<>",'7')->selectRaw('order_id,SUM(trnsf_entered_amnt) as total')
            ->groupBy('order_id');
    }

    public function sum_demurrage()
    {
        return $this->hasOne(Transfer::class,"order_id","id")->where('trnsf_trsp_type',"=",'7')->selectRaw('order_id,SUM(trnsf_entered_amnt) as total')
            ->groupBy('order_id');
    }

    public function sender_terminal()
    {
        return $this->hasOne(Terminal::class,"ter_id","sender_terminal_id");
    }

    public function container_terminal()
    {
        return $this->hasOne(Terminal::class,"ter_id","container_terminal_id");
    }
    public function customer()
    {
        return $this->hasOne(Customer::class,"cus_id","customer_id");
    }

    public function receiver()
    {
        return $this->hasOne(Customer::class,"cus_id","receiver_id");
    }

    public function transfers(){
        return $this->hasMany(Transfer::class,"order_id");
    }

    public function report()
    {
        return $this->belongsTo(Report::class,"report_id");
    }


}
