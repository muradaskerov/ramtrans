<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAction extends \Eloquent
{
    protected $primaryKey = 'oa_id';

    protected $fillable = [
        'oa_ac_id',
        'oa_order_id',
        'oa_start_date',
        'oa_end_date',
        'oa_note'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class,"oa_order_id",'id');
    }

    public function action()
    {
        return $this->hasOne(Action::class,"ac_id",'oa_ac_id');
    }

    public function selectedaction()
    {

//        dd(\Request::get("action"));

        return $this->hasOne(Action::class,"ac_id",'oa_ac_id')->whereKey(\Request::get("action"));
    }

}
