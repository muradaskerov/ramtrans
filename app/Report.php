<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Eloquent;

class Report extends Eloquent
{
    protected $primaryKey = "report_id";

    protected $fillable = [
        'station_id',
        'transoil',
        'transport_num',
        'date_notation',
        'notation_id',
        'transport_type',
    ];

    public function notation()
    {
        return $this->hasOne(Notation::class,"notation_id","notation_id")->with('orders');
    }

    public function station()
    {
        return $this->hasOne(Station::class,"sta_id","station_id");
    }

    public function orders()
    {
        return $this->hasMany(Order::class,"report_id")->with('sum_exp','sum_demurrage','sender_terminal','container_terminal','customer','transfers');
    }

    public function transfers()
    {
        return $this->hasMany(Order::class,"report_id")->with('customer','transfers');
    }


}
