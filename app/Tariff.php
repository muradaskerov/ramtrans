<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tariff extends \Eloquent
{
    protected $table = 'tariffs';

    protected $primaryKey = "tar_id";

    public $timestamps = false;

    protected $fillable = [
        'tar_price',
        'tar_line_id',
        'tar_container_type',
        'tar_free_day',
        'tar_begin_day',
        'tar_end_day',
    ];



}
