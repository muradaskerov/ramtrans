<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminal extends \Eloquent
{
    protected $table = 'terminals';

    protected $primaryKey = "ter_id";

    public $timestamps = false;



}
