<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends \Eloquent
{
    protected $table = 'd_transfers';

    protected $fillable = [
        'trnsf_cus_id',
        'trnsf_type',
        'trnsf_entered_amnt',
        'trnsf_entered_currency',
        'trnsf_transfer_ts',
        'trnsf_note',
        'trnsf_paid_amnt',
        'trnsf_trnsp_id',
        'trnsf_recidue_amnt',
        'trnsf_create_usr_id',
        'trnsf_update_usr_id',
        'trnsf_trsp_type',
        'order_id',
        'trnsf_cashier_type'
    ];

    protected $primaryKey = "trnsf_id";

    public function invoices()
    {
        return $this->belongsTo('Invoices');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class,"trnsf_cus_id","cus_id");
    }

    public function cash_type()
    {
        return $this->hasOne(CashierType::class,"cast_id","trnsf_cashier_type");
    }



}
