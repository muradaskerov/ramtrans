<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \Eloquent
{
    protected $primaryKey = 'usr_id';

    public static function checkEmailData($params){

        $sql = "
                SELECT
                usr_id AS user_id,
                usr_name,usr_surname,usr_excluded
                FROM users
                WHERE
                   usr_login = :login
                   AND usr_password = :usr_password
             ";

        $bind = array(
            'login' => $params['email'],
            'usr_password' => $params['password'],
        );

        return \DB::selectOne($sql, $bind);

    }


    public static function updateSessionId($params){

        $sql = "UPDATE users
                SET usr_session_id = :session_id
                WHERE usr_id = :user_id";

        return \DB::update($sql, $params);
    }

    public function menus()
    {
        return $this->hasManyThrough(Menu::class,UserMenu::class,"um_usr_id","menu_id","usr_id","um_menu_id")->select(
            [
                'menu_view AS id',
                'menu_value AS value',
                'menu_layout AS layout',
                'menu_icon AS icon'
            ]
        );
    }

}
