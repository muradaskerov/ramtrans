<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserMenu extends \Eloquent
{
    protected $primaryKey = 'um_id';

    protected $table = 'user_menu';
    public $timestamps = false;


}
