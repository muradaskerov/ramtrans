<?php

use Faker\Generator as Faker;
use \Illuminate\Support\Str;

$factory->define(\App\Order::class, function (Faker $faker) {
    return [
        'invoice' => random_int(9999,999999),
        'conosament' => Str::random(6),
        'freight_sum' => Str::random(3),
        'line_id' => random_int(1,500),
        'container_num' => random_int(11111,999999),
        'seal'=> random_int(1,500),
        'container_type' => '20',
        'cubic_meter' => random_int(1,500),
        'receiver_id' => random_int(1,500),
        'free_days' => random_int(1,50),
        'est_departure_date' => date("Y-m-d h:i:s", mt_rand(1, time())),
        'est_arrival_date' => date("Y-m-d h:i:s", mt_rand(1, time())),
        'transport_type' => 'ship',
    ];
});
