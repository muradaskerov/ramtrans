<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_menu', function (Blueprint $table) {
            $table->increments('menu_id');
            $table->integer('menu_role_id')->nullable();
            $table->string('menu_value',60)->nullable();
            $table->string('menu_view',45)->nullable();
            $table->string('menu_layout',45)->nullable();
            $table->string('menu_icon',45)->nullable();
            $table->integer('menu_priority')->nullable();
            $table->enum('menu_status',['on','off'])->default('on');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_menu');
    }
}
