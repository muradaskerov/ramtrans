<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice');
            $table->string("conosament",20)->nullable();
            $table->string("freight_sum")->nullable();
            $table->integer("line_id");
            $table->string("container_num")->nullable();
            $table->integer("seal")->nullable();
            $table->enum("container_type",[20,40,80])->nullable();
            $table->integer("place")->nullable();
            $table->string("gross")->nullable();
            $table->string("cubic_meter")->nullable();
            $table->text("cargo")->nullable();
            $table->integer("receiver_id");
            $table->integer("free_days")->nullable();
            $table->timestamp("est_departure_date")->nullable();
            $table->timestamp("est_arrival_date")->nullable();
            $table->enum("transport_type",['ship','bus','train']);
            $table->text("documents")->nullable();
            $table->text("details")->nullable();
            $table->integer("notation_id")->nullable();
            $table->enum("current_status",['waiting'])->default('waiting')->nullable();
            $table->integer('consignee_id')->nullable();
            $table->integer('cartons')->nullable();
            $table->string('weight')->nullable();
            $table->string('weight_isized')->nullable();
            $table->integer('sender_terminal_id')->nullable();
            $table->integer('container_terminal_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
