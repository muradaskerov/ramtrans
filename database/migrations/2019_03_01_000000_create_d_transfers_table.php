<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDTransfersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'd_transfers';

    /**
     * Run the migrations.
     * @table d_transfers
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('trnsf_id');
            $table->integer('trnsf_trnsp_id')->nullable()->default(null);
            $table->integer('trnsf_bacc_id')->nullable()->default(null);
            $table->integer('trnsf_cus_id')->nullable()->default(null);
            $table->integer('trnsf_payer_cus_id')->nullable()->default(null);
            $table->string('trnsf_transfer_code', 50)->nullable()->default(null);
            $table->enum('trnsf_type', ['credit', 'debit'])->nullable()->default(null);
            $table->decimal('trnsf_bacc_amnt', 18, 4)->nullable()->default(null)->comment('Amount Account Currency');
            $table->decimal('trnsf_entered_amnt', 18, 4)->nullable()->default(null);
            $table->enum('trnsf_entered_currency', ['EUR', 'USD', 'AZN', 'RUB', 'TRY', 'GBP'])->nullable()->default(null);
            $table->timestamp('trnsf_transfer_ts')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('trnsf_note')->nullable()->default(null);
            $table->integer('trnsf_trsp_type')->nullable()->default(null);
            $table->decimal('trnsf_paid_amnt', 18, 4)->nullable()->default(null);
            $table->decimal('trnsf_recidue_amnt', 18, 4)->nullable()->default(null);
            $table->integer('trnsf_create_usr_id')->nullable()->default(null);
            $table->integer('trnsf_update_usr_id')->nullable()->default(null);
            $table->timestamps();

            $table->index(["trnsf_create_usr_id"], 'trnsf_create_usr_id');

            $table->index(["trnsf_bacc_id"], 'trnsf_bacc_id');

            $table->index(["trnsf_transfer_ts"], 'trnsf_transfer_ts');

            $table->index(["trnsf_recidue_amnt"], 'index_trnsf_recidue_amnt');

            $table->index(["trnsf_update_usr_id"], 'trnsf_update_usr_id');

            $table->index(["trnsf_bacc_id", "trnsf_cus_id", "trnsf_transfer_ts"], 'trnsf_bacc_id_3');

            $table->index(["trnsf_bacc_id", "trnsf_cus_id", "trnsf_trnsp_id", "trnsf_transfer_ts"], 'trnsf_bacc_id_2');

            $table->index(["trnsf_trnsp_id"], 'd_transfers_ibfk_1');

            $table->index(["trnsf_cus_id"], 'trnsf_cus_id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
