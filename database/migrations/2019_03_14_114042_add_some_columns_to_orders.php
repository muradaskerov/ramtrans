<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeColumnsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->timestamp("arrival_date_to_poti_of_containers")->nullable();
            $table->timestamp("departure_date_from_poti_of_containers")->nullable();
            $table->timestamp("received_date_to_poti_of_documents")->nullable();
            $table->timestamp("sent_date_from_poti_of_documents")->nullable();
            $table->timestamp("release_date")->nullable();
            $table->integer("total_free_days")->nullable();
            $table->integer("remain_free_days")->nullable();
            $table->integer("past_free_days")->nullable();
            $table->integer("first_period_demurrage")->nullable();
            $table->integer("second_period_demurrage")->nullable();
            $table->timestamp("note_date")->nullable();
            $table->enum("reasoner_demurrage",['client','rtg','other'])->nullable();
            $table->string("reason_demurrage_note")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn("arrival_date_to_poti_of_containers");
            $table->dropColumn("departure_date_from_poti_of_containers");
            $table->dropColumn("received_date_to_poti_of_documents");
            $table->dropColumn("sent_date_from_poti_of_documents");
            $table->dropColumn("release_date");
            $table->dropColumn("total_free_days");
            $table->dropColumn("remain_free_days");
            $table->dropColumn("past_free_days");
            $table->dropColumn("first_period_demurrage");
            $table->dropColumn("second_period_demurrage");
            $table->dropColumn("note_date");
            $table->dropColumn("reasoner_demurrage");
            $table->dropColumn("reason_demurrage_note");
        });
    }
}
