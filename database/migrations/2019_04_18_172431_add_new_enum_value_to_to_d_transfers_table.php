<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewEnumValueToToDTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('d_transfers', function(Blueprint $table)
        {
            $table->dropColumn('trnsf_type');
        });


        Schema::table('d_transfers', function (Blueprint $table) {
            $table->enum('trnsf_type', ['credit', 'debit','export'])->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('d_transfers', function(Blueprint $table)
        {
            $table->dropColumn('trnsf_type');
        });
    }
}
