<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->timestamp("arrival_date_to_baku")->nullable();
            $table->timestamp("discharge_date_at_baku")->nullable();
            $table->timestamp("returned_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn("arrival_date_to_baku");
            $table->dropColumn("discharge_date_at_baku");
            $table->dropColumn("returned_date");
        });
    }
}
