<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashregTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashreg', function (Blueprint $table) {
            $table->increments('cas_id');
            $table->timestamp('cas_date');
            $table->float('cas_usd');
            $table->float('cas_azn');
            $table->float('cas_eur');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashreg');
    }
}
