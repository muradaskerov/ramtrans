<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('usr_id');
            $table->string('usr_email',50);
            $table->string('usr_surname',50);
            $table->string('usr_name',50);
            $table->string('usr_father_name',50)->nullable();
            $table->string('usr_login',50);
            $table->string('usr_password');
            $table->string('usr_session_id');
            $table->tinyInteger('usr_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
