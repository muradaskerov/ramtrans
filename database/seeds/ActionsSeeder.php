<?php

use Illuminate\Database\Seeder;

class ActionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("actions")->insert( [
            'ac_name'=>'Yolda',
        ] );

        DB::table("actions")->insert( [
            'ac_name'=>'Potiyə çatıb',
        ] );

        DB::table("actions")->insert( [
            'ac_name'=>'İrana çatıb',
        ] );

        DB::table("actions")->insert( [
            'ac_name'=>'Bakıya gəlir',
        ] );

        DB::table("actions")->insert( [
            'ac_name'=>'Bakıya çatıb',
        ] );
    }
}
