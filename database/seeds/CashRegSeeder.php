<?php

use Illuminate\Database\Seeder;

class CashRegSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("cashreg")->insert( [
            'cas_date'=>\Carbon\Carbon::today(),
            'cas_azn'=>'0',
            'cas_usd'=>'0',
            'cas_eur'=>'0',
        ] );
    }
}
