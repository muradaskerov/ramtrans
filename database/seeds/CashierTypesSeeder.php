<?php

use Illuminate\Database\Seeder;


define("CASHTYPES",array(
    'qapıdan-qapıya xərci',
    'terminal xərci',
    'boş geri qayitma',
    'sürücü',
    'Ehtiyat pul',
    'market xərci',
    'komunal',
    'Poti pul köçürmə',
    'Poti pul köçürmə faizi',
    'Fraxt',
    'Fraxt faizi',
    'koba',
    'RTG işçilərinin əmək haqqına əlavə',
    'Tərəzi (Ge)',
    'Tərəzi (Az)',
    'Dəmir yolu nümayəndə',
    'prastoy',
    'ADY konteyner',
    'ADY express',
    'M-Line',
    'Ramin m',
    'borc',
    'ezamiyyət',
    'vaqon',
));


class CashierTypesSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (CASHTYPES as $CASHTYPES){
            DB::table('cashier_types')->insert([
                'cast_name' => $CASHTYPES,
            ]);
        }
    }
}
