<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(OrdersTableSeeder::class);
         //$this->call(ExpenseTypesTableSeeder::class);
//         $this->call(MenuTableSeeder::class);
//         $this->call(ActionsSeeder::class);
//         $this->call(LinesSeeder::class);
//         $this->call(TariffSeeder::class);
//         $this->call(CashRegSeeder::class);
//         $this->call(TerminalSeeder::class);
//         $this->call(StationSeeder::class);
         $this->call(CashierTypesSeeder::class);
    }
}
