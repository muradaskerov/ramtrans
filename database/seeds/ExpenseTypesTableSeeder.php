<?php

use Illuminate\Database\Seeder;

define("EXPENSES",array(
    'Inspection  fee destination',
    'ТНС',
    'МН',
    'оформление док-тов в линии',
    'Хранен. Конт.',
    'Демередж',
    'перетарка',
    'Ж.Д. Грузия',
    'тупик',
    'Пломб',
    'Авто-перевозка между терминалами',
    'талманирование',
    'Прочие расходы ( cesh )',
    'письмо в ж/д департамент',
    'дополнительное укрепление  (пена,  селафан, гвозд)',
    'вирамаина',
    'перевеска контейнеров',
    'Стархов. Конт.',
    'Итого расходов в Грузии',
    'Итого расходов в Азербайджане',
    'Общая сумма расходов',
    'Стоимость для клиента',

    'фрахт',
    'VAT',
    'таможеный сбор',
    'перетарка',
    'Авто-перевозка Poti-Baku-Poti',
    'КК',
    'Таможенный пункт БК',
    'SM',

    'port facility security',
    'Ж.Д. Грузия пустои',
    'Ж.Д. Азербайджан  пустои ',

    'DHL',
));

class ExpenseTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (EXPENSES as $EXPENS){
            DB::table('expense_types')->insert([
                'et_name' => $EXPENS,
            ]);
        }

    }
}
