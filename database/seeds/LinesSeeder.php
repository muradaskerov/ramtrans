<?php

use Illuminate\Database\Seeder;

class LinesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table("lines")->insert( [
            'line_name'=>'CMA CGM',
        ] );

        DB::table("lines")->insert( [
            'line_name'=>'MSC',
        ] );

        DB::table("lines")->insert( [
            'line_name'=>'MAERSK',
        ] );

        DB::table("lines")->insert( [
            'line_name'=>'HAPAG_LLOYD',
        ] );

        DB::table("lines")->insert( [
            'line_name'=>'EVERGREEN',
        ] );

        DB::table("lines")->insert( [
            'line_name'=>'ARKAS',
        ] );

        DB::table("lines")->insert( [
            'line_name'=>'ZIM',
        ] );

        DB::table("lines")->insert( [
            'line_name'=>'CHINE LINE (JNS. COSCO)',
        ] );

        DB::table("lines")->insert( [
            'line_name'=>'WILHELMSEN',
        ] );




    }
}
