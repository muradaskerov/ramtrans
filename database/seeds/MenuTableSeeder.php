<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


//        DB::table("d_menu")->insert( [
//            'menu_role_id'=>NULL,
//            'menu_value'=>'Sifarişlər',
//            'menu_view'=>'orders',
//            'menu_layout'=>'orders_layout',
//            'menu_icon'=>'mdi mdi-truck',
//            'menu_priority'=>NULL,
//            'menu_status'=>'on',
//            'created_at'=>NULL,
//            'updated_at'=>NULL
//        ] );
//
//
//
//        DB::table("d_menu")->insert( [
//            'menu_role_id'=>NULL,
//            'menu_value'=>'Raportlar',
//            'menu_view'=>'notations',
//            'menu_layout'=>'notations_layout',
//            'menu_icon'=>'mdi mdi-file',
//            'menu_priority'=>NULL,
//            'menu_status'=>'on',
//            'created_at'=>NULL,
//            'updated_at'=>NULL
//        ]);
//
//
//
//        DB::table("d_menu")->insert( [
//            'menu_role_id'=>NULL,
//            'menu_value'=>'Hesabatlar',
//            'menu_view'=>'reports',
//            'menu_layout'=>'reports_layout',
//            'menu_icon'=>'mdi mdi-file-document-box',
//            'menu_priority'=>NULL,
//            'menu_status'=>'on',
//            'created_at'=>NULL,
//            'updated_at'=>NULL
//        ] );
//
//        DB::table("d_menu")->insert( [
//            'menu_role_id'=>NULL,
//            'menu_value'=>'Müştərilər',
//            'menu_view'=>'customers',
//            'menu_layout'=>'customers_layout',
//            'menu_icon'=>'mdi mdi-account-multiple',
//            'menu_priority'=>NULL,
//            'menu_status'=>'on',
//            'created_at'=>NULL,
//            'updated_at'=>NULL
//        ] );
//
//        DB::table("d_menu")->insert( [
//            'menu_role_id'=>NULL,
//            'menu_value'=>'Tracking',
//            'menu_view'=>'tracking',
//            'menu_layout'=>'tracking_layout',
//            'menu_icon'=>'mdi mdi-crosshairs-gps',
//            'menu_priority'=>NULL,
//            'menu_status'=>'on',
//            'created_at'=>NULL,
//            'updated_at'=>NULL
//        ] );
//
//        DB::table("d_menu")->insert( [
//            'menu_role_id'=>NULL,
//            'menu_value'=>'Kassa',
//            'menu_view'=>'cashier',
//            'menu_layout'=>'cashier_layout',
//            'menu_icon'=>'mdi mdi-cash-multiple',
//            'menu_priority'=>NULL,
//            'menu_status'=>'on',
//            'created_at'=>NULL,
//            'updated_at'=>NULL
//        ] );

        DB::table("d_menu")->insert( [
            'menu_role_id'=>NULL,
            'menu_value'=>'Tariflər',
            'menu_view'=>'tariffs',
            'menu_layout'=>'tariffs_layout',
            'menu_icon'=>'mdi mdi-view-week',
            'menu_priority'=>NULL,
            'menu_status'=>'on',
            'created_at'=>NULL,
            'updated_at'=>NULL
        ] );




    }
}
