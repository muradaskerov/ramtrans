<?php

use Illuminate\Database\Seeder;

class StationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("stations")->insert( [
            'sta_name'=>'Gəncə',
        ] );
    }
}
