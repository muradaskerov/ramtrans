<?php

use Illuminate\Database\Seeder;

class TariffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table("tariffs")->insert( [
            'tar_line_id'=>'1',
            'tar_free_day'=>'14',
            'tar_price'=>'16',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'15',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'1',
            'tar_free_day'=>'14',
            'tar_price'=>'31',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'15',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'1',
            'tar_free_day'=>'14',
            'tar_price'=>'27',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'16',
            'tar_end_day'=>null,
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'1',
            'tar_free_day'=>'14',
            'tar_price'=>'52',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'16',
            'tar_end_day'=>null,
        ] );

        //MSC

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'2',
            'tar_free_day'=>'7',
            'tar_price'=>'20',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'7',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'2',
            'tar_free_day'=>'7',
            'tar_price'=>'30',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'7',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'2',
            'tar_free_day'=>'7',
            'tar_price'=>'40',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'8',
            'tar_end_day'=>null,
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'2',
            'tar_free_day'=>'7',
            'tar_price'=>'58',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'8',
            'tar_end_day'=>null,
        ] );

        //MAERSK

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'3',
            'tar_free_day'=>'4',
            'tar_price'=>'20',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'4',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'3',
            'tar_free_day'=>'4',
            'tar_price'=>'40',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'4',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'3',
            'tar_free_day'=>'4',
            'tar_price'=>'40',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'5',
            'tar_end_day'=>null,
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'3',
            'tar_free_day'=>'4',
            'tar_price'=>'60',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'5',
            'tar_end_day'=>null,
        ] );

        //HAPAG

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'4',
            'tar_free_day'=>'10',
            'tar_price'=>'15',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'7',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'4',
            'tar_free_day'=>'10',
            'tar_price'=>'30',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'7',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'4',
            'tar_free_day'=>'10',
            'tar_price'=>'20',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'8',
            'tar_end_day'=>'13',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'4',
            'tar_free_day'=>'10',
            'tar_price'=>'40',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'8',
            'tar_end_day'=>'13',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'4',
            'tar_free_day'=>'10',
            'tar_price'=>'30',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'14',
            'tar_end_day'=>null,
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'4',
            'tar_free_day'=>'10',
            'tar_price'=>'60',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'14',
            'tar_end_day'=>null,
        ] );

        //EVERGREEN

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'5',
            'tar_free_day'=>'10',
            'tar_price'=>'8',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'10',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'5',
            'tar_free_day'=>'10',
            'tar_price'=>'16',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'10',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'5',
            'tar_free_day'=>'14',
            'tar_price'=>'8',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'6',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'5',
            'tar_free_day'=>'14',
            'tar_price'=>'16',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'6',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'5',
            'tar_free_day'=>'21',
            'tar_price'=>'16',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>null
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'5',
            'tar_free_day'=>'21',
            'tar_price'=>'32',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>null,
        ] );

        //ARKAS

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'6',
            'tar_free_day'=>'7',
            'tar_price'=>'22',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'7',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'6',
            'tar_free_day'=>'7',
            'tar_price'=>'44',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'7',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'6',
            'tar_free_day'=>'7',
            'tar_price'=>'44',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'8',
            'tar_end_day'=>null,
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'6',
            'tar_free_day'=>'7',
            'tar_price'=>'88',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'8',
            'tar_end_day'=>null,
        ] );

        //ZIM

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'7',
            'tar_free_day'=>'11',
            'tar_price'=>'22.5',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'7',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'7',
            'tar_free_day'=>'11',
            'tar_price'=>'35',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'7',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'7',
            'tar_free_day'=>'11',
            'tar_price'=>'27.5',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'8',
            'tar_end_day'=>'15',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'7',
            'tar_free_day'=>'11',
            'tar_price'=>'45',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'8',
            'tar_end_day'=>'15',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'7',
            'tar_free_day'=>'11',
            'tar_price'=>'32.5',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'15',
            'tar_end_day'=>null,
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'7',
            'tar_free_day'=>'11',
            'tar_price'=>'55',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'15',
            'tar_end_day'=>null,
        ] );

        //COSCO

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'8',
            'tar_free_day'=>'10',
            'tar_price'=>'15',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>null,
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'8',
            'tar_free_day'=>'10',
            'tar_price'=>'30',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>null,
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'8',
            'tar_free_day'=>'21',
            'tar_price'=>'30',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>null,
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'8',
            'tar_free_day'=>'21',
            'tar_price'=>'60',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>null,
        ] );

        //COSCO

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'9',
            'tar_free_day'=>'10',
            'tar_price'=>'14',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'7',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'9',
            'tar_free_day'=>'10',
            'tar_price'=>'28',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'1',
            'tar_end_day'=>'7',
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'9',
            'tar_free_day'=>'10',
            'tar_price'=>'28',
            'tar_container_type'=>'20',
            'tar_begin_day'=>'8',
            'tar_end_day'=>null,
        ] );

        DB::table("tariffs")->insert( [
            'tar_line_id'=>'9',
            'tar_free_day'=>'10',
            'tar_price'=>'56',
            'tar_container_type'=>'40',
            'tar_begin_day'=>'8',
            'tar_end_day'=>null,
        ] );




    }
}
