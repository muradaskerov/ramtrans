<?php

use Illuminate\Database\Seeder;

class TerminalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("terminals")->insert( [
            'ter_name'=>'GTE',
        ] );

        DB::table("terminals")->insert( [
            'ter_name'=>'almari',
        ] );

        DB::table("terminals")->insert( [
            'ter_name'=>'barvili',
        ] );

        DB::table("terminals")->insert( [
            'ter_name'=>'cavtrexsi',
        ] );

        DB::table("terminals")->insert( [
            'ter_name'=>'ESPERENASI',
        ] );

        DB::table("terminals")->insert( [
            'ter_name'=>'KAISA-2',
        ] );

    }
}
