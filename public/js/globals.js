define(["module"], function (module) {

    var globals = [];

    var data_list = module.config().data_list;
    const url = module.config().url;
    // console.log(url);

    for(var i=0; i<data_list.length; i++){
        var data_list_value = data_list[i];
        if(module.config()[data_list_value] != undefined){
            globals[data_list_value] = JSON.parse(module.config()[data_list_value]);
        }
    }

    globals['url'] = url;
    globals['user_fullname'] = module.config().user_fullname;
    globals['usr_excluded'] = module.config().usr_excluded;

    globals.resetMasterValues =  function (globals){
        var a = module.config().resetMasterValues(globals);
    }

    return globals;
});
