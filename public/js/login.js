define(["views/login", "globals"],
    function (app, globals) {

        webix.i18n.locales["az"] = {   //"es-ES" - the locale name, the same as the file name
            groupDelimiter: " ",       //a mark that divides numbers with many digits into groups
            groupSize: 3,              //the number of digits in a group
            decimalDelimiter: ",",     //the decimal delimiter
            decimalSize: 2,            //the number of digits after the decimal mark

            //applied to columns with 'format:webix.i18n.dateFormatStr'
            dateFormat: "%d/%m/%Y",
            //applied to columns with 'format:webix.i18n.dateFormatStr'
            timeFormat: "%H:%i",
            //applied to columns with 'format:webix.i18n.longDateFormatStr'
            longDateFormat: "%d %F %Y",
            //applied to cols with 'format:webix.i18n.fullDateFormatStr'
            fullDateFormat: "%d/%m/%Y %H:%i",
            am: null,
            pm: null,
            //€ - currency name. Applied to cols with 'format:webix.i18n.priceFormat'
            price: "{obj} AZN",
            calendar: {
                monthFull: ["Yanvar", "Fevral", "Mart", "Aprel", "May", "İyun", "İyul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"],
                monthShort: ["Yan", "Fev", "Mar", "Apr", "May", "İyun", "İyul", "Avq", "Sen", "Okt", "Noy", "Dek"],
                dayFull: ["Bazar günü", "Bazar ertəsi", "Çərşənbə axşamı", "Çərşənbə", "Cümə axşamı", "Cümə", "Şənbə"],
                dayShort: ["baz", "ber", "çax", "çər", "cax", "cüm", "şnb"]
            },
            hours: "Saat ",
            minutes: "Dəqiqə ",
            done: "Saniyə",

            controls: {
                select: "Seçin"
            }
        };

        webix.editors.$popup.date = {
            view: "popup",
            body: {
                view: "calendar",
                timepicker: true,
                weekNumber: true,
                borderless: true,
                calendarDateFormat: "%d/%m/%Y %H:%i",
                width: 300,
                height: 250
            }
        };




        webix.i18n.setLocale("az");
        webix.i18n.parseFormat = "%Y-%m-%d %H:%i";

        webix.extend(app, webix.EventSystem);
        webix.ready(app.init);

        return app;
    }
);
