define(function (require) {
    var app = require('app');

    require([
        'views/main'
    ], function (ui) {
        app.ui = ui;
        webix.ready(app.init);
    });
});
