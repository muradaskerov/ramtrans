define(
    [
        "globals",
        "windows/add_new_expenses",
        "windows/invoice_window",
    ],
    function (globals) {



        function downloadCashierListButton(){
            var items = $$("cashier_datatable").serialize();
            webix.ajax().post("/expensesToExcel", {
                expenses: items,
            }, function (text, data, xhr) {

                var result = data.json();

                // window.location= result.url;
                console.log(result)

                var url = result.url;
                location.assign(result.url);
                // var url = new URL(url);
                // var doc = window.open(url.href);

                // var url = result.url;
                // var url = new URL(url);
                // url.searchParams.set('doc_type', 'pdf');
                // var doc = window.open(url.href);
                // doc.print();

                // webix.message(app.labels.operation_successful);

            });
        }

        var table = {
            id: "cashier_datatable",
            view: "datatable",
            // navigation: true,
            select: "row",
            // pager: "cashier_pager",
            columns: [
                {id: "trnsf_id", header: "İD"},
                // {id: "invoice_number", header: "İnvovs nömrəsi",fillspace:true,},
                {id: "trnsf_cus_id",collection:globals.customer_list, header: [
                        {text: 'Müştəri'},
                        {content:"multiComboFilter"},
                    ], fillspace:true},
                {id: "trnsf_cashier_type",collection:globals.cashier_types_list, header: [
                        {text: 'Tip'},
                        {content:"multiComboFilter"},
                    ], fillspace:true},
                {id: "trnsf_entered_amnt", header: "Məbləğ"},
                {id: "trnsf_entered_currency",
                    collection:['AZN','USD','EUR'],
                    header: [
                        {text: 'Valyuta'},
                        {content:"richSelectFilter"},

                    ]
                },
                {id: "trnsf_transfer_ts",
                    format:webix.Date.dateToStr("%d-%m-%Y"),
                    map:"(date)#trnsf_transfer_ts#",
                    header: [
                        {text: 'Дата'},
                        {content: "dateRangeFilter"}
                    ],
                },
                {id: "trnsf_note", header: "Qeyd",fillspace:true,},
                {
                    id: "download_invoice",
                    header: {text: "Qəbz", css: {"text-align": "center"}},
                    css: {"text-align": "center"},
                    template: function (obj) {
                        // //////////console.log(obj);
                        let str = "<a href=''><span class='download_invoice doc_file mdi mdi-file'></span></a>";
                        return str;
                    },
                },
                {
                    id: "remove",
                    header: {text: "Удалить", css: {"text-align": "center"}},
                    css: {"text-align": "center",'font-size': "22px",    "color": "#279837 !important"},
                    template: function (obj) {
                        // //////////console.log(obj);
                        let str = "<a href=''><span class='remove doc_file mdi mdi-delete'></span></a>";
                        return str;
                    },
                },
                // {
                //     id: "download_certificate",
                //     header: {text: "Sertifikat", css: {"text-align": "center"}},
                //     css: {"text-align": "center"},
                //     template: function (obj) {
                //         // //////////console.log(obj);
                //         let str = "<a href=''><span class='download_certificate doc_file mdi mdi-file'></span></a>";
                //         return str;
                //     },
                // },
            ],

            on:{
                onBeforeLoad: function () {
                    // if($$("segmented_filter").getValue() === "waiting"){
                    //     $$("cashier_datatable").hideColumn("download_certificate");
                    // }else{
                    //     $$("cashier_datatable").showColumn("download_certificate");
                    //
                    // }
                    this.showOverlay("Hazırlanır...");
                },
                onAfterLoad: function () {
                    if (!this.count()) {
                        this.showOverlay("Bu seçimlərə uyğun məlumat yoxdur.");
                    } else {
                        this.hideOverlay();
                    }
                },
                onItemDblClick: function (id, e, node) {
                    // globals.ex_id = $$(this).getItem(id.row).ex_id;
                    // globals.ex_status = $$(this).getItem(id.row).ex_status;
                    // globals.surveys = $$(this).getItem(id.row).surveys;

                },
                onSubViewCreate: function (view, item) {
                    console.log(item);
                    view.parse(item.orders);
                },
            },
            onClick: {
                "download_invoice": function (ev, id) {
                    ev.preventDefault();
                    var trnsf_id = this.getItem(id.row).trnsf_id;
                    globals.trnsf_id = trnsf_id;
                    $$("show_results_view").parse({trnsf_id: trnsf_id});
                    $$("invoice_window").show();
                    return false; // blocks the default click behavior
                },
                "remove": function (ev, id) {
                    ev.preventDefault();
                    var trnsf_id = this.getItem(id.row).trnsf_id;

                    // console.log(item)

                    webix.confirm({
                        title: "Предупреждение!",
                        type: "confirm-warning",
                        text: "Вы уверены, что хотите удалить?",
                        ok: "Да!",
                        cancel: "Нет",
                        width: 350,
                        callback: function (result) {
                            if (result) {
                                //console.log()
                                webix.ajax().del("/transfers/"+trnsf_id, null, function (text, data, xhr) {
                                    var result = data.json();
                                    //console.log(result);
                                    $$("cashier_datatable").loadNext(-1, 0, {
                                        before: function () {
                                            var url = this.data.url;
                                            this.clearAll();
                                            this.data.url = url;
                                        }
                                    }, 0, 1);
                                    // webix.message(app.labels.operation_successful);

                                });
                            }
                        }
                    });

                    // location.assign("/admin/results/resultsExam?ex_id="+ex_id);
                    return false; // blocks the default click behavior
                },
            },


        };

        var cashregtable = {
            id: "cashregtable_datatable",
            view: "datatable",
            url:"/cashreg/getAll",
            columns: [
                {id: "cas_azn", header: "AZN hesabı",fillspace:true},
                {id: "cas_usd", header: "USD hesabı",fillspace:true},
                {id: "cas_eur", header: "EUR hesabı",fillspace:true},
                {id: "cas_date", header: "Tarix",fillspace:true,},
            ],
        };

        var pager = {cols:[
                {},
                {},
                {
                    view: "pager",
                    id: "cashier_pager",
                    size: 50,
                    group: 5
                },
                {},
                {}
            ]};



        var tab = {
            id: 'cashier_tab',
            width: 'auto',
            rows: [
                {
                    borderless: true,
                    cols: [
                        {
                            borderless: true,
                            id: 'cashier_toolbar',
                            view: 'toolbar',
                            css:"toolbarrr",
                            height: 40,
                            elements: [

                                {
                                    view: 'button',
                                    type:"form",
                                    id: "add_new_expense_window_button",
                                    label:"Yeni",
                                    width: 80,
                                    click: function () {
                                        $$("add_new_expenses_window").show()
                                    }
                                },
                                {
                                    view: "button",
                                    type: "iconButton",
                                    icon: "mdi mdi-download white_icon",
                                    id: "downloadCashierListButton",
                                    label: 'Çap et',
                                    autowidth: true,
                                    click: downloadCashierListButton
                                },
                                {},

                                // {
                                //     view: "text",
                                //     placeholder: "Axtarış",
                                //     id: "search_patient",
                                // },
                                {width: 50},

                                {
                                    view: 'segmented', name: 'filter',id:"segmented_cashier", multiview: true, value: 'credit', width: 400,
                                    options: [
                                        {id: 'credit', value: 'Mədaxil'},
                                        {id: 'export', value: 'Məxaric'},
                                    ],
                                    on: {
                                        onChange: function (newv, oldv) {
                                            console.log(newv);
                                            $$("cashier_datatable").clearAll();
                                            $$("cashier_datatable").define('url', '/cashier/getExpenses?type='+newv);
                                        }
                                    }
                                },
                            ]

                        },

                        {width: 5}
                    ]


                },
                {
                    width: 'auto',
                    rows: [
                        table,
                        {
                            view:"resizer",
                            id:"resizer"
                        },
                        cashregtable
                    ]
                }
            ]
        };


        var ui = {
            id: "cashier_layout",
            hidden: true,
            type: "clean",
            height: 'auto',
            rows: [
                tab
            ]
        };

        return ui;
    });
