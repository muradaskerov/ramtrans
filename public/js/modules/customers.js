define(
    [
        "globals",
        "windows/add_customer"
    ],
    function (globals) {
        function downloadBalanceButton(report_id,customer_id){

            webix.ajax().post("/reports/balanceToExcel", {
                // orders: items,
                report_id: report_id,
                customer_id: customer_id,
            }, function (text, data, xhr) {

                var result = data.json();

                console.log(result)

                var url = result.url;
                location.assign(result.url);

            });
        }

        var table = {
            id: "customers_datatable",
            view: "datatable",
            select: "row",
            editable:true,
            columns: [
                {
                    id: "cus_id",
                    header: [{text: "Клиент №", css: {"text-align": "right"}}],
                    css: {"text-align": "right"},
                    width: 115,
                },
                {id: "cus_name", header: 'Имя',fillspace:true,},
                {
                    id: "customer_report",
                    header: {text: "Oтчет", css: {"text-align": "center"}},
                    editor:'combo',
                    options:globals.report_lists,
                    css: {"text-align": "center"},
                },
                {
                    id: "download_balance",
                    header: {text: "Баланс", css: {"text-align": "center"}},
                    css: {"text-align": "center"},
                    template: function (obj) {
                        // //////////console.log(obj);
                        let str = "<a href=''><span class='download_balance doc_file mdi mdi-file'></span></a>";
                        return str;
                    },
                },
                // {
                //     id: "download_certificate",
                //     header: {text: "Sertifikat", css: {"text-align": "center"}},
                //     css: {"text-align": "center"},
                //     template: function (obj) {
                //         // //////////console.log(obj);
                //         let str = "<a href=''><span class='download_certificate doc_file mdi mdi-file'></span></a>";
                //         return str;
                //     },
                // },
            ],

            on:{
                onBeforeLoad: function () {
                    // if($$("segmented_filter").getValue() === "waiting"){
                    //     $$("customers_datatable").hideColumn("download_certificate");
                    // }else{
                    //     $$("customers_datatable").showColumn("download_certificate");
                    //
                    // }
                    this.showOverlay("Hazırlanır...");
                },
                onAfterLoad: function () {
                    if (!this.count()) {
                        this.showOverlay("Bu seçimlərə uyğun məlumat yoxdur.");
                    } else {
                        this.hideOverlay();
                    }
                },
                onItemDblClick: function (id, e, node) {
                    // globals.ex_id = $$(this).getItem(id.row).ex_id;
                    // globals.ex_status = $$(this).getItem(id.row).ex_status;
                    // globals.surveys = $$(this).getItem(id.row).surveys;
                    // $$("request_window").show()
                },

            },
            onClick: {

                "download_balance": function (ev, id) {
                    ev.preventDefault();
                    var report_id = this.getItem(id.row).customer_report;
                    var customer_id = this.getItem(id.row).cus_id;
                    // console.log(report_id);
                    if(report_id === undefined){
                        webix.alert('Пожалуйста, выберите отчет.');
                        return false;
                    }
                    downloadBalanceButton(report_id,customer_id);
                    // webix.toExcel($$("notations_datatable"));
                    // location.assign("/admin/results/resultsExam?ex_id="+ex_id);
                    return false; // blocks the default click behavior
                },
            },


        };

        var pager = {cols:[
                {},
                {},
                {
                    view: "pager",
                    id: "customers_pager",
                    size: 50,
                    group: 5
                },
                {},
                {}
            ]};



        var tab = {
            id: 'customers_tab',
            width: 'auto',
            rows: [
                {
                    borderless: true,
                    cols: [
                        {
                            borderless: true,
                            id: 'customers_toolbar',
                            view: 'toolbar',
                            css:"toolbarrr",
                            height: 40,
                            elements: [
                                {
                                    view: 'button',
                                    type:"form",
                                    id: "show_add_customer_window_button",
                                    label:"Новый клиент",
                                    autowidth:true,
                                    click: function () {
                                        $$("add_customer_window").show()
                                    }
                                },
                                {},
                                // {
                                //     view: "datepicker",
                                //     label: "Dövr: ",
                                //     labelWidth: 45,
                                //     id: "transfers_date_start",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 175,
                                // },
                                // {
                                //     view: "datepicker",
                                //     label: "",
                                //     labelWidth: 45,
                                //     id: "transfers_date_end",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 130,
                                // },
                                // {
                                //     view: "text",
                                //     placeholder: "Axtarış",
                                //     id: "search_patient",
                                // },
                                {width: 50},
                                // {
                                //     view: 'segmented', name: 'filter',id:"segmented_filter_2", multiview: true, value: 'waiting', width: 400,
                                //     options: [
                                //         {id: 'waiting', value: 'Gözləmədə'},
                                //         // {id: 'accepted_in_cash', value: 'Müayinədə'},
                                //         {id: 'completed', value: 'Hesabata salınmış'}
                                //     ],
                                //     on: {
                                //         onChange: function (newv, oldv) {
                                //             console.log(newv);
                                //             $$("customers_datatable").clearAll();
                                //             $$("customers_datatable").define('url', '/customers');
                                //         }
                                //     }
                                // },
                            ]

                        },

                        {width: 5}
                    ]


                },
                {
                    width: 'auto',
                    rows: [
                        table
                    ]
                }
            ]
        };


        var ui = {
            id: "customers_layout",
            hidden: true,
            type: "clean",
            height: 'auto',
            rows: [
                tab
            ]
        };

        return ui;
    });
