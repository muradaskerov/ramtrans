define(
    [
        "globals",
        "windows/new_report",
    ],
    function (globals) {

        function downloadNotationReportButton(notation_id){
            var items = $$("notations_datatable").serialize();
            webix.ajax().post("/notations/notationToExcel", {
                // orders: items,
                notation_id: notation_id
            }, function (text, data, xhr) {

                var result = data.json();

                console.log(result)

                var url = result.url;
                location.assign(result.url);

            });
        }

        globals.checked_orders = [];

        var itemChecked = function (row, column, state) {
            var item = this.getItem(row);
            if (state == 1) {
                globals.checked_orders.push(item);
            } else if (state == 0) {
                var i = globals.checked_orders.indexOf(item);

                if (i != -1) {
                    globals.checked_orders.splice(i, 1);
                }
            }

            if(globals.checked_orders.length > 0){
                $$("create_raport_button").enable();
            }else{
                $$("create_raport_button").disable();

            }
        };

        var table = {
            id: "notations_datatable",
            view: "datatable",
            select: "row",
            columns: [
                {
                    id: "notation_id",
                    header: [{text: "Raport №", css: {"text-align": "right"}}],
                    css: {"text-align": "right"},
                    width: 115,
                    template: function (obj, common, value) {
                        return common.subrow(obj, common) + obj.notation_id;
                    }
                },
                {id: "created_at", header: "Дата создания",fillspace:true,},
                {id: "creator", header: "Автор",fillspace:true,map:'#creater.creator#'},
                {
                    id: "download_notation",
                    header: {text: "Скачать", css: {"text-align": "center"}},
                    css: {"text-align": "center"},
                    template: function (obj) {
                        // //////////console.log(obj);
                        let str = "<a href=''><span class='download_notation doc_file mdi mdi-file'></span></a>";
                        return str;
                    },
                },
                {
                    id: "remove",
                    header: {text: "Удалить", css: {"text-align": "center"}},
                    css: {"text-align": "center",'font-size': "22px",    "color": "#279837 !important"},
                    template: function (obj) {
                        // //////////console.log(obj);
                        let str = "<a href=''><span class='remove doc_file mdi mdi-delete'></span></a>";
                        return str;
                    },
                },
            ],
            subview:{
                view:"datatable",
                //url:"admin/ticket/all",
                scrollY:true,
                scrollX:true,
                select:'row',
                resizeColumn: true,
                headerRowHeight: 32,
                autoheight: true,
                columns: [
                    {id: 'checked', header: { content:"masterCheckbox" }, width: 35, template: '{common.checkbox()}'},
                    {id: "invoice", header: "Инвойс №",},
                    {id: "conosament", header: [
                            {text: 'Коносамент №'},
                            {content:"textFilter"},

                        ],},
                    {id: "freight_sum", header: "Фрахт сумма",},
                    {id: "line_id", header: "Линия",},
                    {id: "container_num", header: "Контейнер №",},
                    {id: "seal", header: "Пломба",},
                    {id: "container_type", header: "Тип-Фут",collection: [
                            {id: '20', value: '20`'},
                            {id: '40', value: '40`'},
                            {id: '40_ot', value: '40` open top`'},
                            {id: 'fr', value: 'flat rack'},
                            {id: '40_hq', value: '40` HQ'},
                        ],},
                    {id: "place", header: "Места",},
                    {id: "gross", header: "Брутто",},
                    {id: "cubic_meter", header: "CBM",},
                    {id: "cargo", header: "Груз",},
                    {id: "receiver_id", header: "Получатель",},
                    {id: "free_days", header: "Free days",},
                    {id: "est_departure_date", header: "Дата - ETD",},
                    {id: "est_arrival_date", header: "Дата ETA",},
                    {id: "transport_type", header: "Судно /Авто",},
                    {id: "documents", header: "Документы :",},
                    {id: "details", header: "ДЕТАЛИ",},
                ],
                scheme: {

                    $init: function (item) {
                        item.index = this.count();

                        if (!item.report_id) {
                            item.$css = 'red_row';
                        }
                    }
                },
                on:{
                    onBeforeLoad: function () {
                        // if($$("segmented_filter").getValue() === "waiting"){
                        //     $$("orders_datatable").hideColumn("download_certificate");
                        // }else{
                        //     $$("orders_datatable").showColumn("download_certificate");
                        //
                        // }
                        this.showOverlay("Hazırlanır...");
                    },
                    onAfterLoad: function () {
                        if (!this.count()) {
                            this.showOverlay("Bu seçimlərə uyğun məlumat yoxdur.");
                        } else {
                            this.hideOverlay();
                        }
                    },
                    onItemDblClick: function (id, e, node) {
                        // globals.ex_id = $$(this).getItem(id.row).ex_id;
                        // globals.ex_status = $$(this).getItem(id.row).ex_status;
                        // globals.surveys = $$(this).getItem(id.row).surveys;
                        globals.selected = $$(this).getItem(id.row);
                        globals.dtable = $$("notations_datatable");

                        $$("info_order_window").show()
                    },
                    onCheck: itemChecked,
                },
            },
            on:{
                onBeforeLoad: function () {
                    // if($$("segmented_filter").getValue() === "waiting"){
                    //     $$("notations_datatable").hideColumn("download_certificate");
                    // }else{
                    //     $$("notations_datatable").showColumn("download_certificate");
                    //
                    // }

                    globals.checked_orders = [];
                    $$("create_raport_button").disable();


                    this.showOverlay("Hazırlanır...");
                },
                onAfterLoad: function () {
                    if (!this.count()) {
                        this.showOverlay("Bu seçimlərə uyğun məlumat yoxdur.");
                    } else {
                        this.hideOverlay();
                    }

                    let data = this.serialize();

                    data.forEach(function (item) {
                        $$('notations_datatable').openSub(item.id);

                    })
                    // console.log(data);


                },
                // onItemDblClick: function (id, e, node) {
                //     // globals.ex_id = $$(this).getItem(id.row).ex_id;
                //     // globals.ex_status = $$(this).getItem(id.row).ex_status;
                //     globals.notation_id = $$(this).getItem(id.row).notation_id;
                //     $$("request_window").show()
                // },
                onSubViewCreate: function (view, item) {
                    console.log(item);
                    view.parse(item.orders);
                },
            },
            onClick: {
                "download_notation": function (ev, id) {
                    ev.preventDefault();
                    var notation_id = this.getItem(id.row).notation_id;
                    console.log(notation_id);
                    downloadNotationReportButton(notation_id);
                    // webix.toExcel($$("notations_datatable"));
                    // location.assign("/admin/results/resultsExam?ex_id="+ex_id);
                    return false; // blocks the default click behavior
                },
                "remove": function (ev, id) {
                    ev.preventDefault();
                    var item = this.getItem(id.row);

                    // console.log(item)

                    webix.confirm({
                        title: "Предупреждение!",
                        type: "confirm-warning",
                        text: "Вы уверены, что хотите удалить?",
                        ok: "Да!",
                        cancel: "Нет",
                        width: 350,
                        callback: function (result) {
                            if (result) {
                                //console.log()
                                webix.ajax().del("/notations/"+item.notation_id, null, function (text, data, xhr) {
                                    var result = data.json();
                                    //console.log(result);
                                    if(result.success){
                                        $$("notations_datatable").loadNext(-1, 0, {
                                            before: function () {
                                                var url = this.data.url;
                                                this.clearAll();
                                                this.data.url = url;
                                            }
                                        }, 0, 1);
                                        webix.message(app.labels.operation_successful);
                                    }else{
                                        webix.alert(result.message);
                                    }

                                    //

                                });
                            }
                        }
                    });

                    // location.assign("/admin/results/resultsExam?ex_id="+ex_id);
                    return false; // blocks the default click behavior
                },
            },



        };

        var pager = {cols:[
                {},
                {},
                {
                    view: "pager",
                    id: "notations_pager",
                    size: 50,
                    group: 5
                },
                {},
                {}
            ]};



        var tab = {
            id: 'notations_tab',
            width: 'auto',
            rows: [
                {
                    borderless: true,
                    cols: [
                        {
                            borderless: true,
                            id: 'notations_toolbar',
                            view: 'toolbar',
                            css:"toolbarrr",
                            height: 40,
                            elements: [
                                {
                                    view: 'button',
                                    type:"form",
                                    id: "create_raport_button",
                                    disabled:true,
                                    label:"Создать отчет",
                                    autowidth:true,
                                    click: function (id, e, node) {
                                        $$("create_report_window").show()
                                    }
                                },
                                {},
                                // {
                                //     view: "datepicker",
                                //     label: "Dövr: ",
                                //     labelWidth: 45,
                                //     id: "transfers_date_start",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 175,
                                // },
                                // {
                                //     view: "datepicker",
                                //     label: "",
                                //     labelWidth: 45,
                                //     id: "transfers_date_end",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 130,
                                // },
                                // {
                                //     view: "text",
                                //     placeholder: "Axtarış",
                                //     id: "search_patient",
                                // },
                                {width: 50},
                                // {
                                //     view: 'segmented', name: 'filter',id:"segmented_filter_2", multiview: true, value: 'waiting', width: 400,
                                //     options: [
                                //         {id: 'waiting', value: 'Gözləmədə'},
                                //         // {id: 'accepted_in_cash', value: 'Müayinədə'},
                                //         {id: 'completed', value: 'Hesabata salınmış'}
                                //     ],
                                //     on: {
                                //         onChange: function (newv, oldv) {
                                //             console.log(newv);
                                //             $$("notations_datatable").clearAll();
                                //             $$("notations_datatable").define('url', '/notations');
                                //         }
                                //     }
                                // },
                            ]

                        },

                        {width: 5}
                    ]


                },
                {
                    width: 'auto',
                    rows: [
                        table
                    ]
                }
            ]
        };


        var ui = {
            id: "notations_layout",
            hidden: true,
            type: "clean",
            height: 'auto',
            rows: [
                tab
            ]
        };

        return ui;
    });
