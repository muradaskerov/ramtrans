define(
    [
        "globals",
        "windows/add_order_window",
        "windows/new_notation",
        "windows/info_order_window"
    ],
    function (globals) {

        globals.unpassaged_list = [];
        globals.checked_list = [];
        globals.selected_array = [];

        var itemChecked = function (row, column, state) {
            var item = this.getItem(row);

            if (state == 1) {
                globals.checked_list.push(row);

                globals.unpassaged_list.push(row);

            } else if (state == 0) {
                var i = globals.checked_list.indexOf(row);
                var j = globals.unpassaged_list.indexOf(row);
                if (i != -1) {
                    globals.checked_list.splice(i, 1);
                }
                if (j != -1) {
                    globals.unpassaged_list.splice(j, 1);
                }
            }


            if (state == 1) {
                globals.selected_array.push(row);
            } else if (state == 0) {
                var i = globals.selected_array.indexOf(row);
                if (i != -1) {
                    globals.selected_array.splice(i, 1);
                }
            }

            if(globals.checked_list.length > 0){
                $$("create_notation_button").enable();
            }else{
                $$("create_notation_button").disable();

            }
        };

        var addPassageButtonClicked = function () {

            var selectedItems = [];
            var ship_id;
            var direction;
            var freight = 0;
            var ship_incompatibility = false;
            for (var i = 0; i < globals.unpassaged_list.length; i++) {
                ship_id = $$('orders_datatable').getItem(globals.unpassaged_list[i]).ship_id;
                direction = $$('orders_datatable').getItem(globals.unpassaged_list[i]).direction;
                selectedItems.push($$('orders_datatable').getItem(globals.unpassaged_list[i]));
                freight += parseFloat($$('orders_datatable').getItem(globals.unpassaged_list[i]).price_usd);

            }

//                if (!ship_id) {
//                    webix.message({type: "error", text: "Gəmisi olan və manifestdə olmayan bilet seçin"});
//                    return false;
//                }
            if (ship_incompatibility) {
                ship_id = null;
            }
            // if (globals.unpassaged_list.length === 0) {
            //     webix.message({type: "error", text: "Raportda olmayan sifarişi seçin"});
            //     return false;
            // }

            $$('selected_requests_table').clearAll();
            $$('selected_requests_table').parse(selectedItems);
            // $$('existing_passages_table_select').define('url', '/notations');
            $$("new_passage_window").show();

        };

        // function addExpensesButtonClicked() {
        //
        //     globals.selectedItems = [];
        //     for (var i = 0; i < globals.unpassaged_list.length; i++) {
        //
        //         if ($$('orders_datatable').getItem(globals.unpassaged_list[i]).notation_id === null) {
        //             globals.selectedItems.push($$('orders_datatable').getItem(globals.unpassaged_list[i]));
        //         }
        //     }
        //     $$("add_expenses_window").show();
        //
        // }



        var table = {
            id: "orders_datatable",
            view: "datatable",
            resizeColumn:true,
            select: "row",

            columns: [
                {id: 'checked', header:{ content:"masterCheckbox" }, width: 35, template: '{common.checkbox()}'},
                {id: "invoice", header: [
                        {text: 'Инвойс №'},
                        {content:"textFilter"},

                    ],fillspace:true},
                {id: "conosament", header: [
                        {text: 'Коносамент №'},
                        {content:"textFilter"},

                    ],fillspace:true},
                // {id: "freight_sum", header: "Фрахт сумма",fillspace:true},
                {id: "line_id",collection:globals.line_list,header: [
                        {text: 'Линия'},
                        {content:"multiComboFilter"},

                    ],fillspace:true},
                {id: "container_num",header: [
                        {text: 'Контейнер №'},
                        {content:"textFilter"},

                    ],fillspace:true},
                // {id: "seal",header: [
                //         {text: 'Пломба'},
                //         {content:"textFilter"},
                //
                //     ]},
                {id: "container_type",
                    collection: [
                        {id: '20', value: '20`'},
                        {id: '40', value: '40`'},
                        {id: '40_ot', value: '40` open top`'},
                        {id: 'fr', value: 'flat rack'},
                        {id: '40_hq', value: '40` HQ'},
                    ],
                    header: [
                        {text: 'Тип-Фут'},
                        {content:"richSelectFilter"},

                    ]},
                {id: "type_order", collection:['import','export'],
                    header: [
                        {text: 'Imp/Exp'},
                        {content:"richSelectFilter"},

                    ]},
                // {id: "place", header: "Места",},
                // {id: "gross", header: "Брутто",},
                // {id: "cubic_meter", header: "CBM",},
                // {id: "cargo", header: "Груз",},
                {id: "receiver_id", fillspace:true,collection: globals.customer_list,header: [
                        {text: 'Получатель'},
                        {content:"richSelectFilter"},

                    ]},
                // {id: "free_days", header: "Free days",fillspace:true},
                {id: "est_departure_date",
                    format:webix.Date.dateToStr("%d-%m-%Y"),
                    map:"(date)#est_departure_date#",
                    header: [
                        {text: 'Дата - ETD'},
                        {content: "dateRangeFilter"}
                    ],
                    fillspace:true},
                {id: "est_arrival_date",
                    format:webix.Date.dateToStr("%d-%m-%Y"),
                    map:"(date)#est_arrival_date#",
                    header: [
                        {text: 'Дата ET'},
                        {content: "dateRangeFilter"}
                    ],
                    fillspace:true},
                {id: "transport_type", fillspace:true,collection:[
                        {id: 'ship', value: 'Судно'},
                        {id: 'bus', value: 'Авто'},
                        {id: 'train', value: 'Train'}
                    ],
                    header: [
                        {text: 'Transport type'},
                        {content:"multiComboFilter"},

                    ]
                },
                {id: "status",
                    fillspace:true,
                    map:"#last_action.action.ac_name#",
                    template:function(obj){
                        if(obj.last_action === null){
                            return 1;
                        }else{
                            return "● "+obj.last_action.action.ac_name;
                        }
                    },

                    header: [
                        {text: 'Статус'},
                        {content:"richSelectFilter",collection:globals.action_list,},


                    ]
                },
                {
                    id: "edit",
                    header: {text: "Редакт.", css: {"text-align": "center"}},
                    css: {"text-align": "center",'font-size': "22px"},
                    template: function (obj) {
                        // //////////console.log(obj);
                        let str = "<a href=''><span class='edit doc_file mdi mdi-table-edit'></span></a>";
                        return str;
                    },
                },
                {
                    id: "remove",
                    header: {text: "Удалить", css: {"text-align": "center"}},
                    css: {"text-align": "center",'font-size': "22px",    "color": "#279837 !important"},
                    template: function (obj) {
                        // //////////console.log(obj);
                        let str = "<a href=''><span class='remove doc_file mdi mdi-delete'></span></a>";
                        return str;
                    },
                },
                // {id: "documents", header: "Документы :",},
                // {id: "details", header: "ДЕТАЛИ",},
                // {
                //     id: "download_certificate",
                //     header: {text: "Sertifikat", css: {"text-align": "center"}},
                //     css: {"text-align": "center"},
                //     template: function (obj) {
                //         // //////////console.log(obj);
                //         let str = "<a href=''><span class='download_certificate doc_file mdi mdi-file'></span></a>";
                //         return str;
                //     },
                // },
            ],

            // scheme: {
            //
            //     $init: function (item) {
            //         item.index = this.count();
            //
            //         if (item.notation_id) {
            //             item.$css = 'reported';
            //         }
            //
            //         console.log(item)
            //
            //
            //     }
            // },

            on:{
                onBeforeLoad: function () {
                    // if($$("segmented_filter").getValue() === "waiting"){
                    //     $$("orders_datatable").hideColumn("download_certificate");
                    // }else{
                    //     $$("orders_datatable").showColumn("download_certificate");
                    //
                    // }
                 //   $$("add_expenses").disable();

                    if(globals.checked_list.length > 0){
                        $$("create_notation_button").enable();
                    }else{
                        $$("create_notation_button").disable();
                    }


                    this.showOverlay("Hazırlanır...");
                },
                onAfterLoad: function () {
                    if (!this.count()) {
                        this.showOverlay("Для этих опций нет информации.");
                    } else {
                        this.hideOverlay();
                    }
                },
                onItemDblClick: function (id, e, node) {
                    globals.order_id = $$(this).getItem(id.row).id;
                    globals.selected = $$(this).getItem(id.row);
                    globals.dtable = this;

                    $$("info_order_window").show();

                },
                onItemClick: function (id, e, node) {
                    // globals.order_id = $$(this).getItem(id.row).id;

                },
                onCheck: itemChecked,
            },
            onClick: {
                "edit": function (ev, id) {
                    ev.preventDefault();
                    globals.order_id = $$(this).getItem(id.row).id;
                    globals.selected = $$(this).getItem(id.row);
                    globals.dtable = this;

                    $$("info_order_window").show();

                    return false; // blocks the default click behavior
                },
                "remove": function (ev, id) {
                    ev.preventDefault();
                    var item = this.getItem(id.row);

                    // console.log(item)

                    webix.confirm({
                        title: "Предупреждение!",
                        type: "confirm-warning",
                        text: "Вы уверены, что хотите удалить заказ?",
                        ok: "Да!",
                        cancel: "Нет",
                        width: 350,
                        callback: function (result) {
                            if (result) {
                                //console.log()
                                webix.ajax().del("/orders/"+item.id, null, function (text, data, xhr) {
                                    var result = data.json();
                                    //console.log(result);
                                    $$("orders_datatable").loadNext(-1, 0, {
                                        before: function () {
                                            var url = this.data.url;
                                            this.clearAll();
                                            this.data.url = url;
                                        }
                                    }, 0, 1);
                                    // webix.message(app.labels.operation_successful);

                                });
                            }
                        }
                    });

                    // location.assign("/admin/results/resultsExam?ex_id="+ex_id);
                    return false; // blocks the default click behavior
                },
            },


        };

        var pager = {cols:[
                {},
                {},
                {
                    view: "pager",
                    id: "orders_pager",
                    size: 50,
                    group: 5
                },
                {},
                {}
            ]};



        var tab = {
            id: 'patiens_tab',
            width: 'auto',
            rows: [
                {
                    borderless: true,
                    cols: [
                        {
                            borderless: true,
                            id: 'orders_toolbar',
                            view: 'toolbar',
                            css:"toolbarrr",
                            height: 40,
                            elements: [
                                {
                                    view: 'button',
                                    type:"form",
                                    id: "show_add_patient_window_button",
                                    label:"Новый заказ",
                                    autowidth:true,
                                    click: function () {
                                        $$("add_order_window").show()
                                    }
                                },
                                {
                                    view: 'button',
                                    type:"form",
                                    id: "create_notation_button",
                                    label:"Создать рапорт",
                                    disabled:true,
                                    autowidth:true,
                                    click: addPassageButtonClicked
                                },
                                // {
                                //     view: 'button',
                                //     type:"form",
                                //     id: "add_expenses",
                                //     label:"Xərclər",
                                //     width:120,
                                //     disabled:true,
                                //     click: addExpensesButtonClicked
                                // },
                                {},

                                // {
                                //     view: "datepicker",
                                //     label: "Dövr: ",
                                //     labelWidth: 45,
                                //     id: "transfers_date_start",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 175,
                                // },
                                // {
                                //     view: "datepicker",
                                //     label: "",
                                //     labelWidth: 45,
                                //     id: "transfers_date_end",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 130,
                                // },
                                // {
                                //     view: "text",
                                //     placeholder: "Axtarış",
                                //     id: "search_patient",
                                // },
                                {width: 50},
                                {
                                    view: 'segmented', name: 'filter',id:"segmented_filter", multiview: true, width: 350,value:'today',
                                    options: [
                                        {id: 'today', value: 'Cегодня'},
                                        {id: 'all', value: 'Все'},
                                    ],
                                    on: {
                                        onChange: function (newv, oldv) {
                                            // console.log(newv);
                                            $$("orders_datatable").clearAll();
                                            $$("orders_datatable").define('url', '/orders?type='+newv);
                                        }
                                    }
                                },
                                // {
                                //     view: 'segmented', name: 'filter',id:"segmented_filter", multiview: true, value: 'waiting', width: 400,
                                //     options: [
                                //         {id: 'waiting', value: 'Gözləmədə'},
                                //         // {id: 'accepted_in_cash', value: 'Müayinədə'},
                                //         {id: 'notationed', value: 'Raporta salınmış'}
                                //     ],
                                //     on: {
                                //         onChange: function (newv, oldv) {
                                //             console.log(newv);
                                //             $$("orders_datatable").clearAll();
                                //             $$("orders_datatable").define('url', '/orders');
                                //         }
                                //     }
                                // },
                            ]

                        },

                        {width: 5}
                    ]


                },
                {
                    width: 'auto',
                    rows: [
                        table
                    ]
                }
            ]
        };


        var ui = {
            id: "orders_layout",
            hidden: true,
            type: "clean",
            height: 'auto',
            rows: [
                tab
            ]
        };

        return ui;
    });
