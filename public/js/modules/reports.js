define(
    [
        "globals",
        "windows/expenses_window",
    ],
    function (globals) {

        function downloadReportButton(report_id){
            var items = $$("notations_datatable").serialize();
            webix.ajax().post("/reports/reportToExcel", {
                // orders: items,
                report_id: report_id
            }, function (text, data, xhr) {

                var result = data.json();

                console.log(result)

                var url = result.url;
                location.assign(result.url);
            });
        }

        function downloadBalanceButton(report_id){

            webix.ajax().post("/reports/balanceToExcel", {
                // orders: items,
                report_id: report_id
            }, function (text, data, xhr) {

                var result = data.json();

                console.log(result)

                var url = result.url;
                location.assign(result.url);

            });
        }

        console.log(globals.station_list);

        var table = {
            id: "reports_datatable",
            view: "datatable",
            select: "row",
            columns: [
                {
                    id: "report_id",
                    header: [{text: "Отчет №", css: {"text-align": "right"}}],
                    css: {"text-align": "right"},
                    width: 115,
                    template: function (obj, common, value) {
                        return common.subrow(obj, common) + obj.report_id;
                    }
                },
                {id: "transport_num", header: "№ транспорта",fillspace:true,},
                // {id: "notation_id", header: "Raport №",fillspace:true,},
                {id: "transport_type", header: "Тип транспорта",fillspace:true,
                    options: [
                        {id: 'auto', value: 'Авто'},
                        {id: 'platform', value: 'Платформа'},
                        {id: 'tent', value: 'Тент'},
                        {id: 'vagon', value: 'Вагон'},
                    ]
                },
                {id: "station_id", header: "Станция",fillspace:true,collection:globals.station_list},
                {id: "created_at", header: "Дата создания",fillspace:true,},
                {
                    id: "download_balance",
                    header: {text: "Баланс", css: {"text-align": "center"}},
                    css: {"text-align": "center"},
                    template: function (obj) {
                        // //////////console.log(obj);
                        let str = "<a href=''><span class='download_balance doc_file mdi mdi-file'></span></a>";
                        return str;
                    },
                },
                {
                    id: "download_report",
                    header: {text: "Скачать", css: {"text-align": "center"}},
                    css: {"text-align": "center"},
                    template: function (obj) {
                        // //////////console.log(obj);
                        let str = "<a href=''><span class='download_report doc_file mdi mdi-file'></span></a>";
                        return str;
                    },
                },
                {
                    id: "remove",
                    header: {text: "Удалить", css: {"text-align": "center"}},
                    css: {"text-align": "center",'font-size': "22px",    "color": "#279837 !important"},
                    template: function (obj) {
                        // //////////console.log(obj);
                        let str = "<a href=''><span class='remove doc_file mdi mdi-delete'></span></a>";
                        return str;
                    },
                },
                // {
                //     id: "download_certificate",
                //     header: {text: "Sertifikat", css: {"text-align": "center"}},
                //     css: {"text-align": "center"},
                //     template: function (obj) {
                //         // //////////console.log(obj);
                //         let str = "<a href=''><span class='download_certificate doc_file mdi mdi-file'></span></a>";
                //         return str;
                //     },
                // },
            ],
            subview:{
                view:"datatable",
                //url:"admin/ticket/all",
                scrollY:true,
                scrollX:true,
                select:'row',
                resizeColumn: true,
                headerRowHeight: 28,
                autoheight: true,
                columns: [
                    {id: "invoice", header: "Инвойс №",},
                    {id: "conosament", header: "Коносамент №",},
                    {id: "freight_sum", header: "Фрахт сумма",},
                    {id: "line_id", header: "Линия",},
                    {id: "container_num", header: "Контейнер №",},
                    {id: "seal", header: "Пломба",},
                    {id: "container_type", header: "Тип-Фут",collection: [
                            {id: '20', value: '20`'},
                            {id: '40', value: '40`'},
                            {id: '40_ot', value: '40` open top`'},
                            {id: 'fr', value: 'flat rack'},
                            {id: '40_hq', value: '40` HQ'},
                        ],},
                    {id: "place", header: "Места",},
                    {id: "gross", header: "Брутто",},
                    {id: "cubic_meter", header: "CBM",},
                    {id: "cargo", header: "Груз",},
                    {id: "receiver_id", header: "Получатель",},
                    {id: "free_days", header: "Free days",},
                    {id: "est_departure_date", header: "Дата - ETD",},
                    {id: "est_arrival_date", header: "Дата ETA",},
                    {id: "transport_type", header: "Судно /Авто",},
                    {id: "documents", header: "Документы :",},
                    {id: "details", header: "ДЕТАЛИ",},
                ],
                on:{
                    onBeforeLoad: function () {
                        this.showOverlay("Hazırlanır...");
                    },
                    onAfterLoad: function () {
                        if (!this.count()) {
                            this.showOverlay("Bu seçimlərə uyğun məlumat yoxdur.");
                        } else {
                            this.hideOverlay();
                        }
                    },
                    onItemDblClick: function (id, e, node) {
                        // globals.ex_id = $$(this).getItem(id.row).ex_id;
                        // globals.ex_status = $$(this).getItem(id.row).ex_status;
                        // globals.surveys = $$(this).getItem(id.row).surveys;

                        // globals.selected = $$(this).getItem(id.row);
                        // globals.dtable = $$("reports_datatable");
                        //
                        //
                        // $$("info_order_window").show()
                    },
                },
            },
            on:{
                onBeforeLoad: function () {
                    // if($$("segmented_filter").getValue() === "waiting"){
                    //     $$("reports_datatable").hideColumn("download_certificate");
                    // }else{
                    //     $$("reports_datatable").showColumn("download_certificate");
                    //
                    // }
                    this.showOverlay("Hazırlanır...");
                },
                onAfterLoad: function () {
                    if (!this.count()) {
                        this.showOverlay("Bu seçimlərə uyğun məlumat yoxdur.");
                    } else {
                        this.hideOverlay();
                    }

                    // let data = this.serialize();
                    //
                    // data.forEach(function (item) {
                    //     $$('reports_datatable').openSub(item.id);
                    //
                    // })
                },
                onItemDblClick: function (id, e, node) {
                    // globals.ex_id = $$(this).getItem(id.row).ex_id;
                    // globals.ex_status = $$(this).getItem(id.row).ex_status;
                    globals.report_orders = $$(this).getItem(id.row).orders;
                    $$("expenses_window").show()
                },
                onSubViewCreate: function (view, item) {
                    console.log(item);
                    view.parse(item.orders);
                },
            },
            onClick: {
                "download_report": function (ev, id) {
                    ev.preventDefault();
                    var report_id = this.getItem(id.row).report_id;
                    console.log(report_id);
                    downloadReportButton(report_id);
                    // webix.toExcel($$("notations_datatable"));
                    // location.assign("/admin/results/resultsExam?ex_id="+ex_id);
                    return false; // blocks the default click behavior
                },
                // "download_balance": function (ev, id) {
                //     ev.preventDefault();
                //     var report_id = this.getItem(id.row).report_id;
                //     console.log(report_id);
                //     downloadBalanceButton(report_id);
                //     // webix.toExcel($$("notations_datatable"));
                //     // location.assign("/admin/results/resultsExam?ex_id="+ex_id);
                //     return false; // blocks the default click behavior
                // },
                "remove": function (ev, id) {
                    ev.preventDefault();
                    var item = this.getItem(id.row);

                    // console.log(item)

                    webix.confirm({
                        title: "Предупреждение!",
                        type: "confirm-warning",
                        text: "Вы уверены, что хотите удалить отчет?",
                        ok: "Да!",
                        cancel: "Нет",
                        width: 350,
                        callback: function (result) {
                            if (result) {
                                //console.log()
                                webix.ajax().del("/reports/"+item.report_id, null, function (text, data, xhr) {
                                    var result = data.json();
                                    //console.log(result);
                                    $$("reports_datatable").loadNext(-1, 0, {
                                        before: function () {
                                            var url = this.data.url;
                                            this.clearAll();
                                            this.data.url = url;
                                        }
                                    }, 0, 1);
                                    // webix.message(app.labels.operation_successful);

                                });
                            }
                        }
                    });

                    // location.assign("/admin/results/resultsExam?ex_id="+ex_id);
                    return false; // blocks the default click behavior
                },
            },


        };

        var pager = {cols:[
                {},
                {},
                {
                    view: "pager",
                    id: "reports_pager",
                    size: 50,
                    group: 5
                },
                {},
                {}
            ]};



        var tab = {
            id: 'reports_tab',
            width: 'auto',
            rows: [
                {
                    borderless: true,
                    cols: [
                        {
                            borderless: true,
                            id: 'reports_toolbar',
                            view: 'toolbar',
                            css:"toolbarrr",
                            height: 40,
                            elements: [
                                {},
                                // {
                                //     view: "datepicker",
                                //     label: "Dövr: ",
                                //     labelWidth: 45,
                                //     id: "transfers_date_start",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 175,
                                // },
                                // {
                                //     view: "datepicker",
                                //     label: "",
                                //     labelWidth: 45,
                                //     id: "transfers_date_end",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 130,
                                // },
                                // {
                                //     view: "text",
                                //     placeholder: "Axtarış",
                                //     id: "search_patient",
                                // },
                                {width: 50},
                                // {
                                //     view: 'segmented', name: 'filter',id:"segmented_filter_2", multiview: true, value: 'waiting', width: 400,
                                //     options: [
                                //         {id: 'waiting', value: 'Gözləmədə'},
                                //         // {id: 'accepted_in_cash', value: 'Müayinədə'},
                                //         {id: 'completed', value: 'Hesabata salınmış'}
                                //     ],
                                //     on: {
                                //         onChange: function (newv, oldv) {
                                //             console.log(newv);
                                //             $$("reports_datatable").clearAll();
                                //             $$("reports_datatable").define('url', '/reports');
                                //         }
                                //     }
                                // },
                            ]

                        },

                        {width: 5}
                    ]


                },
                {
                    width: 'auto',
                    rows: [
                        table,
                    ]
                }
            ]
        };


        var ui = {
            id: "reports_layout",
            hidden: true,
            type: "clean",
            height: 'auto',
            rows: [
                tab
            ]
        };

        return ui;
    });
