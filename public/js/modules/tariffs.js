define(
    [
        "globals",
        "windows/add_tariff",
    ],
    function (globals) {

        function downloadNotationReportButton(notation_id){
            var items = $$("tariffs_datatable").serialize();
            webix.ajax().post("/tariffs/notationToExcel", {
                // orders: items,
                notation_id: notation_id
            }, function (text, data, xhr) {

                var result = data.json();

                console.log(result)

                var url = result.url;
                location.assign(result.url);

            });
        }

        var table = {
            id: "tariffs_datatable",
            view: "datatable",
            select: "row",
            columns: [
                {
                    id: "tar_id",
                    header: [{text: "Tariff ID", css: {"text-align": "right"}}],
                    css: {"text-align": "right"},
                    width: 115,
                },
                {id: "tar_line_id",collection:globals.line_list,header: [
                        {text: 'Линия'},
                        {content:"multiComboFilter"},

                    ],fillspace:true},
                {id: "tar_container_type", collection:['20','40'],
                    header: [
                        {text: 'Тип-Фут'},
                        {content:"selectFilter"},

                    ]},
                {id: "tar_price", header: "сумма",fillspace:true,template:function (obj) {
                        return "$ "+obj.tar_price;
                    }},
                {id: "tar_free_day", header: "свободных дней",fillspace:true},
                {id: "tar_begin_day", header: "First day",fillspace:true},
                {id: "tar_end_day", header: "Last day",fillspace:true},
            ],
            on:{
                onBeforeLoad: function () {
                    // if($$("segmented_filter").getValue() === "waiting"){
                    //     $$("tariffs_datatable").hideColumn("download_certificate");
                    // }else{
                    //     $$("tariffs_datatable").showColumn("download_certificate");
                    //
                    // }
                    this.showOverlay("Hazırlanır...");
                },
                onAfterLoad: function () {
                    if (!this.count()) {
                        this.showOverlay("Bu seçimlərə uyğun məlumat yoxdur.");
                    } else {
                        this.hideOverlay();
                    }
                },
                onItemDblClick: function (id, e, node) {
                    // globals.ex_id = $$(this).getItem(id.row).ex_id;
                    // globals.ex_status = $$(this).getItem(id.row).ex_status;
                    globals.notation_id = $$(this).getItem(id.row).notation_id;
                    $$("request_window").show()
                },

            },
            onClick: {
                "download_notation": function (ev, id) {
                    ev.preventDefault();
                    var notation_id = this.getItem(id.row).notation_id;
                    console.log(notation_id);
                    downloadNotationReportButton(notation_id);
                    // webix.toExcel($$("tariffs_datatable"));
                    // location.assign("/admin/results/resultsExam?ex_id="+ex_id);
                    return false; // blocks the default click behavior
                },
            },


        };




        var tab = {
            id: 'tariffs_tab',
            width: 'auto',
            rows: [
                {
                    borderless: true,
                    cols: [
                        {
                            borderless: true,
                            id: 'tariffs_toolbar',
                            view: 'toolbar',
                            css:"toolbarrr",
                            height: 40,
                            elements: [
                                {
                                    view: 'button',
                                    type:"form",
                                    id: "add_tariff_window_button",
                                    label:"Новый тариф",
                                    width: 80,
                                    click: function () {
                                        $$("add_tariff_window").show()
                                    }
                                },
                                {},
                                // {
                                //     view: "datepicker",
                                //     label: "Dövr: ",
                                //     labelWidth: 45,
                                //     id: "transfers_date_start",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 175,
                                // },
                                // {
                                //     view: "datepicker",
                                //     label: "",
                                //     labelWidth: 45,
                                //     id: "transfers_date_end",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 130,
                                // },
                                // {
                                //     view: "text",
                                //     placeholder: "Axtarış",
                                //     id: "search_patient",
                                // },
                                {width: 50},
                                // {
                                //     view: 'segmented', name: 'filter',id:"segmented_filter_2", multiview: true, value: 'waiting', width: 400,
                                //     options: [
                                //         {id: 'waiting', value: 'Gözləmədə'},
                                //         // {id: 'accepted_in_cash', value: 'Müayinədə'},
                                //         {id: 'completed', value: 'Hesabata salınmış'}
                                //     ],
                                //     on: {
                                //         onChange: function (newv, oldv) {
                                //             console.log(newv);
                                //             $$("tariffs_datatable").clearAll();
                                //             $$("tariffs_datatable").define('url', '/tariffs');
                                //         }
                                //     }
                                // },
                            ]

                        },

                        {width: 5}
                    ]


                },
                {
                    width: 'auto',
                    rows: [
                        table
                    ]
                }
            ]
        };


        var ui = {
            id: "tariffs_layout",
            hidden: true,
            type: "clean",
            height: 'auto',
            rows: [
                tab
            ]
        };

        return ui;
    });
