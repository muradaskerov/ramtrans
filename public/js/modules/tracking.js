define(
    [
        "globals",
        "windows/add_action"
    ],
    function (globals) {

        var table = {
            id: "tracking_datatable",
            view: "datatable",
            select: "row",
            columns: [
                {
                    id: "invoice",
                    header: [{text: "Инвойс №", css: {"text-align": "right"}}],
                    css: {"text-align": "right"},
                    width: 115,
                },
                {id: "container_num", header: "Контейнер №",fillspace:true,},
                {id: "container_type", header: "Тип-Фут",fillspace:true,collection: [
                        {id: '20', value: '20`'},
                        {id: '40', value: '40`'},
                        {id: '40_ot', value: '40` open top`'},
                        {id: 'fr', value: 'flat rack'},
                        {id: '40_hq', value: '40` HQ'},
                    ],},
                {id: "line_id", header: "Линия",fillspace:true,collection:globals.line_list},
                {id: "created_at", map: "#selectedaction.oa_start_date#", header: "История транзакций",fillspace:true,},
                // {id: "status", header: "Status",fillspace:true,},
                // {
                //     id: "download_certificate",
                //     header: {text: "Sertifikat", css: {"text-align": "center"}},
                //     css: {"text-align": "center"},
                //     template: function (obj) {
                //         // //////////console.log(obj);
                //         let str = "<a href=''><span class='download_certificate doc_file mdi mdi-file'></span></a>";
                //         return str;
                //     },
                // },
            ],
            on:{
                onBeforeLoad: function () {
                    // if($$("segmented_filter").getValue() === "waiting"){
                    //     $$("tracking_datatable").hideColumn("download_certificate");
                    // }else{
                    //     $$("tracking_datatable").showColumn("download_certificate");
                    //
                    // }
                    this.showOverlay("Hazırlanır...");
                },
                onAfterLoad: function () {
                    if (!this.count()) {
                        this.showOverlay("Bu seçimlərə uyğun məlumat yoxdur.");
                    } else {
                        this.hideOverlay();
                    }
                },
                onItemDblClick: function (id, e, node) {
                    globals.id = $$(this).getItem(id.row).id;
                    // globals.ex_status = $$(this).getItem(id.row).ex_status;
                    // globals.surveys = $$(this).getItem(id.row).surveys;
                    $$("add_action_window").show()
                },
            },
            // onClick: {
            //     "download_certificate": function (ev, id) {
            //         ev.preventDefault();
            //         var ex_id = this.getItem(id.row).ex_id;
            //         globals.exam_id = ex_id;
            //         $$("show_results_view").parse({exam_id: ex_id});
            //         $$("show_results_view2").parse({exam_id: ex_id});
            //         $$("show_results_window").show();
            //
            //         // location.assign("/admin/results/resultsExam?ex_id="+ex_id);
            //         return false; // blocks the default click behavior
            //     },
            // },


        };

        var pager = {cols:[
                {},
                {},
                {
                    view: "pager",
                    id: "tracking_pager",
                    size: 50,
                    group: 5
                },
                {},
                {}
            ]};



        var tab = {
            id: 'tracking_tab',
            width: 'auto',
            rows: [
                {
                    borderless: true,
                    cols: [
                        {
                            borderless: true,
                            id: 'tracking_toolbar',
                            view: 'toolbar',
                            css:"toolbarrr",
                            height: 40,
                            elements: [
                                {},
                                // {
                                //     view: "datepicker",
                                //     label: "Dövr: ",
                                //     labelWidth: 45,
                                //     id: "transfers_date_start",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 175,
                                // },
                                // {
                                //     view: "datepicker",
                                //     label: "",
                                //     labelWidth: 45,
                                //     id: "transfers_date_end",
                                //     format: webix.Date.dateToStr("%d/%m/%Y"),
                                //     height: 30,
                                //     width: 130,
                                // },
                                // {
                                //     view: "text",
                                //     placeholder: "Axtarış",
                                //     id: "search_patient",
                                // },
                                {width: 50},
                                {
                                    view: 'segmented', name: 'filter',id:"segmented_filter_status", multiview: true, value: '1', width: 800,
                                    options: [
                                        {id: '1', value: 'На дороге'},
                                        {id: '2', value: 'В Поти'},
                                        {id: '3', value: 'В иране'},
                                        {id: '4', value: 'Прибытие в Баку'},
                                        {id: '5', value: 'Был прибыл'}
                                    ],
                                    on: {
                                        onChange: function (newv, oldv) {
                                            console.log(newv);
                                            $$("tracking_datatable").clearAll();
                                            $$("tracking_datatable").define('url', '/order/getWithStatus?action='+newv);
                                        }
                                    }
                                },
                            ]

                        },

                        {width: 5}
                    ]


                },
                {
                    width: 'auto',
                    rows: [
                        // {
                        //     container:"testA",
                        //     view:"organogram",
                        //     borderless: true,
                        //     select: true,
                        //     type:{
                        //         listMarginX: 20,
                        //         listMarginY: 20
                        //     },
                        //     data: [
                        //         { id:"1.1.2", value:"Development", $type: "list", data:[
                        //                 { id:"1.1.2.1", value:"- Faculty development workshops" },
                        //                 { id:"1.1.2.2", value:"- Student development" }
                        //             ]}
                        //     ]
                        //
                        // },
                        table
                    ]
                }
            ]
        };


        var ui = {
            id: "tracking_layout",
            hidden: true,
            type: "clean",
            height: 'auto',
            rows: [
                tab
            ]
        };

        return ui;
    });
