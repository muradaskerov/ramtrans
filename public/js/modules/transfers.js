define(
    [
        "globals",
        "windows/choose_customer"
    ],
    function (globals) {

        webix.ui({
            view: "contextmenu",
            id: "transfer_actions_menu",
            data: [
                {
                    id: 1,
                    value: "<span>Müştərini seç</span>"
                },
            ],
            on: {
                onItemClick: function (id) {

                    var list = $$("transfers_datatable"); //list item object
                    var item = list.getSelectedItem();
                    console.log(item)
                    globals.selected_transfers = item;
                    switch (id) {
                        case '1':
                            console.log('sec')
                            if(globals.selected_transfers === undefined){
                                webix.alert('Xərc sətirlərini seçin.')
                            }else{
                                $$('choose_customer_window').show()
                            }
                            break;
                    }
                },
            }
        });

        var table = {
            id: "transfers_datatable",
            view: "datatable",
            multiselect: true,
            blockselect: true,
            select: "row",
            leftSplit: 6,
            // editaction: "dblclick",
            // save: {
            //     "update": function(data) {
            //         console.log(data);
            //         var item = $$('transfers_datatable').getItem(data);
            //         console.log(item)
            //         return webix.ajax().put("/transfers/"+data,{customer_id: item.customer_id,id: data});
            //     }
            // },
            rightSplit: 4,
            headerRowHeight: 140,
            scrollX: true,
            editable: true,
            headermenu: {
                width: 250,
                height: 500,
                scroll: true,
            },
            columns: [],
            on: {
                onBeforeLoad: function () {
                    var dtable = $$("transfers_datatable");

                    cols = [
                        {
                            id: "invoice", header: [
                                {text: 'Инвойс №', rotate: true},
                                {content: "textFilter"},
                            ],
                            width: 100,
                            headermenu: false
                        },
                        {
                            id: "customer_id",
                            width: 100,
                            // editor:'textAreaPopup',
                            collection: globals.customer_list,
                            header: [
                                {text: 'Клиент'},
                                {content: "textFilter"},
                            ],
                            headermenu: false
                        },
                        {
                            id: "container_num", header: [
                                {text: 'Контейнер №', rotate: true},
                                {content: "textFilter"},
                            ], width: 100,
                            headermenu: false
                        },
                        {
                            id: "transport_type",
                            width: 100,
                            collection: [
                                {id: 'ship', value: 'Судно'},
                                {id: 'bus', value: 'Авто'},
                                {id: 'train', value: 'Train'}
                            ],
                            header: [
                                {text: 'Transport type', rotate: true},
                                {content: "selectFilter"},
                            ],
                            headermenu: false
                        },
                        {
                            id: "container_type",
                            collection: [
                                {id: '20', value: '20`'},
                                {id: '40', value: '40`'},
                                {id: '40_ot', value: '40` open top`'},
                                {id: 'fr', value: 'flat rack'},
                                {id: '40_hq', value: '40` HQ'},
                            ],
                            header: [
                                {text: 'Тип-Фут'},
                                {content: "selectFilter"},
                            ],
                            width: 100,
                            headermenu: false
                        },
                        {
                            id: "ccc", header: [
                                {text: 'Дата отчета:'},
                            ],
                            width: 140,
                            format: webix.Date.dateToStr("%d-%m-%Y"),
                            map: '(date)#report.date_notation#',
                            headermenu: false
                        },
                        {
                            id: 'expense_1',
                            header: [
                                {text: 'Фрахт', rotate: true,},

                            ],
                        },
                        {
                            id: 'expense_2',
                            header: [
                                {text: 'Inspection  fee destination', rotate: true,},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_3', header: [
                                {text: 'ТНС', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_4', header: [
                                {text: 'МН', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_5', header: [
                                {text: 'оформление док-тов в линии', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_19', header: [
                                {text: 'Стархов Конт.', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_6', header: [
                                {text: 'Хранен. Конт.', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_7', header: [
                                {text: 'Демередж ', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_8', header: [
                                {text: 'перетарка ', rotate: true},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_9', header: [
                                {text: 'Ж.Д. Грузия ', rotate: true},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_33', header: [
                                {text: 'Ж.Д. Грузия пустои', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_10', header: [
                                {text: 'тупик', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_11', header: [
                                {text: 'Пломб', rotate: true},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_12', header: [
                                {text: 'Авто-перевозка между терминалами', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_13', header: [
                                {text: 'талманирование', rotate: true},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_14', header: [
                                {text: 'Прочие расходы ( cesh )', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_15', header: [
                                {text: 'письмо в ж/д департамент', rotate: true},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_16', header: [
                                {text: 'дополнительное укрепление пена', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_17', header: [
                                {text: 'вирамаина', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_18', header: [
                                {text: 'перевеска контейнеров', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_20', header: [
                                {text: 'Итого расходы в Грузии', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_25', header: [
                                {text: 'VAT', rotate: true},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_26', header: [
                                {text: 'таможеный сбор', rotate: true},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_27', header: [
                                {text: 'перетарка', rotate: true},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_28', header: [
                                {text: 'Авто-перевозка Poti-Baku-Poti', rotate: true},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_32', header: [
                                {text: 'port facility security', rotate: true},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_36', header: [
                                {text: 'Ж.Д. Азербайджан', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_29', header: [
                                {text: 'КК', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_34', header: [
                                {text: 'Ж.Д. Азербайджан  пустои', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_35', header: [
                                {text: 'DHL', rotate: true},

                            ],
                            hidden: true,
                        },
                        {
                            id: 'expense_30', header: [
                                {text: 'Таможенный пункт БК', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_31', header: [
                                {text: 'SM', rotate: true},

                            ],
                        },
                        {
                            id: 'expense_21', header: [
                                {text: 'Итого расходы в Азербайджане', rotate: true},

                            ],
                        },


                    ];

                    console.log(globals.expense_types);

                    globals.expense_types.forEach(function (obj) {

                        var found = false;
                        cols.forEach(function (obje) {
                            if (obje.id === 'expense_' + obj.id || parseInt(obj.id) === 22 || parseInt(obj.id) === 23 || parseInt(obj.id) === 37 || parseInt(obj.id) === 38) {
                                found = true;
                            }
                        });

                        if (!found) {
                            // console.log(obj)
                            cols.push({
                                id: 'expense_' + obj.id, header: [
                                    {text: obj.value, rotate: true},
                                ],
                            });
                        }

                    });

                    cols.push({
                            id: 'expense_22',
                            header: [
                                {text: 'Общая\n сумма\n расходов', rotate: true},

                            ],
                            headermenu: false
                        },
                        {
                            id: 'expense_23',
                            header: [
                                {text: 'Стоимость для клиента', rotate: true},

                            ],
                            headermenu: false
                        },
                        {
                            id: 'expense_37',
                            header: [
                                {text: 'Доход от фрахта', rotate: true},

                            ],
                            headermenu: false
                        },
                        {
                            id: 'expense_38',
                            header: [
                                {text: 'Доход от перевозки', rotate: true},

                            ],
                            headermenu: false
                        });

                    dtable.define("columns", cols);

                    dtable.refreshColumns();
                },
                onAfterLoad: function () {
                    $$("transfer_actions_menu").attachTo($$("transfers_datatable"));

                },
            }


        };


        var tab = {
            id: 'transfers_tab',
            width: 'auto',
            rows: [
                {
                    borderless: true,
                    cols: [
                        {
                            borderless: true,
                            id: 'transfers_toolbar',
                            view: 'toolbar',
                            css: "toolbarrr",
                            height: 40,
                            elements: [
                                {},
                                {width: 50},
                            ]

                        },

                        {width: 5}
                    ]


                },
                {
                    width: 'auto',
                    rows: [
                        table
                    ]
                }
            ]
        };


        var ui = {
            id: "transfers_layout",
            hidden: true,
            type: "clean",
            height: 'auto',
            rows: [
                tab
            ]
        };

        return ui;
    });
