define([], function () {

    /**-----------------------------------------------------------------------------------------------
     **                                            System popup, logout button
     **----------------------------------------------------------------------------------------------*/
    var systemPopupSelectChanged = function (name, args) {
        var id = $$('system_popup_list').getSelectedId();
        if (id == 'logout_button') {
            location.assign("/logout");
        }
    };

    var ui = {
        id: 'user_popup',
        view: 'popup',
        width: 200,
        body: {
            id: 'system_popup_list',
            view: 'list',
            template: '#label#',
            data: [
                {id: 'logout_button', label: 'Выход'}
            ],
            borderless: true,
            autoheight: true,
            select: true,
            on: {
                onSelectChange: systemPopupSelectChanged
            }

        }
    };

    return webix.ui(ui);
});
