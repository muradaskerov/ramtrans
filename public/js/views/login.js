define(
    [
        "globals",
    ],
    function (globals) {
        var element = document.querySelector('meta[name="csrf-token"]');
        var csrf = element && element.getAttribute("content");

        var app = {
            init: function () {
                webix.ui(app.ui);

                webix.extend($$("login_window"), webix.ProgressBar);

                webix.ajax.original_callback_function = webix.ajax.$callback;

                webix.ajax.$callback = function(owner, call, text, data, x, is_error) {
                    $$("login_window").hideProgress();

                    return webix.ajax.original_callback_function.apply(this,arguments);
                };



                webix.attachEvent("onBeforeAjax",
                    function (mode, url, data, request, headers, files, promise) {

                        headers["X-CSRF-TOKEN"] =csrf;

                        $$("login_window").showProgress({
                            type:"top",
                        });
                    }
                );
            },
            csrf_token: csrf

        };

        var parseEmail = function (value, prev) {
            if (value != null)
                value = value.toLowerCase().replace(/[^a-z.0-9.@.\.]/g,'');
            this.setValue(value);
        };

        var loginButtonClicked = function () {
            if($$('login_form').validate()){
                var dataRequest = $$('login_form').getValues();
                webix.ajax().post('login', dataRequest, function (text, data, xhr) {
                    var result = data.json();
                    if (result != undefined && result.type === 'success') {
                        webix.message(result.message);
                        location.reload();
                    } else if (result == undefined || result.type === 'error') {
                        webix.message(result.message);
                    }
                });
            }
        };

        app.ui = {
            id: 'login_window',
            view: 'window',
            container: "main_div",
            head: globals.labels.login_system,
            hidden: false,
            position: 'center',
            width: 400,
            css: "login_window",
            height: 200,
            body: {
                id: 'login_form',
                view: 'form',
                elements: [
                    {
                        view: 'text',
                        label: globals.labels.username,
                        required: true,
                        name: 'login',
                        labelWidth:120,
                        on: {
                            onChange: parseEmail
                        }
                    },
                    {
                        view: 'text',
                        type: 'password',
                        required: true,
                        label: globals.labels.password,
                        name: 'password',
                        labelWidth:120
                    },
                    {
                        margin: 5,
                        cols: [
                            {
                                view: 'button',
                                value: 'Войти',
                                type: 'form',
                                hotkey: 'enter',
                                click: loginButtonClicked
                            }
                        ]
                    }
                ]
            }
        };

        return app;
    }
);
