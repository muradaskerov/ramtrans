define(
    [
        "globals",
        "modules/orders",
        "modules/notations",
        "modules/reports",
        "modules/customers",
        "modules/tracking",
        "modules/cashier",
        "modules/tariffs",
        "modules/transfers",
        "popups/user_popup",
        "windows/feedback_window"


    ],
    function (globals, orders, notations, reports, customers, tracking, cashier,tariffs,transfers) {

        /**-----------------------------------------------------------------------------------------------
         **                                            Application initialization
         **----------------------------------------------------------------------------------------------*/

        var element = document.querySelector('meta[name="csrf-token"]');
        var csrf = element && element.getAttribute("content");

        var app = {
            init: function () {

                webix.ui(app.ui);

                webix.extend($$("application"), webix.ProgressBar);

                webix.ajax.original_callback_function = webix.ajax.$callback;

                webix.ajax.$callback = function (owner, call, text, data, x, is_error) {
                    $$("application").hideProgress();

                    return webix.ajax.original_callback_function.apply(this, arguments);
                };

                webix.attachEvent("onBeforeAjax",
                    function (mode, url, data, request, headers, files, promise) {

                        headers["X-CSRF-TOKEN"] = csrf;

                        $$("application").showProgress({
                            type: "top",
                        });
                    }
                );

                webix.ajax().get("/menus", null, function (text, data, xhr) {
                    var response = data.json();

                    console.log(response);
                });


                $$("sidebarNav").select("orders");
                // $$("sidebarNav").toggle();
            },
            csrf_token: csrf

        };


        var selectLayout = function (id) {
            console.log(id);
            var menu_data = globals.menu_data;
            $$("empty").hide();
            //console.log(menu_data);
            for (var i = 0; i < menu_data.length; i++) {
                if (id === menu_data[i].id) {
                    $$(menu_data[i].layout).show();
                } else {
                    // console.log(menu_data[i].layout)
                    $$(menu_data[i].layout).hide();
                }
            }

            switch (id) {
                case 'orders':
                    $$("orders_datatable").clearAll();
                    $$("orders_datatable").define('url', '/orders');
                    break;
                case 'notations':
                    $$("notations_datatable").clearAll();
                    $$("notations_datatable").define('url', '/notations');
                    break;
                case 'reports':
                    $$("reports_datatable").clearAll();
                    $$("reports_datatable").define('url', '/reports');
                    break;
                case 'customers':
                    $$("customers_datatable").clearAll();
                    $$("customers_datatable").define('url', '/customers');
                    break;
                case 'tracking':
                    $$("tracking_datatable").clearAll();
                    $$("tracking_datatable").define('url', '/order/getWithStatus?action=1');
                    break;
                case 'cashier':
                    $$("cashier_datatable").clearAll();
                    $$("cashier_datatable").define('url', '/cashier/getExpenses?type=credit');
                    break;
                case 'tariffs':
                    $$("tariffs_datatable").clearAll();
                    $$("tariffs_datatable").define('url', '/tariffs');
                    break;
                 case 'transfers':
                    $$("transfers_datatable").clearAll();
                    $$("transfers_datatable").define('url', '/transfers');
                    break;
            }
        };

        var openFeedbackWindow = function () {
            $$("feedback_window").show();
        };


        app.ui = {
            id: "application",
            view: 'headerlayout',
            margin: 0,
            padding: 0,
            container: document.body,
            rows: [
                {
                    view: "toolbar",
                    padding: 3,
                    height:70,
                    elements: [
                        {
                            view: "icon", icon: "mdi mdi-menu", click: function () {
                                $$("sidebarNav").toggle();
                            }
                        },
                        { view: "template",css: "nobackground",borderless:true,width:115, template:"<img width='80' src='img/logo.png' />"},
                        {view: "label", label: globals.labels.app_name},
                        {},
                        {
                            id: 'logout_button',
                            name: 'logout_button',
                            view: 'button',
                            type: 'icon',
                            label: globals.user_fullname,
                            icon: 'user',
                            width: 200,
                            template: "<div class='container'><div class='row'><div class='col-md-2'><span class='mdi mdi-account account_icon_style'></span></div><div class='col-md-10 justify-content-center align-self-center user_name'>"+globals.user_fullname+"</div> </div></div>",
                            css: 'small menu_button',
                            popup: 'user_popup'
                        },
                        // {view: "button", type: "icon", icon: "envelope", width: 50, click: openFeedbackWindow},
                    ]
                },
                {
                    cols: [
                        {
                            view: "sidebar",
                            id: "sidebarNav",
                            css: "custom_sidebar",
                            data: globals.menu_data,
                            on: {
                                onAfterSelect: function (id) {
                                    selectLayout(id);
                                }
                            }
                        },
                        {
                            id: 'application_layout',
                            height: 'auto',
                            rows: [
                                {id: 'empty'},
                                orders,
                                notations,
                                reports,
                                transfers,
                                customers,
                                tracking,
                                tariffs,
                                cashier,
                            ]
                        }
                    ]
                }
            ]
        };

        return app;
    }
);

