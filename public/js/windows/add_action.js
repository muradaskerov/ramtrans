define(["globals"], function (globals) {

    var windowClose = function () {
        $$("add_action_window").hide();
    };

    function createOperation() {
        if ($$("add_action_form").validate()) {
            let formData = $$("add_action_form").getValues();
            console.log(formData)

            webix.ajax().post("/orderActions/store", {formData:formData,oa_order_id: globals.id}, function (text, data, XmlHttpRequest) {
                var response = data.json();

                if (response && response.success === true) {
                    webix.message(globals.labels.operation_successful);
                    windowClose();



                    $$("tracking_datatable").loadNext(-1,0,{
                        before:function(){
                            var url = this.data.url;
                            this.clearAll();
                            this.data.url = url;
                        }
                    },0,1);
                }
            });
        } else {
            webix.message({type: 'error', text: globals.labels.required_input_message});
        }
    }




    var fields = {
            cols: [
                {
                    id: 'fs46',
                    height: 250,
                    rows: [
                        {
                            view: 'combo',
                            name: 'oa_ac_id',
                            label: 'Выбрать',
                            required: true,
                            options:[
                                {id: '1', value: 'На дороге'},
                                {id: '2', value: 'В Поти'},
                                {id: '3', value: 'В иране'},
                                {id: '4', value: 'Прибытие в Баку'},
                                {id: '5', value: 'Был прибыл'}
                            ]
                            // suggest: globals.ports_suggest
                        },
                        {
                            view: 'datepicker',
                            name: 'oa_start_date',
                            label: 'Дата',
                        },
                        {
                            view: 'text',
                            name: 'oa_note',
                            label: 'Заметка',
                        },
                    ]
                },
                {id: 'fs_spacer', width: 10},
            ]
        }
    ;

    var footer = {
        cols: [
            {
                view: 'button',
                type: 'iconButton',
                // icon: 'usd',
                label: 'Создать',
                width: 100,
                click: createOperation
            },
            {}
        ]
    };


    var ui = {
        id: 'add_action_window',
        view: 'window',
        width:600,
        height:500,
        // fullscreen:true,
        head: {
            view: 'toolbar',
            height: 30,
            elements: [
                {id: 'add_action_window_title', view: 'label', label: 'Новая операция'},
                {},
                {
                    view: 'button', type: 'icon', icon: 'close', label: '', width: 20, hotkey: "escape",
                    click: function () {
                        $$("add_action_window").hide();
                    }
                }
            ]
        },
        position: 'center',
        modal: true,
        move: true,
        body: {
            id: 'add_action_form',
            view: 'form',
            // scroll:"y",
            // width: 1050,
            // height: 625,
            elements: [
                fields,
                // {
                //     type: 'line',
                //     rows: [
                //         toolbar,
                //       // datatable
                //     ]
                // },
                footer
            ],
            elementsConfig: {
                labelWidth: 150
            },
        },
        on:{
            onShow: function(){
            }
        }
    };

    return webix.ui(ui);


});
