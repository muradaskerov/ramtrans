define([
    "globals"
], function (globals) {
    var windowClose = function () {
        $$("add_company_window").hide();
    };

    function addPaient() {
        if($$("formAddCompany").validate()){
            let formData = $$("formAddCompany").getValues();

            webix.ajax().post("company-add", formData, function (text, data, xhr) {
                var response = data.json();
                //
                $$("companies_datatable").loadNext(-1,0,{
                    before:function(){
                        var url = this.data.url;
                        this.clearAll();
                        this.data.url = url;
                    }
                },0,1);

                windowClose();

                if (response != undefined && response.type == "ok") {

                }
                else {

                }
            });
        }else{
            webix.message({type: 'error', text: globals.labels.required_input_message});
        }
    }

    var form = {
        id: 'formAddCompany',
        view: 'form',
        scroll: false,
        elements: [
            {
                padding: 5,
                    rows: [
                        {
                            cols: [
                                {
                                    rows: [
                                        {
                                            view: "text",
                                            name: "com_name",
                                            label: globals.labels.company,
                                            labelWidth: 200,
                                            width: 380,
                                            required: true,
                                        },
                                    ]
                                }
                            ]
                        },
                        {height: 30},
                        {
                            view: "button",
                            label: globals.labels.confirm,
                            type: "form",
                            click: addPaient
                        }
                    ]
            },

        ],
    };
    var ui = {
        id: "add_company_window",
        view: "window",
        width: 950,
        move: false,
        resize: false,
        modal: true,
        position: "center",
        head: {
            padding: 5,
            cols: [
                {
                    view: 'label',
                    borderless: true,
                    label: "Şirkət əlavə et",
                },
                {},
                {width:100},
                {
                    borderless: true,
                    view: "button",
                    type: "iconButton",
                    icon: "mdi mdi-close",
                    css: "close_window_button",
                    name: "close",
                    width: 30,
                    height: 30,
                    click: "$$('add_company_window').hide();"
                }
            ]
        },
        body: {
            rows: [
                form
            ]
        }
    };

    return webix.ui(ui);
});
