define(["globals"], function (globals) {

    var windowClose = function () {
        $$("add_customer_window").hide();
    };

    function createCustomer() {
        if ($$("add_customer_form").validate()) {
            let formData = $$("add_customer_form").getValues();
            console.log(formData)

            webix.ajax().post("/customers", {formData:formData}, function (text, data, XmlHttpRequest) {
                var response = data.json();

                console.log(response.data);

                if (response && response.success === true) {
                    webix.message(globals.labels.operation_successful);
                    windowClose();

                    $$("customers_datatable").loadNext(-1,0,{
                        before:function(){
                            var url = this.data.url;
                            this.clearAll();
                            this.data.url = url;
                        }
                    },0,1);

                    location.assign("/");
                }
            });
        } else {
            webix.message({type: 'error', text: globals.labels.required_input_message});
        }
    }




    var fields = {
            cols: [
                {
                    id: 'fs46',
                    height: 100,
                    rows: [
                        {
                            view: 'text',
                            name: 'cus_name',
                            label: 'Имя',
                        },
                    ]
                },
                {id: 'fs_spacer', width: 10},
            ]
        }
    ;

    var footer = {
        cols: [
            {
                view: 'button',
                type: 'iconButton',
                // icon: 'usd',
                label: globals.labels.confirm,
                autowidth:true,
                click: createCustomer
            },
            {}
        ]
    };


    var ui = {
        id: 'add_customer_window',
        view: 'window',
        width:600,
        height:500,
        // fullscreen:true,
        head: {
            view: 'toolbar',
            height: 30,
            elements: [
                {id: 'add_customer_window_title', view: 'label', label: 'Новый клиент'},
                {},
                {
                    view: 'button', type: 'icon', icon: 'close', label: '', width: 20, hotkey: "escape",
                    click: function () {
                        $$("add_customer_window").hide();
                    }
                }
            ]
        },
        position: 'center',
        modal: true,
        move: true,
        body: {
            id: 'add_customer_form',
            view: 'form',
            // scroll:"y",
            // width: 1050,
            // height: 625,
            elements: [
                fields,
                // {
                //     type: 'line',
                //     rows: [
                //         toolbar,
                //       // datatable
                //     ]
                // },
                footer
            ],
            elementsConfig: {
                labelWidth: 150
            },
        },
        on:{
            onShow: function(){
            }
        }
    };

    return webix.ui(ui);


});
