define([
    "globals"
], function (globals) {
    var windowClose = function () {
        $$("add_new_expenses_window").hide();
    };

    function addExpense() {
        var formData = $$("formAddEpensess").getValues();
        console.log(formData)
        // console.log(surveysData);
        webix.ajax().post("transfers/addCredit", {expenses: formData}, function (text, data, xhr) {
            var response = data.json();
            console.log(response);
            if (response.success === true) {
                console.log(response);
                windowClose();

                $$("cashier_datatable").loadNext(-1,0,{
                    before:function(){
                        var url = this.data.url;
                        this.clearAll();
                        this.data.url = url;
                    }
                },0,1);

                $$("cashregtable_datatable").loadNext(-1,0,{
                    before:function(){
                        var url = this.data.url;
                        this.clearAll();
                        this.data.url = url;
                    }
                },0,1);



            } else {
                webix.message({type: 'error', text: response.message});
            }
        });
    }



    let yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);

    var form = {
        id: 'formAddEpensess',
        view: 'form',
        scroll: false,
        elements: [
            {
                padding: 5,
                rows: [
                    {
                        cols: [
                            {
                                rows: [
                                    {
                                        view: "combo",
                                        id: "customers",
                                        name: "trnsf_cus_id",
                                        options: {
                                            data: globals.customer_list,
                                            width: 100,
                                            fitMaster: true,
                                        },
                                        label: 'Müştəri',
                                        labelWidth: 170,
                                        width: 350,
                                        required: true,
                                    },
                                    {
                                        view: "combo",
                                        name: "trnsf_entered_currency",
                                        value:"6",
                                        label: 'Valyuta',
                                        labelWidth: 170,
                                        width: 350,
                                        options:["USD","AZN","EUR"],
                                        required: true,
                                    },
                                    {
                                        view: "datepicker",
                                        name: "trnsf_transfer_ts",
                                        // value: new Date(1996,2,2),
                                        label: 'Tarix',
                                        labelWidth: 170,
                                        width: 350,
                                        required: true,
                                    },
                                ]
                            },
                            {width: 40},
                            {
                                width:500,
                                rows: [

                                    {
                                        view: "text",
                                        name: "trnsf_entered_amnt",
                                        attributes:{ type:"number",min: "0",},
                                        label:'Məbləğ',
                                        labelWidth: 200,
                                        width: 380,
                                    },
                                    {
                                        view: "radio",
                                        name: "trnsf_type",
                                        label: "Xərc tipi",
                                        value: 2,
                                        options: [
                                            {"id": 'credit', "value": "Mədaxil"},
                                            {"id": 'export', "value": "Məxaric"}
                                        ],
                                        labelWidth: 200,
                                        on: {
                                            onChange: function (id) {
                                                if(id === 'credit'){
                                                    $$("trnsf_cashier_type").hide();
                                                    $$("trnsf_cashier_type").setValue(null);
                                                }else{
                                                    $$("trnsf_cashier_type").show();

                                                }
                                            }
                                        }
                                    },
                                    {
                                        view: "combo",
                                        name: "trnsf_cashier_type",
                                        id: "trnsf_cashier_type",
                                        label: 'Xərc tipi',
                                        labelWidth: 200,
                                        width: 380,
                                        options:globals.cashier_types_list,
                                        required: true,
                                    },
                                    {
                                        view: "text",
                                        name: "trnsf_note",
                                        label:'Qeyd',
                                        labelWidth: 200,
                                        width: 380,
                                    },
                                ]
                            },
                            {
                                width:5,
                            },
                        ]
                    },
                    {height: 30},
                    {
                        view: "button",
                        label: globals.labels.confirm,
                        type: "form",
                        click: addExpense
                    }
                ]
            },

        ],
    };
    var ui = {
        id: "add_new_expenses_window",
        view: "window",
        width:850,
        move: false,
        resize: false,
        modal: true,
        position: "center",
        head: {
            padding: 5,
            cols: [
                {
                    view: 'label',
                    borderless: true,
                    label: 'Yeni xərc'
                },
                {},
                {width:100},
                {
                    borderless: true,
                    view: "button",
                    type: "iconButton",
                    icon: "mdi mdi-close",
                    css: "close_window_button",
                    name: "close",
                    width: 30,
                    height: 30,
                    click: "$$('add_new_expenses_window').hide();"
                }
            ]
        },
        body: {
            rows: [
                form
            ]
        }
    };

    return webix.ui(ui);
});
