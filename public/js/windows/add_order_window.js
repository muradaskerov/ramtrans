define([
    "globals"
], function (globals) {
    var windowClose = function () {
        $$("add_order_window").hide();
    };

    function saveOrder() {
        if ($$("formAddOrder").validate()) {
            let formData = $$("formAddOrder").getValues();
            let containers = $$("cargo_dtable").serialize();
            console.log(formData)
            // console.log(surveysData);
            webix.ajax().post("orders", {formData,containers}, function (text, data, xhr) {
                var response = data.json();

                if (response && response.success === true) {
                    webix.message(globals.labels.operation_successful);

                    windowClose();

                    $$("orders_datatable").loadNext(-1,0,{
                        before:function(){
                            var url = this.data.url;
                            this.clearAll();
                            this.data.url = url;
                        }
                    },0,1);
                }
            });
        } else {
            webix.message({type: 'error', text: globals.labels.required_input_message});
        }
    }

    let yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);

    var addCargoButtonClicked = function (name, args) {

        var id = $$('cargo_dtable').add({
            container_num: 0
        });

        $$('cargo_dtable').select(id);

    };

    var deleteRow = function (event, obj, node) {

        setTimeout(function () {
            var table = $$("cargo_dtable");
            table.editStop();
            var selectedId = obj.row;//item['id'];

            if (selectedId != undefined) {
                table.remove(selectedId);
            }
        }, 100);

    }

    var toolbar = {
        view: 'toolbar',
        height: 35,
        elements: [
            {
                view: "label",
                label: "Kонтейнеры"
            },
            {},
            {
                view: 'button',
                id: 'newCargoRowButton',
                type: 'iconButton',
                name: 'cargo_add_button',
                icon: 'plus',
                label: 'Новый',
                width: 90,
                click: addCargoButtonClicked
            }
        ]
    };

    var datatable = {
        id: 'cargo_dtable',
        view: 'datatable',
        scrollX: false,
        select: 'row',
        headerRowHeight: 24,
        footer: true,
        height: 155,
        editable: true,
        autoConfig: true,
        columns: [
            {
                id: 'container_num',
                header: '<b>Контейнер №</b>',
                fillspace: true,
                editor: "text",
            },
            {
                id: 'seal',
                header: '<b>Пломба</b>',
                editor:"text",
                fillspace: true,
            },
            {
                id: 'container_type',
                header: '<b>Тип-Фут</b>',
                editor:"select",
                fillspace: true,
                optionslist:true,
                options: [
                    {id: '20', value: '20`'},
                    {id: '40', value: '40`'},
                    {id: '40_ot', value: '40` open top`'},
                    {id: 'fr', value: 'flat rack'},
                    {id: '40_hq', value: '40` HQ'},
                ],
            },
            {
                id: 'place',
                header: '<b>Места</b>',
                editor:"text",
                fillspace: true,
            },
            {
                id: 'gross',
                header: '<b>Брутто</b>',
                editor:"text",
                fillspace: true,

            },
            {
                id: 'cargo',
                header: '<b>Груз</b>',
                editor: "text",
                fillspace: true,
            },
            {id: "delete_row", header: "", template: "{common.trashIcon()}", width: 35}
        ],
        data: [],
        on: {
        },
        onClick: {
            'fa-trash': deleteRow
        }
    };

    var form = {
        id: 'formAddOrder',
        view: 'form',
        scroll: false,
        elements: [
            {
                padding: 5,
                rows: [
                    {
                        cols: [
                            {
                                rows: [
                                    // {
                                    //     view: "text",
                                    //     name: "invoice",
                                    //     label: 'Инвойс №',
                                    //     attributes: {type: "number"},
                                    //     labelWidth: 170,
                                    //     width: 350,
                                    // },
                                    {
                                        view: "text",
                                        name: "conosament",
                                        label: 'Коносамент №',
                                        labelWidth: 170,
                                        width: 350,
                                    },
                                    {
                                        view: "text",
                                        name: "freight_sum",
                                        label: 'Фрахт сумма',
                                        attributes: {type: "number"},
                                        labelWidth: 170,
                                        width: 350,
                                    },
                                    {
                                        view: "combo",
                                        name: "line_id",
                                        label: 'Линия',
                                        labelWidth: 170,
                                        required: true,
                                        width: 350,
                                        options: {
                                            data: globals.line_list,
                                            // width: 100,
                                            fitMaster: true,
                                        },
                                    },
                                    // {
                                    //     view: "text",
                                    //     name: "container_num",
                                    //     label: 'Контейнер №',
                                    //     labelWidth: 170,
                                    //     width: 350,
                                    // },
                                    // {
                                    //     view: "text",
                                    //     name: "seal",
                                    //     label: 'Пломба',
                                    //     attributes: {type: "number"},
                                    //     labelWidth: 170,
                                    //     width: 350,
                                    // },
                                    // {
                                    //     view: "combo",
                                    //     name: "container_type",
                                    //     options: {
                                    //         data: [
                                    //             {id: '20', value: '20`'},
                                    //             {id: '40', value: '40`'},
                                    //             {id: '40_ot', value: '40` open top`'},
                                    //             {id: 'fr', value: 'flat rack'},
                                    //             {id: '40_hq', value: '40` HQ'},
                                    //         ],
                                    //         width: 100,
                                    //         fitMaster: false,
                                    //     },
                                    //     label: 'Тип-Фут',
                                    //     labelWidth: 170,
                                    //     width: 350,
                                    // },
                                    // {
                                    //     view: "text",
                                    //     name: "place",
                                    //     label: 'Места',
                                    //     attributes: {type: "number"},
                                    //     labelWidth: 170,
                                    //     width: 350,
                                    // },
                                    // {
                                    //     view: "text",
                                    //     name: "gross",
                                    //     label: 'Брутто',
                                    //     attributes: {type: "number"},
                                    //     labelWidth: 170,
                                    //     width: 350,
                                    // },
                                    {
                                        view: "text",
                                        name: "cubic_meter",
                                        label: 'CBM',
                                        attributes: {type: "number"},
                                        labelWidth: 170,
                                        width: 350,
                                    },
                                    {
                                        view: "combo",
                                        name: "receiver_id",
                                        options: {
                                            data: globals.customer_list,
                                            width: 100,
                                            fitMaster: true,
                                        },
                                        label: 'Получатель',
                                        labelWidth: 170,
                                        required: true,
                                        width: 350,
                                    },
                                    {
                                        view: "text",
                                        name: "free_days",
                                        label: 'Free days',
                                        attributes: {type: "number"},
                                        labelWidth: 170,
                                        width: 350,
                                    },
                                    // {
                                    //     view: "text",
                                    //     name: "cargo",
                                    //     label: 'Груз',
                                    //     labelWidth: 170,
                                    //     width: 350,
                                    // },

                                    // {
                                    //     view: "checkbox",
                                    //     id: "disability",
                                    //     name: "p_disability",
                                    //     label: globals.labels.disability,
                                    //     labelWidth: 170,
                                    //     width: 350,
                                    //     on: {
                                    //         onChange: function (newv, oldv) {
                                    //             if (newv === 1) {
                                    //                 $$("disability_type").enable();
                                    //             } else {
                                    //                 $$("disability_type").setValue('');
                                    //                 $$("disability_type").disable();
                                    //             }
                                    //         }
                                    //     }
                                    // },

                                ]
                            },
                            {width: 40},
                            {
                                width: 500,
                                rows: [

                                    {
                                        view: "datepicker",
                                        name: "est_departure_date",
                                        label: "Дата - ETD",
                                        labelWidth: 170,
                                        width: 350,
                                    },
                                    {
                                        view: "datepicker",
                                        name: "est_arrival_date",
                                        label: "Дата ETA",
                                        labelWidth: 170,
                                        width: 350,
                                    },
                                    {
                                        view: "combo",
                                        name: "transport_type",
                                        required: true,
                                        options: {
                                            data: [
                                                {id: 'ship', value: 'Судно'},
                                                {id: 'bus', value: 'Авто'},
                                                {id: 'train', value: 'Train'}
                                            ],
                                            fitMaster: true,
                                        },
                                        label: 'Тип транспорта',
                                        labelWidth: 170,
                                        width: 350,
                                    },
                                    {
                                        view: "text",
                                        name: "documents",
                                        label: 'Документы :',
                                        labelWidth: 170,
                                        width: 350,
                                    },
                                    {
                                        view: "text",
                                        name: "details",
                                        label: 'ДЕТАЛИ',
                                        labelWidth: 170,
                                        width: 350,
                                    },
                                    {
                                        view: "radio",
                                        name: "type_order",
                                        options: [
                                            {"id": 'import', "value": "Import"},
                                            {"id": 'export', "value": "Export"},
                                        ],
                                        required: true,
                                    },

                                ]
                            },
                            {
                                width: 5,
                            },
                        ]
                    },
                    {height: 30},
                    toolbar,
                    datatable,

                    {
                        view: "button",
                        label: globals.labels.confirm,
                        type: "form",
                        click: saveOrder
                    }
                ]
            },

        ],
    };
    var ui = {
        id: "add_order_window",
        view: "window",
        width: 850,
        move: true,
        resize: false,
        modal: true,
        position: "center",
        head: {
            padding: 5,
            cols: [
                {
                    view: 'label',
                    borderless: true,
                    label: 'Новый заказ'
                },
                {},
                // {
                //     view: "button",
                //     type: "iconButton",
                //     icon: "mdi mdi-credit-card-scan",
                //     label: globals.labels.rfid_reader_button,
                //     width: 70
                // },
                {width: 100},
                {
                    borderless: true,
                    view: "button",
                    type: "iconButton",
                    icon: "mdi mdi-close",
                    css: "close_window_button",
                    name: "close",
                    width: 30,
                    height: 30,
                    click: "$$('add_order_window').hide();"
                }
            ]
        },
        body: {
            rows: [
                form
            ]
        }
    };

    return webix.ui(ui);
});
