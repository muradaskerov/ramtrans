define(["globals"], function (globals) {

    var windowClose = function () {
        $$("add_tariff_window").hide();
    };

    function createTariff() {
        if ($$("add_tariff_form").validate()) {
            let formData = $$("add_tariff_form").getValues();
            console.log(formData)

            webix.ajax().post("/tariffs", formData, function (text, data, XmlHttpRequest) {
                var response = data.json();

                if (response && response.success === true) {
                    webix.message(globals.labels.operation_successful);
                    windowClose();

                    $$("tariffs_datatable").loadNext(-1,0,{
                        before:function(){
                            var url = this.data.url;
                            this.clearAll();
                            this.data.url = url;
                        }
                    },0,1);
                }
            });
        } else {
            webix.message({type: 'error', text: globals.labels.required_input_message});
        }
    }




    var fields = {
            cols: [
                {
                    id: 'fs46',
                    height: 250,
                    rows: [
                        {
                            view: "combo",
                            name: "tar_line_id",
                            label: 'Линия',
                            labelWidth: 170,
                            width: 350,
                            options: {
                                data: globals.line_list,
                                width: 100,
                                fitMaster: false,
                            },
                        },
                        {
                            view: "combo",
                            name: "tar_container_type",
                            options: {
                                data: [
                                    {id: '20', value: '20'},
                                    {id: '40', value: '40'}
                                ],
                                width: 100,
                                fitMaster: false,
                            },
                            label: 'Тип-Фут',
                            labelWidth: 170,
                            width: 350,
                        },
                        {
                            view: 'text',
                            attributes: {type: "number"},
                            name: 'tar_free_day',
                            label: 'свободных дней',
                        },
                        {
                            view: 'text',
                            attributes: {type: "number"},
                            name: 'tar_price',
                            label: 'сумма',
                        },
                        {
                            view: 'text',
                            attributes: {type: "number"},
                            name: 'tar_begin_day',
                            label: 'First day',
                        },
                        {
                            view: 'text',
                            attributes: {type: "number"},
                            name: 'tar_end_day',
                            label: 'Last day',
                        },
                    ]
                },
                {id: 'fs_spacer', width: 10},
            ]
        }
    ;

    var footer = {
        cols: [
            {
                view: 'button',
                type: 'iconButton',
                // icon: 'usd',
                label: 'Создать',
                width: 100,
                click: createTariff
            },
            {}
        ]
    };


    var ui = {
        id: 'add_tariff_window',
        view: 'window',
        width:600,
        height:500,
        // fullscreen:true,
        head: {
            view: 'toolbar',
            height: 30,
            elements: [
                {id: 'add_tariff_window_title', view: 'label', label: 'Новый тариф'},
                {},
                {
                    view: 'button', type: 'icon', icon: 'close', label: '', width: 20, hotkey: "escape",
                    click: function () {
                        $$("add_tariff_window").hide();
                    }
                }
            ]
        },
        position: 'center',
        modal: true,
        move: true,
        body: {
            id: 'add_tariff_form',
            view: 'form',
            // scroll:"y",
            // width: 1050,
            // height: 625,
            elements: [
                fields,
                // {
                //     type: 'line',
                //     rows: [
                //         toolbar,
                //       // datatable
                //     ]
                // },
                footer
            ],
            elementsConfig: {
                labelWidth: 150
            },
        },
        on:{
            onShow: function(){
            }
        }
    };

    return webix.ui(ui);


});
