define(["globals"], function (globals) {

    var windowClose = function () {
        $$("choose_customer_window").hide();
        globals.selected_transfers = [];
    };

    function addClient() {
        var selectedItem = $$('customer_choose_dt').getSelectedItem();
        // console.log(selectedItem)
        let orderIds = [];


        if(Array.isArray(globals.selected_transfers)){
            orderIds = globals.selected_transfers.map(x => x.id);
        }else{
            orderIds.push(globals.selected_transfers.id)
        }

        if(selectedItem === undefined || orderIds.length === 0){
            webix.alert('Klienti seç')
        }else{
            var cus_id = selectedItem.cus_id;

            webix.ajax().post("/orders/addClient/",{customer_id: cus_id,orderIds: orderIds}, function (text, data, xhr) {
                var response = data.json();
                if (response.success === true) {
                    webix.message(globals.labels.operation_successful);
                    $$('transfers_datatable').loadNext(-1, 0, {
                        before: function () {
                            var url = this.data.url;
                            this.clearAll();
                            this.data.url = url;
                        }
                    }, 0, 1);
                    windowClose()
                } else {
                    webix.message({type: 'error', text: response.message});
                }
            });
        }
    }



    var ui = {
        id: 'choose_customer_window',
        view: 'window',
        width:600,
        height:500,
        // fullscreen:true,
        head: {
            view: 'toolbar',
            height: 30,
            elements: [
                {id: 'choose_customer_window_title', view: 'label', label: 'Klienti seç'},
                {},
                {
                    view: 'button', type: 'icon', icon: 'close', label: '', width: 30, hotkey: "escape",
                    click: function () {
                        $$("choose_customer_window").hide();
                    }
                }
            ]
        },
        position: 'center',
        modal: true,
        move: true,
        body: {
            id: 'choose_customer_form',
            view: 'form',
            // scroll:"y",
            // width: 1050,
            // height: 625,
            elements: [
                {
                    view: "datatable",
                    id:'customer_choose_dt',
                    url:"/customers",
                    select: "row",
                    columns: [
                        {
                            id: "cus_id",
                            header: [{text: "Клиент №"}, {content:"textFilter"},],
                            css: {"text-align": "right"},
                        },
                        {id: "cus_name",
                            header: [{text: "Имя"}, {content:"textFilter"},],
                            fillspace:true,
                        },
                    ],
                    on:{
                        onAfterSelect: function (id, preserve) {
                            console.log(id,preserve)

                            // globals.editor_ch =  $$(this).getItem(id.row).cus_id;

                        },

                    },
                },
                {
                    cols: [
                        {
                            view: 'button',
                            type: 'form',
                            // icon: 'usd',
                            label: 'Yadda saxla',
                            click: addClient
                        },
                        {}
                    ]
                }
            ],
            elementsConfig: {
                labelWidth: 150
            },
        },
        on:{
            onShow: function(){
            }
        }
    };

    return webix.ui(ui);


});
