define([
    "globals"
], function (globals) {
    var windowClose = function () {
        $$("expenses_datatable").clearAll();
        $$("expenses_window").hide();

        $$("reports_datatable").loadNext(-1,0,{
            before:function(){
                var url = this.data.url;
                this.clearAll();
                this.data.url = url;
            }
        },0,1);
    };

    var columns = null;
    var cols = [];
    console.log(globals.usr_excluded)

    function addExpenses() {
        let formData = $$("expenses_datatable").serialize();

        var dtable = $$("expenses_datatable");

        var column = dtable.getFilter('expense_1')
        var header = column.value; // 0 - index of header row for multiline

        var currencies = {};

        formData.forEach(function (obj) {
            console.log(Object.keys(obj))
            Object.keys(obj).forEach(function(key) {
                // console.log(obj[key]);
                if(key.indexOf("expense") !== -1){
                    if(dtable.getFilter(key) === null){
                        currencies[key] = 'usd';
                    }else{
                        currencies[key] = dtable.getFilter(key).value
                    }
                }
            });
        });



        console.log(currencies)

        webix.ajax().post("transfers", {
            expenses: formData,
            currencies: currencies
        }, function (text, data, xhr) {
            var response = data.json();
            console.log(response);
            if (response.success === true) {
                console.log(response);
                windowClose();

                location.reload();
            } else {
                webix.message({type: 'error', text: response.message});
            }
        });
    }

    var calculateExpense = function (state,editor) {

        console.log(state,editor);

        if(editor.column === 'customer_id'){
            console.log('sss')
            return 0;
        }

        var dtable = $$("expenses_datatable");
        let new_value = parseFloat(state.value);
        let old_value = parseFloat(state.old);

        var item = dtable.getItem(editor.row);

        switch (editor.column) {
            case 'expense_22':
                console.log('22');
                break;
            case 'customer_id':
                console.log('cus');
                break;
            case 'expense_23':
                var profit = parseFloat(item['expense_23'])-parseFloat(item['expense_22']);

                if(!isNaN(profit)){
                    console.log(item);

                    item['expense_38'] = profit;

                    console.log(item['expense_23'])


                }else{
                    item['expense_38'] = parseFloat(item['expense_23']);
                }
                dtable.updateItem(editor.row, item);

                console.log('23');
                break;
            case 'expense_37':


                var doxod = parseFloat(item['expense_38'])-parseFloat(item['expense_37']);
                // if(!isNaN(old_value)){
                //     doxod = doxod+old_value;
                // }

                console.log(doxod);

                if(!isNaN(doxod)){
                    console.log(item);

                    item['expense_38'] = doxod;

                }else{
                    item['expense_38'] = parseFloat(item['expense_37']);
                }

                dtable.updateItem(editor.row, item);

                break;
            default:

                if(!isNaN(new_value)){

                    console.log(item);

                    if(item['expense_22'] !== undefined && !isNaN(item['expense_22'])){
                        console.log('topladi');
                        if(!isNaN(old_value)){
                            item['expense_22'] = parseFloat(item['expense_22'])+new_value-old_value;
                        }else{
                            item['expense_22'] = parseFloat(item['expense_22'])+new_value;
                        }
                    }else{
                        item['expense_22'] = new_value;
                    }

                    if(item['expense_23'] !== undefined && !isNaN(item['expense_23']) ){
                        console.log('topladi')
                        if(!isNaN(old_value)){
                            item['expense_23'] = parseFloat(item['expense_23'])+new_value-old_value;
                        }else{
                            item['expense_23'] = parseFloat(item['expense_23'])+new_value;
                        }
                    }else{
                        item['expense_23'] = item['expense_22'];
                            console.log('ozune')

                    }


                    console.log(item['expense_22'])

                    dtable.updateItem(editor.row, item);

                    // dtable.refresh();
                    // dtable.refreshColumns();
                }
        }


        var data = dtable.serialize();
        let sum = 0;


        var column = dtable.getColumnConfig('expense_1')
        var header = column.header[1]; // 0 - index of header row for multiline


        console.log(data);

        for (var i = 0; i < data.length; i++) {

            sum += parseFloat(data[i].expense_value);
        }


    };

    function addExpenseTypes(){
        var typ = $$("new_ex_type").getValue();
        var dtable = $$("expenses_datatable");

        columns = webix.toArray(dtable.config.columns);

            webix.ajax().post("addExpenseType", {
            expense_type: typ,
        }, function (text, data, xhr) {
            var response = data.json();

            if (response.success === true) {
                console.log(response);

                columns.insertAt({
                    id: 'expense_'+response.et_id, header: [
                        {text: typ, rotate: true},
                        {
                            content: 'selectFilter',
                            options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                        },
                    ], editor: "text",
                }, 5);

                dtable.refreshColumns();

                console.log('expense_'+response.et_id);

                // cols.push({
                //         id: 'expense_'+response.et_id, header: [
                //             {text: typ, rotate: true},
                //             {
                //                 content: 'selectFilter',
                //                 options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                //             },
                //         ], editor: "text",
                //     },
                //     );
                //
                // dtable.define("columns",cols);


                // dtable.refresh();



                $$("new_ex_type").setValue("");

                globals.resetMasterValues(globals);


            } else {
                webix.message({type: 'error', text: response.message});
            }
        });

        console.log(typ)
    }

    let yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);



    var formExpenses = {
        id: "expenses_datatable",
        view: "datatable",
        editable: true,
        // footer: true,
        leftSplit: 5,
        rightSplit: 4,
        headerRowHeight: 140,
        scrollX: true,
        headermenu:{
            width:250,
            height:500,
            scroll:true,
        },
        on: {
            onAfterEditStop: calculateExpense,
            onBeforeFilter: function () {
                return false;
            },
            onBeforeColumnHide: function (id) {
                var dtable = $$("expenses_datatable");

                this.eachRow(function(row){
                    const item = dtable.getItem(row);

                    var old_value = parseFloat(item[id]);
                    var new_value = 0;

                    if(!isNaN(old_value)){

                        console.log(item);

                        if(item['expense_22'] !== undefined && !isNaN(item['expense_22'])){
                            console.log('topladi');
                            item['expense_22'] = parseFloat(item['expense_22'])-old_value;
                        }

                        if(item['expense_23'] !== undefined && !isNaN(item['expense_23']) ){
                            console.log('topladi')
                            item['expense_23'] = parseFloat(item['expense_23'])-old_value;
                        }

                        item[id] = 0;


                        dtable.updateItem(row, item);

                        // dtable.refresh();
                        // dtable.refreshColumns();
                    }
                });

                return true;

                // var item = this.getItem(editor.row);
            },
            onBeforeColumnShow: function (id) {
                console.log(id);

                // setTimeout(function () {
                //     console.log('geetdii')
                // },100);

                // return true;
            }
        },


    };


    var ui = {
        id: "expenses_window",
        view: "window",
        resize: false,
        fullscreen: true,
        width: 1050,
        modal: true,
        position: "center",
        head: {
            padding: 5,
            cols: [
                {
                    view: 'label',
                    borderless: true,
                    label: 'Расходы'
                },
                {},

                {width: 100},
                {
                    borderless: true,
                    view: "button",
                    type: "iconButton",
                    icon: "mdi mdi-close",
                    css: "close_window_button",
                    name: "close",
                    width: 30,
                    height: 30,
                    click: windowClose
                }
            ]
        },
        on: {
            onShow: function () {
                var dtable = $$("expenses_datatable");

                cols = [
                    {
                        id: "invoice", header: [
                            {text: 'Инвойс №', rotate: true},
                        ],
                        width: 100,
                        headermenu:false
                    },
                    // {
                    //     id: "customer_id",
                    //     width: 100,
                    //     editor:'textAreaPopup',
                    //     collection: globals.customer_list,
                    //     header: [
                    //         {text: 'Клиент', rotate: true},
                    //     ],
                    //     headermenu:false
                    // },
                    {
                        id: "container_num", header: [
                            {text: 'Контейнер №', rotate: true},
                        ], width: 100,
                        headermenu:false
                    },
                    {
                        id: "transport_type",
                        width: 100,
                        collection: [
                            {id: 'ship', value: 'Судно'},
                            {id: 'bus', value: 'Авто'},
                            {id: 'train', value: 'Train'}
                        ],
                        header: [
                            {text: 'Transport type', rotate: true},
                        ],
                        headermenu:false
                    },
                    {
                        id: "container_type",
                        collection: [
                            {id: '20', value: '20`'},
                            {id: '40', value: '40`'},
                            {id: '40_ot', value: '40` open top`'},
                            {id: 'fr', value: 'flat rack'},
                            {id: '40_hq', value: '40` HQ'},
                        ],
                        header: [
                            {text: 'Тип-Фут', rotate: true},
                        ],
                        width: 100,
                        headermenu:false
                    },
                    {
                        id: "date_notation", header: [
                            {text: 'Дата отчета:', rotate: true},
                        ],
                        width: 140,
                        format:webix.Date.dateToStr("%d-%m-%Y"),
                        map:'(date)#date_notation#',
                        headermenu:false
                    },
                    {
                        id: 'expense_1',
                        header: [
                            {text: 'Фрахт', rotate: true,},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text"
                    },
                    {
                        id: 'expense_2',
                        header: [
                            {text: 'Inspection  fee destination', rotate: true,},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_3', header: [
                            {text: 'ТНС', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text"
                    },
                    {
                        id: 'expense_4', header: [
                            {text: 'МН', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text"
                    },
                    {
                        id: 'expense_5', header: [
                            {text: 'оформление док-тов в линии', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text"
                    },
                    {
                        id: 'expense_19', header: [
                            {text: 'Стархов Конт.', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text"
                    },
                    {
                        id: 'expense_6', header: [
                            {text: 'Хранен. Конт.', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text"
                    },
                    {
                        id: 'expense_7', header: [
                            {text: 'Демередж ', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text"
                    },
                    {
                        id: 'expense_8', header: [
                            {text: 'перетарка ', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_9', header: [
                            {text: 'Ж.Д. Грузия ', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_33', header: [
                            {text: 'Ж.Д. Грузия пустои', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text"
                    },
                    {
                        id: 'expense_10', header: [
                            {text: 'тупик', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text"
                    },
                    {
                        id: 'expense_11', header: [
                            {text: 'Пломб', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_12', header: [
                            {text: 'Авто-перевозка между терминалами', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text"
                    },
                    {
                        id: 'expense_13', header: [
                            {text: 'талманирование', rotate: true},
                            {
                                content: 'selectFilter',
                                options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                            },
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_14', header: [
                            {text: 'Прочие расходы ( cesh )', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text"
                    },
                    {
                        id: 'expense_15', header: [
                            {text: 'письмо в ж/д департамент', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_16', header: [
                            {text: 'дополнительное укрепление пена', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text"
                    },
                    {
                        id: 'expense_17', header: [
                            {text: 'вирамаина', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text"
                    },
                    {
                        id: 'expense_18', header: [
                            {text: 'перевеска контейнеров', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text"
                    },
                    {
                        id: 'expense_20', header: [
                            {text: 'Итого расходы в Грузии', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text"
                    },
                    {
                        id: 'expense_25', header: [
                            {text: 'VAT', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_26', header: [
                            {text: 'таможеный сбор', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_27', header: [
                            {text: 'перетарка', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_28', header: [
                            {text: 'Авто-перевозка Poti-Baku-Poti', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_32', header: [
                            {text: 'port facility security', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_36', header: [
                            {text: 'Ж.Д. Азербайджан', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text"
                    },
                    {
                        id: 'expense_29', header: [
                            {text: 'КК', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text"
                    },
                    {
                        id: 'expense_34', header: [
                            {text: 'Ж.Д. Азербайджан  пустои', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text"
                    },
                    {
                        id: 'expense_35', header: [
                            {text: 'DHL', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text",
                        hidden:true,
                    },
                    {
                        id: 'expense_30', header: [
                            {text: 'Таможенный пункт БК', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text"
                    },
                    {
                        id: 'expense_31', header: [
                            {text: 'SM', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text"
                    },
                    {
                        id: 'expense_21', header: [
                            {text: 'Итого расходы в Азербайджане', rotate: true},
                            {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                        ], editor: "text"
                    },



                ];

                console.log(globals.expense_types);

                globals.expense_types.forEach(function (obj) {


                    var found = false;
                    cols.forEach(function (obje){
                        if(obje.id === 'expense_'+obj.id || parseInt(obj.id) === 22 || parseInt(obj.id) === 23 ||  parseInt(obj.id) === 37 || parseInt(obj.id) === 38){
                            found = true;
                        }
                    });

                    if(!found){
                        // console.log(obj)
                        cols.push({
                            id: 'expense_'+obj.id, header: [
                                {text: obj.value, rotate: true},
                                {
                                    content: 'selectFilter',
                                    options: [{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]
                                },
                            ], editor: "text",
                            hidden:true
                        });
                    }

                });

                if(parseInt(globals.usr_excluded) === 0){
                    cols.push({
                            id: 'expense_22',
                            header: [
                                {text: 'Общая\n сумма\n расходов',rotate: true},
                                {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                            ],
                            headermenu:false
                        },
                        {
                            id: 'expense_23',
                            editor: "text",
                            header: [
                                {text: 'Стоимость для клиента', rotate: true},
                                {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                            ],
                            headermenu:false
                        },
                        {
                            id: 'expense_37',
                            header: [
                                {text: 'Доход от фрахта',rotate: true},
                                {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                            ], editor: "text",
                            headermenu:false
                        },
                        {
                            id: 'expense_38',
                            header: [
                                {text: 'Доход от перевозки', rotate: true},
                                {content: 'selectFilter',options:[{id:'USD',value:'USD'},{id:'EUR',value:'EUR'},{id:'AZN',value:'AZN'}]},
                            ],
                            headermenu:false
                        });
                }



                dtable.define("columns",cols);

                dtable.refreshColumns();

                $$("expenses_datatable").parse(globals.report_orders);
                //
                if(globals.report_orders.length > 0){
                    if(globals.report_orders[0]['transfers'] !== undefined){
                        globals.report_orders[0]['transfers'].forEach(function (obj) {
                            // console.log(obj.trnsf_entered_currency)

                            // console.log(obj)
                            $$("expenses_datatable").showColumn('expense_'+obj.trnsf_trsp_type);


                            if($$("expenses_datatable").getFilter('expense_'+obj.trnsf_trsp_type) !== null){
                                $$("expenses_datatable").getFilter('expense_'+obj.trnsf_trsp_type).value = obj.trnsf_entered_currency;
                            }

                            // if(obj.trnsf_trsp_type == '68'){
                            //     console.log($$("expenses_datatable").getFilter('expense_'+obj.trnsf_trsp_type).value);
                            // }

                            // console.log([
                            //     'expense_'+obj.trnsf_trsp_type,
                            //     obj.trnsf_entered_currency
                            // ])

                        })
                    }
                }

                // dtable.refreshColumns();





                // console.log(globals.report_orders)

                // let selectedItem = $$("orders_datatable").getSelectedItem();

            }
        },
        body: {
            rows: [
                {
                    rows: [
                        {
                            borderless: true,
                            id: 'expenses_toolbar',
                            view: 'toolbar',
                            css: "toolbarrr",
                            height: 50,
                            elements: [
                                {
                                    view: 'button',
                                    type: "form",
                                    id: "save_expenses",
                                    label: "Сохранять",
                                    width: 120,
                                    click: addExpenses
                                },
                                {},
                                {
                                    view: 'text',
                                    id: "new_ex_type",
                                    width: 170,
                                    placeholder: "Новый тип расходов",
                                },

                                {
                                    view: 'button',
                                    type: "form",
                                    id: "add_new_ex_type",
                                    label: "Создать",
                                    width: 120,
                                    click: addExpenseTypes
                                },
                            ]

                        },
                        formExpenses
                    ]
                }
            ]
        }
    };


    return webix.ui(ui);
});
