define(["globals"], function (globals) {

    var sendMessage = function (){

        if ($$("formFeedback").validate()) {
            var data = $$("formFeedback").getValues();

            webix.ajax().post("/feedback",data, function (text, data, xhr) {

                var response = data.json();

                if (response.success === true) {
                    webix.message({text: "Müraciət göndərildi."})
                    $$('formFeedback').clear();
                    $$('feedback_window').hide();
                }
                else {
                    webix.message({type: "error", text: response.message})
                }

            });
        }else{
            webix.message({type: "error", text: "Bütün bölmələr doldurulmalıdır!"})
        }

    };



    var header = {
        view: "toolbar",
        height: 40,
        elements: [

            {
                cols: [
                    {
                        view: "template",
                        template: "Proqram tərtibatçısına müraciət",
                        align: "left",
                        css: "radiogram_title",
                        borderless: true
                    },
                    {
                        view: "button",
                        label: 'Bağla',
                        width: 100,
                        align: 'right',
                        click: "$$('feedback_window').hide();",
                        hotkey: "escape"
                    }
                ]
            }
        ]
    }

    var formFeedback = {
        id: 'formFeedback',
        view:'form',
        scroll:false,
        elements:[
            {
                view: 'richselect', name: 'appeal_type', label: 'Müraciətin növü:',
                required: true,
                labelWidth: 150,
                options: [
                    {id: 'Müraciət'},
                    {id: 'Təklif'},
                    {id: 'Şikayət'},
                    {id: 'Nasazlıq'},
                ]
            },
            {
                view: "textarea",
                name: 'appeal_text',
                label: 'Müraciət mətni',
                required: true,
                labelWidth: 150,
                height: 150
            },
            {
                view:"button",
                label: 'Müraciəti göndər',
                type: 'form',
                css: "button_primary button_raised",
                click: sendMessage
            }
        ]
    };


    var ui = {
        id: "feedback_window",
        view: "window",
        width: 500,
        move: false,
        resize: false,
        modal: true,
        position: "center",
        body: {
            padding:10,
            rows: [
                formFeedback
            ]

        },
        head: {
            rows: [
                header
            ]
        }
    };

    return webix.ui(ui);
});
