define([
    "globals"
], function (globals) {
    var windowClose = function () {
        $$("info_order_window").hide();
    };

    function editOrder(form) {

        if ($$(form).validate()) {
            let formData = $$(form).getValues();
            console.log(formData)
            // console.log(surveysData);
            let order_id = globals.selected.id;
            webix.ajax().put("orders/" + order_id, formData, function (text, data, xhr) {
                var response = data.json();
                if (response.success === true) {
                    webix.message(globals.labels.operation_successful);

                    globals.dtable.loadNext(-1, 0, {
                        before: function () {
                            var url = this.data.url;
                            this.clearAll();
                            this.data.url = url;
                        }
                    }, 0, 1);


                } else {
                    webix.message({type: 'error', text: response.message});
                }
            });
        } else {
            webix.message({type: 'error', text: globals.labels.required_input_message});
        }
    }


    let yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);

    function calculateFreeDays() {
        let all_date = [
            $$("arrival_date_to_poti_of_containers").getValue(),
            $$("departure_date_from_poti_of_containers").getValue(),
            $$("arrival_date_to_baku").getValue(),
            $$("discharge_date_at_baku").getValue(),
            $$("returned_date").getValue()
        ];

        console.log(all_date)
        for (var i = all_date.length - 1; i >= 0; i--) {
            if (all_date[i] !== null && i !== 0 && all_date[0] !== null) {
                const diffTime = Math.abs(all_date[i].getTime() - all_date[0].getTime());
                const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                // let past_days = all_date[i]-all_date[0];
                console.log(diffDays)
                $$("past_free_days").setValue(diffDays);
                $$("remain_free_days").setValue(parseInt($$("free_days").getValue()) - diffDays);
                break;
            }
        }

    }

    function calculateDemurrage() {
        let val = $$("remain_free_days").getValue();
        console.log(val);

        if (parseInt(val) < 0) {
            console.log("calc demurrage");
            let futt = $$("container_type").getValue();
            let line_id = $$("line_id").getValue();
            let free_days = $$("free_days").getValue();
            let tar_id = $$("selected_tariff_id").getValue();
            console.log(futt);

            if (tar_id === null || tar_id.length === 0) {
                webix.message({type: 'error', text: 'Tarif seçilməyib'});
            } else {
                webix.ajax().get("calculateDemurrage", {
                    tar_id: tar_id,
                    futt: futt,
                    line_id: line_id,
                    remain_free_days: val,
                    free_days: free_days
                }, function (text, data, xhr) {
                    var response = data.json();

                    if (response.success === true) {
                        console.log(response);
                        $$("first_period_demurrage").setValue(response.first_period)
                        $$("second_period_demurrage").setValue(response.second_period)
                        $$("third_period_demurrage").setValue(response.third_period)

                        $$("first_period_amount").setValue("$ " + response.first_price)
                        $$("second_period_amount").setValue("$ " + response.second_price)
                        $$("third_period_amount").setValue("$ " + response.third_price)
                    } else {
                        webix.message({type: 'error', text: response.message});
                    }

                });
            }


        }
    }


    var containers_status = {

        view: 'fieldset', label: '<b>Konteynerlərin vəziyyəti</b>',
        body: {
            rows: [
                {
                    view: "datepicker",
                    name: "arrival_date_to_poti_of_containers",
                    id: "arrival_date_to_poti_of_containers",
                    label: "Дата прибытия в Поти",
                    on: {
                        onChange: calculateFreeDays
                    }
                },
                {
                    view: "datepicker",
                    name: "departure_date_from_poti_of_containers",
                    id: "departure_date_from_poti_of_containers",
                    label: "Выход из Поти",
                    on: {
                        onChange: calculateFreeDays
                    }
                },
                {
                    view: "datepicker",
                    name: "arrival_date_to_baku",
                    id: "arrival_date_to_baku",
                    label: "Bakıya çatma tarixi",
                    on: {
                        onChange: calculateFreeDays
                    }
                },
                {
                    view: "datepicker",
                    name: "discharge_date_at_baku",
                    id: "discharge_date_at_baku",
                    label: "Bakıda boşalma tarixi",
                    on: {
                        onChange: calculateFreeDays
                    }
                },
                {
                    view: "datepicker",
                    name: "returned_date",
                    id: "returned_date",
                    label: "Təhvil verildiyi tarix",
                    on: {
                        onChange: calculateFreeDays
                    }
                },
                {
                    view: "text",
                    id: "past_free_days",
                    name: "past_free_days",
                    label: 'Количество прошедших свободных дней',
                    attributes: {type: "number"},
                },
                {
                    view: "text",
                    name: "free_days",
                    id: "free_days",
                    label: 'Количество свободных дней всего',
                    attributes: {type: "number"},
                },

                {
                    view: "text",
                    name: "remain_free_days",
                    id: "remain_free_days",
                    label: 'Количество оставшихся свободных дней',
                    attributes: {type: "number"},
                    on: {
                        onChange: calculateDemurrage
                    }
                },


            ]
        }
    }

    var documents_satatus = {

        view: 'fieldset', label: '<b>Sənədlərin vəziyyəti</b>',
        body: {
            rows: [
                {
                    view: "datepicker",
                    name: "received_date_to_poti_of_documents",
                    label: "Дата прибытия в Поти",
                },
                {
                    view: "datepicker",
                    name: "sent_date_from_poti_of_documents",
                    label: "Дата отправки из Поти",
                },
                {
                    view: "datepicker",
                    name: "release_date",
                    label: 'Дата релиза',
                },

            ]
        }
    };

    var demurrage_field = {

        view: 'fieldset', label: '<b>Demerej</b>',
        body: {
            rows: [
                // {
                //     view: "combo",
                //     name: "tar_id",
                //     id: "selected_tariff_id",
                //     label: 'Tariff',
                //     labelWidth: 170,
                //     width: 350,
                //     body:{ //list configuration
                //         view: "list",
                //         url:"/tariffs",
                //     },
                //     // options: {
                //     //     data: globals.tariff_list,
                //     //     width: 100,
                //     //     fitMaster: false,
                //     // },
                //     on:{
                //         onChange:calculateDemurrage
                //     }
                // },
                {
                    view: "multiselect",
                    name: "tar_id",
                    labelWidth: 170,
                    label: 'Тарифы',
                    id: "selected_tariff_id",
                    options: {
                        data: globals.tariff_list,
                        fitMaster: true,
                    },
                    on: {
                        onChange: calculateDemurrage
                    }
                },
                {
                    cols: [
                        {
                            view: "text",
                            name: "first_period_demurrage",
                            id: "first_period_demurrage",
                            labelWidth: 100,
                            label: 'I period',
                            attributes: {type: "number"},
                        },
                        {
                            view: "label",
                            name: "first_period_amount",
                            id: "first_period_amount",
                            // attributes: {type: "number"},
                        },
                    ],
                },
                {
                    cols: [
                        {
                            view: "text",
                            name: "second_period_demurrage",
                            id: "second_period_demurrage",
                            labelWidth: 100,
                            label: 'II period',
                            attributes: {type: "number"},
                        },
                        {
                            view: "label",
                            name: "second_period_amount",
                            id: "second_period_amount",
                            // attributes: {type: "number"},
                        },
                    ],
                },
                {
                    cols: [
                        {
                            view: "text",
                            name: "third_period_demurrage",
                            id: "third_period_demurrage",
                            labelWidth: 100,
                            label: 'III period',
                            attributes: {type: "number"},
                        },
                        {
                            view: "label",
                            name: "third_period_amount",
                            id: "third_period_amount",
                            // attributes: {type: "number"},
                        },
                    ],
                },

                // {
                //     name: 'note_date',
                //     view: "datepicker",
                //     label: 'gon. Son. Gel-qay ucun nez alin',
                // },
            ]
        }
    };

    var demurrage_reason_field = {

        view: 'fieldset', label: '<b>Demerejə səbəb</b>',
        body: {
            rows: [
                {
                    view: "radio",
                    name: "reasoner_demurrage",
                    options: [
                        {"id": 'client', "value": "Клиент"},
                        {"id": 'rtg', "value": "РТГ"},
                        {"id": 'other', "value": "Прочие"},
                    ]
                },
                {
                    view: 'textarea',
                    name: 'reason_demurrage_note',
                    label: 'Заметка:',
                    labelWidth: "70",
                },
            ]
        }
    }

    var fields = {

        cols: [
            {

                rows: [
                    documents_satatus,
                    {height: 10},
                    containers_status,

                ]
            },
            {width: 10},
            {
                rows: [
                    demurrage_field,
                    {height: 10},
                    demurrage_reason_field
                ]
            },
            {width: 10}
        ]
    }

    // var formExpenses = {
    //     id: "expenses_datatable",
    //     view: "datatable",
    //     editable: true,
    //     height: 500,
    //     footer: true,
    //     drag:"order",
    //     scrollX: false,
    //     resizeColumn: true,
    //     columns: [
    //         {id: 'expense_type', header: 'Xərc növü', editor: "combo", fillspace: true, options: globals.expense_types},
    //         {id: "expense_value", header: "Xərc miqdarı", editor: "text", fillspace: true}
    //     ],
    //     data: [
    //         {
    //             expense_type: "ok",
    //             expense_value: 1
    //         }
    //     ],
    //     on: {
    //         onAfterEditStop: calculateExpense,
    //     },
    //
    //
    // };


    var ui = {
        id: "info_order_window",
        view: "window",
        move: true,
        resize: false,
        width: 1050,
        modal: true,
        position: "center",
        head: {
            padding: 5,
            cols: [
                {
                    view: 'label',
                    borderless: true,
                    label: 'О заказе'
                },
                {},

                {width: 100},
                {
                    borderless: true,
                    view: "button",
                    type: "iconButton",
                    icon: "mdi mdi-close",
                    css: "close_window_button",
                    name: "close",
                    width: 30,
                    height: 30,
                    click: "$$('info_order_window').hide();"
                }
            ]
        },
        on: {
            onShow: function () {
                console.log(globals.dtable);
                $$("formEditOrder").clear();
                $$("formInfoOrder").clear();

                $$("formEditOrder").parse(globals.selected);
                $$("formInfoOrder").parse(globals.selected);

                // $$("expenses_datatable").clearAll();

                webix.ajax().get("/getTariffsByOrderID", {futt: globals.selected.container_type,line_id: globals.selected.line_id}, function (text, data, xhr) {
                    var response = data.json();

                    $$("selected_tariff_id").define("options",response);
                    $$("selected_tariff_id").refresh();

                    console.log(response.data);
                });

                calculateFreeDays()
                //console.log(globals.views)


            }
        },
        body: {
            rows: [
                {
                    type: "clean",
                    rows: [
                        {
                            borderless: true,
                            view: "tabbar",
                            multiview: true,
                            value: "formEditOrder",
                            options: [
                                {value: 'Редактировать', id: 'formEditOrder'},
                                {value: 'Информация', id: 'formInfoOrder'},
                                // {value: 'Xərclər', id: 'tabExpenses'},
                            ],
                            on: {
                                onAfterTabClick: function (id) {
                                    // loadDatatableValues(id);
                                }
                            }
                        },
                        {
                            animate: false,
                            cells: [
                                {
                                    id: 'formEditOrder',
                                    view: 'form',
                                    elements: [
                                        {
                                            padding: 5,
                                            rows: [
                                                {
                                                    cols: [
                                                        {
                                                            width: 450,
                                                            rows: [

                                                                // {
                                                                //     view: "text",
                                                                //     name: "invoice",
                                                                //     label: 'Инвойс №',
                                                                //     attributes: {type: "number"},
                                                                //     labelWidth: 170,
                                                                //     width: 350,
                                                                // },
                                                                {
                                                                    view: "text",
                                                                    name: "conosament",
                                                                    label: 'Коносамент №',
                                                                    labelWidth: 170,
                                                                    width: 350,
                                                                },
                                                                {
                                                                    view: "text",
                                                                    name: "freight_sum",
                                                                    label: 'Фрахт сумма',
                                                                    attributes: {type: "number"},
                                                                    labelWidth: 170,
                                                                    width: 350,
                                                                },
                                                                {
                                                                    view: "combo",
                                                                    name: "line_id",
                                                                    id: "line_id",
                                                                    label: 'Линия',
                                                                    labelWidth: 170,
                                                                    width: 350,
                                                                    options: {
                                                                        data: globals.line_list,
                                                                        width: 100,
                                                                        fitMaster: false,
                                                                    },
                                                                },
                                                                {
                                                                    view: "text",
                                                                    name: "container_num",
                                                                    label: 'Контейнер №',
                                                                    labelWidth: 170,
                                                                    width: 350,
                                                                },
                                                                {
                                                                    view: "text",
                                                                    name: "seal",
                                                                    label: 'Пломба',
                                                                    attributes: {type: "number"},
                                                                    labelWidth: 170,
                                                                    width: 350,
                                                                },
                                                                {
                                                                    view: "combo",
                                                                    name: "container_type",
                                                                    id: "container_type",
                                                                    options: {
                                                                        data: [
                                                                            {id: '20', value: '20`'},
                                                                            {id: '40', value: '40`'},
                                                                            {id: '40_ot', value: '40` open top`'},
                                                                            {id: 'fr', value: 'flat rack'},
                                                                            {id: '40_hq', value: '40` HQ'}
                                                                        ],
                                                                        width: 100,
                                                                        fitMaster: false,
                                                                    },
                                                                    label: 'Тип-Фут',
                                                                    labelWidth: 170,
                                                                    width: 350,
                                                                },
                                                                {
                                                                    view: "text",
                                                                    name: "place",
                                                                    label: 'Места',
                                                                    attributes: {type: "number"},
                                                                    labelWidth: 170,
                                                                    width: 350,
                                                                },
                                                                {
                                                                    view: "text",
                                                                    name: "gross",
                                                                    label: 'Брутто',
                                                                    attributes: {type: "number"},
                                                                    labelWidth: 170,
                                                                    width: 350,
                                                                },
                                                                {
                                                                    view: "text",
                                                                    name: "cubic_meter",
                                                                    label: 'CBM',
                                                                    attributes: {type: "number"},
                                                                    labelWidth: 170,
                                                                    width: 350,
                                                                },
                                                                {
                                                                    view: "text",
                                                                    name: "cargo",
                                                                    label: 'Груз',
                                                                    labelWidth: 170,
                                                                    width: 350,
                                                                },
                                                                {
                                                                    view: "combo",
                                                                    name: "receiver_id",
                                                                    options: {
                                                                        data: globals.customer_list,
                                                                        width: 100,
                                                                        fitMaster: true,
                                                                    },
                                                                    label: 'Получатель',
                                                                    width: 350,
                                                                    labelWidth: 170,
                                                                },

                                                            ]
                                                        },
                                                        {width: 10},
                                                        {
                                                            width: 500,
                                                            rows: [

                                                                {
                                                                    view: "combo",
                                                                    name: "customer_id",
                                                                    options: {
                                                                        data: globals.customer_list,
                                                                        width: 100,
                                                                        fitMaster: true,
                                                                    },
                                                                    label: 'Клиент',
                                                                    width: 600,
                                                                    labelWidth: 300,
                                                                },
                                                                // {
                                                                //     view: "text",
                                                                //     name: "free_days",
                                                                //     label: 'Free days',
                                                                //     attributes: {type: "number"},
                                                                //     width: 600,
                                                                //     labelWidth:300,
                                                                // },
                                                                {
                                                                    view: "datepicker",
                                                                    name: "est_departure_date",
                                                                    label: "Дата - ETD",
                                                                    width: 600,
                                                                    labelWidth: 300,
                                                                },
                                                                {
                                                                    view: "datepicker",
                                                                    name: "est_arrival_date",
                                                                    label: "Дата ETA",
                                                                    width: 600,
                                                                    labelWidth: 300,
                                                                },
                                                                {
                                                                    view: "combo",
                                                                    name: "transport_type",
                                                                    options: {
                                                                        data: [
                                                                            {id: 'ship', value: 'Судно'},
                                                                            {id: 'bus', value: 'Авто'},
                                                                            {id: 'train', value: 'Train'}
                                                                        ],
                                                                        width: 100,
                                                                        fitMaster: false,
                                                                    },
                                                                    label: 'Transport type',
                                                                    width: 600,
                                                                    labelWidth: 300,
                                                                },
                                                                {
                                                                    view: "text",
                                                                    name: "documents",
                                                                    label: 'Документы :',
                                                                    width: 600,
                                                                    labelWidth: 300,
                                                                },
                                                                {
                                                                    view: "text",
                                                                    name: "details",
                                                                    label: 'ДЕТАЛИ',
                                                                    width: 600,
                                                                    labelWidth: 300,
                                                                },
                                                                // {
                                                                //     view: "combo",
                                                                //     options: {
                                                                //         data: globals.customer_list,
                                                                //         width: 100,
                                                                //         fitMaster: true,
                                                                //     },
                                                                //     name: "consignee_id",
                                                                //     label: 'Consignee',
                                                                //     width: 600,
                                                                //     labelWidth:300,
                                                                // },
                                                                {
                                                                    view: "text",
                                                                    name: "cartons",
                                                                    label: 'Cartons',
                                                                    width: 600,
                                                                    labelWidth: 300,
                                                                },
                                                                {
                                                                    view: "text",
                                                                    name: "weight",
                                                                    label: 'Weight',
                                                                    attributes: {type: "number"},
                                                                    width: 600,
                                                                    labelWidth: 300,
                                                                },
                                                                {
                                                                    view: "text",
                                                                    name: "weight_isized",
                                                                    attributes: {type: "number"},
                                                                    label: 'ВЕС ОФОРМИЛСЯ',
                                                                    width: 600,
                                                                    labelWidth: 300,
                                                                },
                                                                {
                                                                    view: "combo",
                                                                    name: "container_terminal_id",
                                                                    options: {
                                                                        data: globals.terminal_list,
                                                                        width: 100,
                                                                        fitMaster: true,
                                                                    },
                                                                    label: 'ТЕРМИНАЛ ДЛЯ ВОЗВРАТА КОНТЕЙНЕРА',
                                                                    width: 600,
                                                                    labelWidth: 300,
                                                                },
                                                                {
                                                                    view: "combo",
                                                                    name: "sender_terminal_id",
                                                                    options: {
                                                                        data: globals.terminal_list,
                                                                        width: 100,
                                                                        fitMaster: true,
                                                                    },
                                                                    label: 'ТЕРМИНАЛ ОТПРАВКИ',
                                                                    width: 600,
                                                                    labelWidth: 300,
                                                                },
                                                            ]
                                                        },

                                                    ]
                                                },
                                                {height: 30},
                                                {
                                                    view: "button",
                                                    label: globals.labels.confirm,
                                                    type: "form",
                                                    click: function(){editOrder("formEditOrder")}
                                                }
                                            ]
                                        },

                                    ],
                                    width: 1050,
                                    height: 600
                                },
                                {
                                    id: 'formInfoOrder',
                                    view: 'form',
                                    width: 1050,
                                    height: 600,
                                    // scroll: false,
                                    elements: [
                                        fields,
                                        {height: 30},
                                        {
                                            view: "button",
                                            label: globals.labels.confirm,
                                            type: "form",
                                            click: function(){editOrder("formInfoOrder")}
                                        }
                                    ],
                                    elementsConfig: {
                                        labelWidth: 290
                                    },
                                },
                                // {
                                //     id: "tabExpenses",
                                //     width: 1050,
                                //     height: 600,
                                //     rows: [
                                //         {
                                //             borderless: true,
                                //             id: 'expenses_toolbar',
                                //             view: 'toolbar',
                                //             css: "toolbarrr",
                                //             height: 50,
                                //             elements: [
                                //                 {
                                //                     view: 'button',
                                //                     type: "form",
                                //                     id: "add_row",
                                //                     label: "Əlavə et",
                                //                     width: 120,
                                //                     click: addRow
                                //                 },
                                //                 {
                                //                     view: 'button',
                                //                     type: "form",
                                //                     id: "save_expenses",
                                //                     label: "Yadda saxla",
                                //                     width: 120,
                                //                     click: addExpenses
                                //                 },
                                //                 {
                                //                     view: 'combo',
                                //                     id: "selected_currency",
                                //                     label: "Məzənnə",
                                //                     value: "USD",
                                //                     options: ["USD", "AZN", "EUR"],
                                //                 },
                                //
                                //                 {},
                                //             ]
                                //
                                //         },
                                //         formExpenses
                                //     ]
                                // }
                            ]
                        }
                    ]
                }
            ]
        }
    };


    return webix.ui(ui);
});
