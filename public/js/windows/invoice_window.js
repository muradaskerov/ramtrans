define(["globals"], function (globals) {
    //console.log(app);
    var printshow_results = function () {
        console.log(globals)
        var embed = $$("result_tabview").$view.getElementsByTagName('embed');
        if (embed.length > 0) {
            var src = embed[0].getAttribute("src");
            // console.log(src)
            var url = src;
            var url = new URL(url);
            url.searchParams.set('doc_type', 'pdf');
            var doc = window.open(url.href);
            doc.print();
        }
    };

    var ui = {
        id: 'invoice_window',
        view: 'window',
        position: 'center',
        modal: true,
        head: {
            view: 'toolbar',
            height: 30,
            elements: [
                {id: 'invoice_window_title', view: 'label', label: 'Nəticə', width: 150},
                {},
                {
                    view: 'button', type: 'icon', icon: 'close', label: 'Bağla', width: 70,
                    click: function () {
                        $$("invoice_window").hide();
                    }
                }
            ]
        },
        body: {
            id: 'result_form',
            view: 'form',
            width: 860,
            height: 580,
            elements: [
                {
                    id: 'result_tabview',
                    view: 'tabview',
                    cells: [
                        {
                            header: 'Qəbz',
                            width: 50,
                            body: {
                                id: 'show_results_view',
                                view: 'template',
                                template: '<embed width = "100%" height = "480" src = "'+globals.url+'/transfer/getInvoice?trnsf_id=#trnsf_id#"></embed>'
                            }
                        },
                    ]
                },
                {
                    cols: [
                        {
                            view: 'button', type: 'iconButton', icon: 'print', label: 'Çap', width: 100,
                            click: printshow_results
                        },
                        {},
                        {
                            view: 'button', type: 'iconButton', icon: 'close', label: 'Bağla', width: 100, hotkey: "escape",
                            click: function () {
                                $$("invoice_window").hide();
                            }
                        }
                    ]
                }
            ]
        }
    };

    return webix.ui(ui);
});