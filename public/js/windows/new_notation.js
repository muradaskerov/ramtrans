define(["globals"], function (globals) {

    var selected_row_id;

    function addNewPassage() {

        var data = new Object();

        data.requests = globals.unpassaged_list;
        data.selected_passage_id = selected_row_id;
        // data.checkbox_value = $$('check_existing_passage').getValue();

        if ($$("new_passage_window") !== undefined) {
            webix.extend($$("new_passage_window"), webix.ProgressBar);
        }
        $$("new_passage_window").showProgress({hide: false});

        webix.ajax().get("/notations/create", null, function (text, data, XmlHttpRequest) {
            var response = data.json();


            console.log(response.data);

            if (response && response.success === true) {
                let notation_id = response.data;

                webix.ajax().post("/orders/addToNotation", {notation_id: notation_id,data:$$("selected_requests_table").serialize()}, function (text, data, XmlHttpRequest) {
                    var response = data.json();

                    console.log(response.data);

                    if (response && response.success === true) {
                        globals.unpassaged_list = [];
                        globals.checked_list = [];
                        globals.selected_array = [];

                        webix.message("Информация была добавлена в рапорт.");

                        $$("orders_datatable").loadNext(-1,0,{
                            before:function(){
                                var url = this.data.url;
                                this.clearAll();
                                this.data.url = url;
                            }
                        },0,1);


                        $$("new_passage_window").showProgress({hide: true});
                        $$('new_passage_window').hide();
                    }
                });
            }
        });

    }

    var selected_requests_table = {
        view: "datatable",
        id: "selected_requests_table",
        resizeColumn: true,
        css: 'request_dtable',
        height: 300,
        scroll: "y",
        rowHeight: 30,
        headerRowHeight: 35,
        columns: [
            {id: "invoice", header: "Инвойс №",fillspace: true},
            {id: "conosament", header: "Коносамент №",fillspace: true},
            {id: "freight_sum", header: "Фрахт сумма",fillspace: true},
            {id: "line_id", header: "Линия",fillspace: true},
            {id: "container_num", header: "Контейнер №",fillspace: true},
        ]
    };
    var passage_toolbar = {
        height: 35,
        type: "clean", cols: [
            {width: 10},
            {},
            {
                view: "template", //optional
                width: 190,
                css: "passage_header"
            },

//            },
            {width: 10},
            {},
            {
                view: "button",
                id: "add_passage_button",
                value: "Добавить",
                type: "form",
                width: 100,
                click: addNewPassage
            },
            {width: 20}
        ]
    };


    var ui = {
        view: "window",
        id: "new_passage_window",
        move: false,
        // resize: true,
        fullscreen: true,
        position: "center",
        head: {
            view: 'toolbar',
            height: 30,
            elements: [
                {width: 450},
                {view: 'label', label: 'Yeni raport'},
                {},
                {
                    view: 'button', type: 'icon', icon: 'close', label: '', width: 20, hotkey: "escape",
                    click: function () {
                        $$('new_passage_window').hide();

                    }
                }
            ]
        },
        body: {
            rows: [
                passage_toolbar,
                selected_requests_table,
                // {},
                // existig_tables,
                // existing_passages_pager_select,
                {height: 5},
            ]
        }

    };
    return webix.ui(ui);
});