define(["globals"], function (globals) {

    /**-----------------------------------------------------------------------------------------------
     **                                            Opens cargo window
     **----------------------------------------------------------------------------------------------*/


    var addCargoButtonClicked = function (name, args) {

        var id = globals.cargo_table.add({
            cargo_quantity: 1
        });
//        $$("ticket_window").show();
        globals.cargo_table.editCell(id, 'cargo_type');
        globals.cargo_table.select(id);

    };



    function createReport() {
        if ($$("create_report_form").validate()) {
            let formData = $$("create_report_form").getValues();
            console.log(formData)

            webix.ajax().get("/reports/create", null, function (text, data, XmlHttpRequest) {
                var response = data.json();

                console.log(response.data);

                if (response && response.success === true) {
                    let report_id = response.data;
                    console.log(report_id)

                    webix.ajax().post("/notations/addToReport", {report_id: report_id,data:formData,orders:globals.checked_orders}, function (text, data, XmlHttpRequest) {
                        var response = data.json();

                        console.log(response.data);

                        if (response && response.success === true) {
                            webix.message("Отчет создан.");

                            $$('create_report_window').hide();
                            globals.checked_orders = [];

                            $$("notations_datatable").loadNext(-1,0,{
                                before:function(){
                                    var url = this.data.url;
                                    this.clearAll();
                                    this.data.url = url;
                                }
                            },0,1);

                        }
                    });
                }
            });
        } else {
            webix.message({type: 'error', text: globals.labels.required_input_message});
        }
    }



    var footer = {
        cols: [
            {
                id: 'request_bill_button',
                view: 'button',
                type: 'form',
                label: 'Создать',
                click: createReport
            },
            {}
        ]
    };


    var ui = {
        id: 'create_report_window',
        view: 'window',
        width:600,
        height:500,
        // fullscreen:true,
        head: {
            view: 'toolbar',
            height: 30,
            elements: [
                {id: 'create_report_window_title', view: 'label', label: 'Создать отчет'},
                {},
                {
                    view: 'button', type: 'icon', icon: 'close', label: '', width: 20, hotkey: "escape",
                    click: function () {
                        $$("create_report_window").hide();
                    }
                }
            ]
        },
        position: 'center',
        modal: true,
        move: true,
        body: {
            id: 'create_report_form',
            view: 'form',
            elements: [
                {
                    cols: [
                        {
                            id: 'fs4',
                            view: 'fieldset', label: '<b>Транспортная информация</b>',
                            body: {
                                rows: [
                                    {
                                        view: 'combo',
                                        name: 'station_id',
                                        label: 'Станция',
                                        required: true,
                                        options:globals.station_list
                                        // suggest: globals.ports_suggest
                                    },
                                    {
                                        view: 'combo',
                                        name: 'transport_type',
                                        label: 'Тип транспорта',
                                        required: true,
                                        options: {
                                            data: [
                                                {id: 'auto', value: 'Авто'},
                                                {id: 'platform', value: 'Платформа'},
                                                {id: 'tent', value: 'Тент'},
                                                {id: 'vagon', value: 'Вагон'},
                                            ],
                                            fitMaster: true,
                                        },
                                        // suggest: globals.ports_suggest
                                    },
                                    {
                                        view: 'text',
                                        name: 'transport_num',
                                        label: '№ транспорта',
                                        required: true,
                                    },
                                    {
                                        view: 'datepicker',
                                        name: 'date_notation',
                                        label: 'Дата отчета:',
                                    }
                                ]
                            }
                        },
                    ]
                },
                footer
            ],
            elementsConfig: {
                labelWidth: 150
            },
        },
        on:{
            onShow: function(){
                console.log(globals.checked_orders)
            }
        }
    };

    return webix.ui(ui);


});
