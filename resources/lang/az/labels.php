<?php

return [

    'app_name' => 'Система логистики',
    'exit' => 'Выход',
    'settings' => 'Tənzimləmələr',
    'institution' => "Müəssisə",
    'structure_field' => 'Struktur bölmə',
    'confirm' => 'Подтвердите',
    'skip' => 'Keç',
    'previous' => 'Əvvəlki',
    'next' => 'Növbəti',
    'reports' => 'Hesabatlar',
    'username' => 'Имя Пользователя',
    'password' => 'Пароль',
    'login_system' => 'Вход в систему',
    'sap_id' => 'SAP ID',
    'name' => 'Ad',
    'surname' => 'Soyad',
    'father_name'=> 'Ata adı',
    'search_by_sap_id' => "SAP koda görə axtar",
    'gender' => 'Cins',
    'male' => "Kişi",
    'female' => "Qadın",
    'upload' => 'Yüklə',
    'choose_excel_file' => 'Excel fayl seç',
    'delete' => 'Sil',
    'edit' => 'Düzəliş et',
    'close' => 'Bağla',
    'save' => 'Yadda saxla',
    'yes' => 'Bəli',
    'no' => 'Xeyr',
    'status' => 'Status',
    'p_name' => 'Ad',
    'p_surname' => 'Soyad',
    'p_father_name' => 'Ata adı',
    'p_birthday' => 'Doğum tarixi',
    'p_gender' => 'Cins',
    'p_home_address' => 'Ev ünvanı',
    'p_phone' => 'Telefon nömrəsi',
    'checkup_date' => 'Müayinənin keçirildiyi tarix',
    'checkup_locate' => 'Müayinənin keçirildiyi yer',
    'company' => 'Şirkət',
    'position' => 'Vəzifə',
    'current_experience' => 'Vəzifə üzrə staj (ay)',
    'total_experience' => 'Ümumi staj (ay)',
    'rfid_reader_button' => 'Kart',
    'add_form_title' => 'Pasientin qeydiyyat pəncərəsi',
    'required_input_message' => 'Обязательные для заполнения поля должны быть заполнены',
    'p_id_number' => 'Şəxsiyyət vəsiqəsi',
    'disability' => 'Əlillik',
    'disability_type' => 'Əlillik məlumatı',
    'suvey_window' => 'Müayinə pəncərəsi',
    'bodies_of_the_chest' => 'Döş müayinəsi / The bodies of the chest',
    'note_of_doctor' => 'Həkimin şərhi / Comments',
    'digestive' => 'Həzm üzvləri/ Digestive',
    'saved' => 'Yadda saxlanıldı',
    'patient_nationality' => 'Milliyyəti',
    'new_patient_added' => 'Gözləmədə yeni pasient var.',
    'new_company_added' => 'Yeni şirkət əlavə olundu.',
    'survey_types_not_selected' => "Müayinə tipləri seçilməyib.",
    'operation_successful' => 'Операция успешно'
];
