<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
{{--{{dd($data)}}--}}
<table class="table table-bordered" style="">
    <tr>
        <th>
            Nömrəsi
        </th>
        <th>
            Müştəri
        </th>
        <th>
            Məbləğ
        </th>
        <th>
            Valyuta
        </th>
        <th>
            Tarix
        </th>
        <th>
            Qeyd
        </th>
        <th>
            Tip
        </th>
    </tr>
    @foreach($data as $result)
        <tr>
            <td>
                {{$result['trnsf_id']}}
            </td>
            <td>
                {{$result['customer']['cus_name']}}
            </td>
            <td>
                {{$result['trnsf_entered_amnt']}}
            </td>
            <td>
                {{$result['trnsf_entered_currency']}}
            </td>
            <td>
                {{$result['trnsf_transfer_ts']}}
            </td>
            <td>
                {{$result['trnsf_note']}}
            </td>
            <td>
                @if($result['trnsf_type'] == 'credit')
                    Mədaxil
                @else
                    Məxaric
                @endif
            </td>

        </tr>
    @endforeach




</table>
</body>
</html>