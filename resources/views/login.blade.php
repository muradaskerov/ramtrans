<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}?v={{env('APP_VERSION')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('css/skins/material.css')}}?v={{env('APP_VERSION')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}?v={{env('APP_VERSION')}}">

    <link type="text/css" media="all" rel="stylesheet" href="{{asset('css/materialdesignicons.css')}}?v={{env('APP_VERSION')}}">


    <script type="text/javascript" src="{{asset('js/libs/webix.js')}}?v={{env('APP_VERSION')}}"></script>


    <script type="text/javascript">
        var require = {
            config: {
                'globals': {
                    labels: '{!! json_encode(Lang::get('labels')) !!}',
                    data_list: ['labels'],
                },
                paths: {
                    text: 'libs/text'
                },
            },
            urlArgs: 'v={{env('APP_VERSION')}}'
        };
    </script>
    <script type="text/javascript" data-main="js/login" src="{{asset('js/libs/require.js')}}?v={{env('APP_VERSION')}}"></script>
    <title>{{env("APP_NAME")}}</title>
</head>
<body>
<div id="main_div">

</div>


</body>
</html>
