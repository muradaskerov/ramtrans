<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}?v={{env('APP_VERSION')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('css/skins/material.css')}}?v={{env('APP_VERSION')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}?v={{env('APP_VERSION')}}">
{{--    <link type="text/css" rel="stylesheet" href="{{asset('css/webix.css')}}?v={{env('APP_VERSION')}}">--}}


    <link type="text/css" media="all" rel="stylesheet" href="{{asset('css/materialdesignicons.css')}}?v={{env('APP_VERSION')}}">
    <link type="text/css" media="all" rel="stylesheet" href="{{asset('css/test.css')}}?v={{env('APP_VERSION')}}">

    <script type="text/javascript" src="{{asset('js/libs/webix.js')}}?v={{env('APP_VERSION')}}"></script>
    <script type="text/javascript" src="{{asset('js/libs/sidebar.js')}}?v={{env('APP_VERSION')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/libs/xlsx.core.styles.min.js')}}?v={{env('APP_VERSION')}}" type="text/javascript"></script>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
{{--    {{dd(getenv("APP_URL"))}}--}}
    <script type="text/javascript">
        var require = {
{{--        {!! dd(env("APP_URL")) !!}--}}
            config: {
                'globals': {
                    labels: '{!! json_encode(Lang::get('labels')) !!}',
                    url: '{!! env("APP_URL") !!}',
                    menu_data: '{!! json_encode(\App\Http\Controllers\DataController::menuList()) !!}',
                    line_list: '{!! json_encode(\App\Http\Controllers\DataController::lineList()) !!}',
                    customer_list: '{!! json_encode(\App\Http\Controllers\DataController::customer_list()) !!}',
                    terminal_list: '{!! json_encode(\App\Http\Controllers\DataController::terminal_list()) !!}',
                    action_list: '{!! json_encode(\App\Http\Controllers\DataController::action_list()) !!}',
                    tariff_list: '{!! json_encode(\App\Http\Controllers\DataController::tariff_list()) !!}',
                    station_list: '{!! json_encode(\App\Http\Controllers\DataController::station_list()) !!}',
                    cashier_types_list: '{!! json_encode(\App\Http\Controllers\DataController::cashier_types_list()) !!}',
                    expense_types: '{!! \App\Http\Controllers\ExpenseTypesController::expenseTypesMap()!!}',
                    report_lists: '{!! json_encode(\App\Http\Controllers\DataController::report_lists()) !!}',
                    user_fullname: '{!! (!is_null(Session::get("user"))) ? Session::get("user")->usr_name." ".Session::get("user")->usr_surname : null !!}',
                    usr_excluded: '{!! (!is_null(Session::get("user"))) ? Session::get("user")->usr_excluded : null !!}',
                    data_list: ['labels','menu_data','expense_types','line_list','customer_list','terminal_list','action_list','tariff_list','station_list','cashier_types_list','report_lists'],
                    resetMasterValues: function (globals) {

                        webix.ajax().get("/expense_types/mapped", '1', function (text, data, xhr) {
                            var result = data.json();
                            console.log(result);
                            globals.expense_types = result;
                        });
                    }
                },
                paths: {
                    text: 'libs/text'
                },

            },
            'urlArgs': 'v={{env('APP_VERSION')}}'
        };
    </script>
    <script type="text/javascript" data-main="js/app" src="{{asset('js/libs/require.js')}}?v={{env('APP_VERSION')}}"></script>
    <title>{{env("APP_NAME")}}</title>
</head>
<body>
{{--{{dd(Session::get("medical"))}}--}}

</body>
</html>
