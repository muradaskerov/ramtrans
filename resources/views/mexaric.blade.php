<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<style>
    body {
        font-family: "Arial";
        font-size:12px;
    }
    tr,th,td{
        border: 1px solid #000000;
        padding: 5px;
        text-align: center;
        margin: 0;

    }
    td{
        width: 50px;
    }
    table{
        border-collapse: collapse;
    }
    h1{
        font-size: 16px;
    }

    h2{
        font-size: 14px;
    }

    h3{
        font-size: 14px;
        font-weight: normal;
        margin: 0;
    }
    .from_whom{
        border-bottom: 1px solid #000000;
        font-size: 14px;

    }

    .why{
        border-bottom: 1px solid #000000;
        font-size: 14px;

    }

    .amount{
        border-bottom: 1px solid #000000;
        font-size: 14px;

    }

</style>
<div class="header">
    <h1 style="text-align: center;border-bottom: 1px solid #000000;padding-bottom: 5px;font-weight: normal">"Ramtrans Group LTD</h1>
</div>
<div class="center">
    <h2 style="text-align: center; padding-bottom: 5px;margin-top: 50px">KASSA MƏXARİC ORDERİNİN QƏBZİ</h2>
</div>
<div style="text-align: center">
    <h3 style="text-align: center;margin-right:20px;display: inline-block;">
        Seriya
    </h3>
    <h3 style="text-align: center;display: inline-block;">
        NO
    </h3>
</div>
<br>
<div style="text-align: center">
    <h3 style="text-align: center;display: inline-block;">
        {{date("d M Y",strtotime($data['trnsf_transfer_ts']))}}-cu il
    </h3>
</div>

<br>
<div class="from_whom">
    <div style="float: left"><b>Kimdən</b></div>
    <div style="text-align: center;display: block">{{$data['customer']['cus_name']}}</div>
</div>
<br>
<div class="why">
    <div style="float: left"><b>Nə üçün</b></div>
    <div style="text-align: center;display: block">{{$data['cash_type']['cast_name']}}</div>
</div>
<br>
<div class="amount">
    <div style="float: left"><b>Məbləğ:</b></div>
    <div style="text-align: center;display: block">{{$data['trnsf_entered_amnt']}} {{$data['trnsf_entered_currency']}} </div>
</div>

<br>
<div class="amount">
    <div style="text-align: center;display: block"><b>kassadan məxaric edilmişdir.</b></div>
</div>
<br>

<div style="display: inline-block;margin-right: 10px;font-size: 15px">
M.Y.
</div>
<div style="display: inline-block;margin-right: 10px;font-size: 15px">
    Rəhbər: Ramin Əliyev<br>
    Baş mühasib: <br>
    Kassir: <br>

</div>
{{--{{dd($ticketData)}}--}}
<br>

</body>
</html>
{{--{{dd("stop")}}--}}
