<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<table class="table table-bordered" style="">
    <tr>
        <th>
            №
        </th>
        <th>
            Инвойс №
        </th>
        <th>
            Коносамент №
        </th>
        <th>
            Фрахт сумма
        </th>
        <th>
            Линия
        </th>
        <th>
            Контейнер №
        </th>
        <th>
            Пломба
        </th>
        <th>
            Тип-Фут
        </th>
        <th>
            Места
        </th>
        <th>
            Брутто
        </th>
        <th>
            CBM
        </th>
        <th>
            Груз
        </th>
        <th>
            Получатель
        </th>
        <th>
            Free days
        </th>
        <th>
            Дата - ETD
        </th>
        <th>
            Дата ETA
        </th>
        <th>
            Судно /Авто
        </th>
        <th>
            Документы :
        </th>
        <th>
            ДЕТАЛИ
        </th>
    </tr>
    <?php $i = 1;?>
    @foreach($data as $result)
        <tr>
            <td>{{$i}}</td>
            @foreach($columns as $column)
                <td>
                    {{$result[$column]}}
                </td>
            @endforeach
        </tr>
        <?php $i++;?>
    @endforeach



</table>
</body>
</html>