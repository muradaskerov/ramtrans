<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<table class="table table-bordered" style="">
    <tr>
        <th>
            №
        </th>
        <th>
            Инвойс №
        </th>
        <th>
            STATION
        </th>
        <th>
            CONTAINER No
        </th>
        <th>
            CONSIGNEE
        </th>
        <th>
            CARGO
        </th>
        <th>
            CARTONS
        </th>
        <th>
            WEIGHT
        </th>
        <th>
            ВЕС ОФОРМИЛСЯ
        </th>
        <th>
            DATE
        </th>
        <th>
            PLATFORMA/WAGON  No
        </th>
        <th>
            TRANSOIL
        </th>
        <th>
            ТЕРМИНАЛ ОТПРАВКИ
        </th>
        <th>
            ТЕРМИНАЛ ДЛЯ ВОЗВРАТА ПУСТОГО КОНТЕЙНЕРА
        </th>
        <th>
            ТИП КОНТЕЙНЕРА
        </th>
    </tr>
    <?php $i = 1;?>
    @foreach($data['orders'] as $result)
        <tr>
            <td>{{$i}}</td>
            <td>
                {{$result['invoice']}}
            </td>
            @if($i == 1)
            <td rowspan="{!! count($data['orders']) !!}">
                {{$data['station']['sta_name']}}
            </td>
            @endif
            <td>
                {{$result['container_num']}}
            </td>
            <td>
                {{$result['customer']['cus_name']}}
            </td>
            <td>
                {{$result['cargo']}}
            </td>
            <td>
                {{$result['cartons']}}
            </td>
            <td>
                {{$result['weight']}}
            </td>
            <td>
                {{$result['weight_isized']}}
            </td>
            @if($i == 1)
                <td rowspan="{!! count($data['orders']) !!}">
                    {{$data['date_notation']}}
                </td>
                <td rowspan="{!! count($data['orders']) !!}">
                    {{$data['wagon_no']}}
                </td>
                <td rowspan="{!! count($data['orders']) !!}">
                    {{$data['transoil']}}
                </td>
            @endif

            <td>
                {{$result['sender_terminal']['ter_name']}}
            </td>
            <td>
                {{$result['container_terminal']['ter_name']}}
            </td>
            <td>
                {{$result['container_type']}}
            </td>
        </tr>
        <?php $i++;?>
    @endforeach



</table>
</body>
</html>
