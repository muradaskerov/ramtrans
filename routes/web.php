<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the 'web' middleware group. Now create something great!
|
*/

Route::get('/', function () {

    if(!is_null(Session::get('user'))){
        return view('main');
    }else{
        return view('login');//login
    }
});

Route::post('/login', 'UserController@logIn');
Route::get('/logout', 'UserController@logOut');

Route::resources([
    'orders' => 'OrderController',
    'notations' => 'NotationController',
    'reports' => 'ReportController',
    'transfers' => 'TransferController',
    'customers' => 'CustomerController',
    'tariffs' => 'TariffController',
]);

Route::post('/orders/addToNotation','OrderController@addToNotation');
Route::post('/orders/addClient','OrderController@addClient');
Route::get('/order/getWithStatus','OrderController@getWithStatus');
Route::get('/cashier/getExpenses','TransferController@getExpenses');
Route::get('/calculateDemurrage','OrderController@calculateDemurrage');
Route::get('/getTariffsByOrderID','TariffController@getTariffsByOrderID');
Route::get('/cashreg/getAll','TransferController@getAllCashReg');
Route::post('/notations/addToReport','NotationController@addToReport');
Route::post('/notations/notationToExcel','NotationController@notationToExcel');
Route::post('/reports/reportToExcel','ReportController@reportToExcel');
Route::post('/reports/balanceToExcel','ReportController@balanceToExcel');
Route::post('/orderActions/store','OrderActionController@store');
Route::post('/transfers/getTransfersByOrderId','TransferController@getTransfersByOrderId');
Route::get('/transfers/all','TransferController@all');
Route::post('/transfers/addCredit','TransferController@addCredit');
Route::post('/expensesToExcel', "TransferController@expensesToExcel");
Route::post('/addExpenseType', "ExpenseTypesController@addExpenseType");
Route::get('/transfer/getInvoice', "TransferController@getInvoice");
Route::get('/expense_types/mapped', "ExpenseTypesController@expenseTypesMap");
Route::get('/menus', "UserController@menus");
